﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IENetP.Generic
{
    public class Enums
    {
        /// <summary>
        /// Type of analysis to use during the test
        /// </summary>
        public enum AnalysisType
        {
            /// <summary>
            /// Analyze the relative difference between subsequent packets in a cyclic stream
            /// </summary>
            Jitter,

            /// <summary>
            /// Analyze the time delay between two or more events
            /// </summary>
            Latency,
        }

        /// <summary>
        /// Analysis method used to evaluate the results
        /// </summary>
        public enum AnalysisMethod
        {
            /// <summary>
            /// Gaussian Random Standard Deviation
            /// </summary>
            StandardDeviation,
        }

        /// <summary>
        /// Status state for a process
        /// </summary>
        public enum ProcessStatusState
        {
            /// <summary>
            /// The process has been defined, but has not been started
            /// </summary>
            Unstarted,

            /// <summary>
            /// The process is currently running
            /// </summary>
            InProgress,

            /// <summary>
            /// The process is currently paused
            /// </summary>
            Paused,

            /// <summary>
            /// The process has completed running
            /// </summary>
            Completed,

            /// <summary>
            /// The process encountered an error
            /// </summary>
            Error,
        }

        /// <summary>
        /// Type of network address
        /// </summary>
        public enum NetworkAddressType
        {
            /// <summary>
            /// Ethernet MAC Address
            /// </summary>
            MacAddress,

            /// <summary>
            /// IP v4 Address
            /// </summary>
            Ipv4Address,
        }

        /// <summary>
        /// Status state for a thread
        /// </summary>
        public enum ThreadStatusState
        {
            /// <summary>
            /// The task has been defined, but has not been started
            /// </summary>
            Unstarted,

            /// <summary>
            /// The task is currently running
            /// </summary>
            InProgress,

            /// <summary>
            /// The task is currently paused
            /// </summary>
            Paused,

            /// <summary>
            /// The task has completed running
            /// </summary>
            Completed,

            /// <summary>
            /// The task encountered an error
            /// </summary>
            Error,
        }

        /// <summary>
        /// Status of the analysis
        /// </summary>
        public enum AnalysisStatus
        {
            /// <summary>
            /// TShark process completed successfully and has packets to analyze
            /// </summary>
            Success,

            /// <summary>
            /// An error occured while processing the capture file
            /// </summary>
            Error,

            /// <summary>
            /// No packets were captured with the desired filter paramaters
            /// </summary>
            NoPacketsCaptured,
        }

        /// <summary>
        /// What type of validation should be conducted on the Wireshark Field Type
        /// </summary>
        public enum ValidateWiresharkFieldType
        {
            /// <summary>
            /// Don't validate the field type
            /// </summary>
            None,

            /// <summary>
            /// Validate that the Wireshark field type can represent a timestamp
            /// </summary>
            Timestamp,

            /// <summary>
            /// Validate tha the Wireshark field type can represent an address
            /// </summary>
            Address,
        }
    }
}
