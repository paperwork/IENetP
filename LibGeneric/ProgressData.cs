﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IENetP.Generic
{
    /// <summary>
    /// Class to hold progress data to be passed between processes and threads
    /// </summary>
    public class ProgressData
    {
        #region Constructors

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        private int _minimum = 0;
        /// <summary>
        /// Minimum value of the progress data
        /// </summary>
        public int Minimum
        {
            get { return _minimum; }
            set { _minimum = Math.Min(value,_maximum); }
        }

        private int _maximum = 100;
        /// <summary>
        /// Maximum value of the progress data
        /// </summary>
        public int Maximum
        {
            get { return _maximum; }
            set { _maximum = Math.Max(value, _minimum); }
        }

        private int _increment = 1;
        /// <summary>
        /// Step value used to increment the progress data
        /// </summary>
        public int Increment
        {
            get { return _increment; }
            set { _increment = value; }
        }

        private int _current = 0;
        /// <summary>
        /// Current value of the progress data
        /// </summary>
        public int Current
        {
            get { return _current; }
            set { _current = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Calculate the step increment given the number of steps
        /// </summary>
        /// <param name="number_steps">An <c>integer</c> representing the number of steps between the minimum and maximum</param>
        public void CalculateIncrement(int number_steps)
        {
            if (number_steps == 0)
                _increment = _maximum - _minimum;
            else
                _increment = Math.Abs((int)((_maximum - _minimum) / number_steps));
        }
        #endregion

    }
}
