﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IENetP.Generic
{
    /// <summary>
    /// Identification information about a particular object
    /// </summary>
    public class Identification
    {
        #region Constructors

        /// <summary>
        /// Initialize an object identification
        /// </summary>
        public Identification()
        {
        }

        /// <summary>
        /// Create an object identification with the basic information
        /// </summary>
        /// <param name="id">An <c>Int32</c> containing the ID number</param>
        /// <param name="name_string">A <c>string</c> containing the name of the object</param>
        public Identification(Int32 id, string name_string)
        {
            IdNumber = id;
            Name = name_string;
            Description = "";
            Comment = "";
        }

        /// <summary>
        /// Create an object identification with the full set of information
        /// </summary>
        /// <param name="id">An <c>Int32</c> containing the ID number</param>
        /// <param name="name_string">A <c>string</c> containing the name of the object</param>
        /// <param name="descr_string">A <c>string</c> containing a brief description</param>
        /// <param name="comm_string">A <c>string</c> containing additional comments</param>
        public Identification(Int32 id, string name_string, string descr_string, string comm_string)
        {
            IdNumber = id;
            Name = name_string;
            Description = descr_string;
            Comment = comm_string;
        }

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        private int _id_number = 0;
        /// <summary>
        /// Identification number for the object
        /// </summary>
        /// <value>An <c>int</c> containing the identification number for the object</value>
        public int IdNumber
        {
            get { return _id_number; }
            set { _id_number = value; }
        }

        private string _name = "";
        /// <summary>
        /// Human-readable name for the object
        /// </summary>
        /// <value>A <c>string</c> containing the name of the object</value>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _description = "";
        /// <summary>
        /// Brief description of the object
        /// </summary>
        /// <value>A <c>string</c> containing a brief description of the object</value>
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _comment = "";
        /// <summary>
        /// Additional comments about the object
        /// </summary>
        /// <value>A <c>string</c> containing any additional comments about the object</value>
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// String representing the object's identification information
        /// </summary>
        /// <returns>A <c>string</c> containing the identification information for the object</returns>
        public override string ToString()
        {
            string output = string.Format("ID={X}", IdNumber).PadLeft(8, '0');
            output += " | Name=" + Name;
            return output;
        }

        #endregion
    }

    /// <summary>
    /// Identification information about a particular device
    /// </summary>
    public class DeviceIdentification : Identification
    {
        #region Constructors

        /// <summary>
        /// Initialize a device identification object
        /// </summary>
        public DeviceIdentification()
            : base()
        {
        }

        /// <summary>
        /// Create a device identification object with the basic information
        /// </summary>
        /// <param name="id">An <c>Int32</c> containing the ID number</param>
        /// <param name="name_string">A <c>string</c> containing the name of the device</param>
        /// <param name="manuf_string">A <c>string</c> containing the manufacturer's name</param>
        /// <param name="model_string">A <c>string</c> containing the model number</param>
        public DeviceIdentification(Int32 id, string name_string, string manuf_string, string model_string)
            : base(id, name_string)
        {
            Manufacturer = manuf_string;
            Model = model_string;
        }

        /// <summary>
        /// Create a device identification object with the full set of information
        /// </summary>
        /// <param name="id">An <c>Int32</c> containing the ID number</param>
        /// <param name="name_string">A <c>string</c> containing the name of the device</param>
        /// <param name="descr_string">A <c>string</c> containing a brief description</param>
        /// <param name="comm_string">A <c>string</c> containing additional comments</param>
        /// <param name="manuf_string">A <c>string</c> containing the manufacturer's name</param>
        /// <param name="model_string">A <c>string</c> containing the model number</param>
        /// <param name="sn_string">A <c>string</c> containing the serial number</param>
        /// <param name="fw_string">A <c>string</c> containing the firmware version</param>
        public DeviceIdentification(Int32 id, string name_string, string descr_string, string comm_string,
            string manuf_string, string model_string, string sn_string, string fw_string)
            : base(id, name_string, descr_string, comm_string)
        {
            Manufacturer = manuf_string;
            Model = model_string;
            SerialNumber = sn_string;
            FirmwareVersion = fw_string;
        }

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        private string _manufacturer = "";
        /// <summary>
        /// Human-readable name of the manufacturer of the device
        /// </summary>
        /// <value>A <c>string</c> containing the name of the manufacturer of the device</value>
        public string Manufacturer
        {
            get { return _manufacturer; }
            set { _manufacturer = value; }
        }

        private string _model = "";
        /// <summary>
        /// Model number of the device
        /// </summary>
        /// <value>A <c>string</c> containing the model number of the device</value>
        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }

        private string _serial_number = "";
        /// <summary>
        /// Serial number of the device
        /// </summary>
        /// <value>A <c>string</c> containing the serial number of the device</value>
        public string SerialNumber
        {
            get { return _serial_number; }
            set { _serial_number = value; }
        }

        private string _firmware_version = "";
        /// <summary>
        /// Firmware version of the device
        /// </summary>
        /// <value>A <c>string</c> containing the firmware version of the device</value>
        public string FirmwareVersion
        {
            get { return _firmware_version; }
            set { _firmware_version = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// String representing the device's identification information
        /// </summary>
        /// <returns>A <c>string</c> representing the device's identification information</returns>
        public override string ToString()
        {
            string output = string.Format("ID={X}", IdNumber).PadLeft(8, '0');
            output += " | Manuf=" + Manufacturer + " | Name=" + Name;
            return output;
        }

        #endregion
    }
}
