﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IENetP.Generic
{
    public class TimeUnit
    {
        #region Constructors

        /// <summary>
        /// Time unit using default values
        /// </summary>
        public TimeUnit()
        {
        }

        /// <summary>
        /// Time unit with the desired values
        /// </summary>
        /// <param name="abbreviation">A <c>string</c> defining the abbreviation for the time unit</param>
        /// <param name="full_name">A <c>string</c> defining the full name for the time unit</param>
        /// <param name="multiplier">A <c>double</c> value representing the multiplier used to conver the time unit to seconds</param>
        public TimeUnit(string abbreviation, string full_name, double multiplier)
        {
            _abbreviation = abbreviation;
            _full_name = full_name;
            _multiplier = multiplier;
        }

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        private string _abbreviation = "";
        /// <summary>
        /// Abbreviation for the time unit
        /// </summary>
        public string Abbreviation
        {
            get { return _abbreviation; }
            set { _abbreviation = value; }
        }

        private string _full_name = "";
        /// <summary>
        /// Full name for the time unit
        /// </summary>
        public string FullName
        {
            get { return _full_name; }
            set { _full_name = value; }
        }

        private double _multiplier = 1;
        /// <summary>
        /// Multiplier used to convert the time unit to seconds
        /// </summary>
        public double Multiplier
        {
            get { return _multiplier; }
            set { _multiplier = value; }
        }

        #endregion

        #region Methods

        #endregion
        
        #region Static Read-Only

        public static readonly TimeUnit Hour = new TimeUnit("hr", "Hour", 60 * 60);
        public static readonly TimeUnit Minute = new TimeUnit("min", "Minute", 60);
        public static readonly TimeUnit Second = new TimeUnit("s", "Second", 1);
        public static readonly TimeUnit Millisecond = new TimeUnit("ms", "Millisecond", 10 ^ -3);
        public static readonly TimeUnit Microsecond = new TimeUnit("us", "Microsecond", 10 ^ -6);
        public static readonly TimeUnit Nanosecond = new TimeUnit("ns", "Nanosecond", 10 ^ -9);
        public static readonly TimeUnit Picosecond = new TimeUnit("ps", "Picosecond", 10 ^ -12);
        
        #endregion

    }
}
