﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace IENetP.Generic
{
    /// <summary>
    /// Class to define the constants used in the IENetP software.
    /// </summary>
    public class Constants
    {

        #region Statistics Constants

        /// <summary>
        /// Default minimum number of points necessary to calculate statistics for the data set
        /// </summary>
        public const int StatisticsThreshholdDefault = 10;

        /// <summary>
        /// Default number of points to remove from the data set before calculating statistics
        /// </summary>
        /// <remarks>The first point in the data set is always a problem, since the relative time for the point may not be 
        /// relative to the last packet, it may be relative to the beginning of the data file.  Also, there may be an initial 
        /// point or two that are slower or faster than normal during the connection initiation procedure.  Remove at least the 
        /// first 2 points, and possibly more.</remarks>
        public const int StatisticsRemoveLeadingDefault = 2;

        /// <summary>
        /// Number of decimal places to round statistical values
        /// </summary>
        /// <remarks>Wireshark only uses 6 decimal places for its times, without rounding the numbers, there are round-off 
        /// errors that creep into the calculations</remarks>
        public const int StatisticsDecimalPlaces = 6;

        /// <summary>
        /// Number of decimal places to offset the final output data
        /// </summary>
        /// <remarks>All Wireshark timing data is based on seconds, but most of the real-time performance data 
        /// is based on milliseconds.  This value represents the default 10-based exponent to use as a multiplier
        /// for the data.</remarks>
        public const int StatisticsDecimalOffsetExponentDefault = -3;

        #endregion

        #region Chart Bars

        /// <summary>
        /// Graph bar #1 percentage multiplier = 10%
        /// </summary>
        public const int Bar1PercentDefault = 10;

        /// <summary>
        /// Graph bar #1 bar color
        /// </summary>
        public static System.Drawing.Color Bar1ColorDefault = System.Drawing.Color.FromKnownColor(KnownColor.PaleTurquoise);

        /// <summary>
        /// Graph bar #1 enabled = true
        /// </summary>
        public static bool Bar1EnabledDefault = true;

        /// <summary>
        /// Graph bar #2 percentage multiplier = 25%
        /// </summary>
        public const int Bar2PercentDefault = 25;

        /// <summary>
        /// Graph bar #2 bar color
        /// </summary>
        public static System.Drawing.Color Bar2ColorDefault = System.Drawing.Color.FromKnownColor(KnownColor.LightSkyBlue);

        /// <summary>
        /// Graph bar #2 enabled = true
        /// </summary>
        public static bool Bar2EnabledDefault = true;

        /// <summary>
        /// Graph bar #3 percentage multiplier = 50%
        /// </summary>
        public const int Bar3PercentDefault = 50;

        /// <summary>
        /// Graph bar #3 bar color
        /// </summary>
        public static System.Drawing.Color Bar3ColorDefault = System.Drawing.Color.FromKnownColor(KnownColor.LightSeaGreen);

        /// <summary>
        /// Graph bar #3 enabled = true
        /// </summary>
        public static bool Bar3EnabledDefault = true;

        /// <summary>
        /// Graph bar #4 percentage multiplier = 100%
        /// </summary>
        public const int Bar4PercentDefault = 100;

        /// <summary>
        /// Graph bar #4 bar color
        /// </summary>
        public static System.Drawing.Color Bar4ColorDefault = System.Drawing.Color.FromKnownColor(KnownColor.LightGreen);

        /// <summary>
        /// Graph bar #4 enabled = true
        /// </summary>
        public static bool Bar4EnabledDefault = true;

        /// <summary>
        /// Graph bar #5 percentage multiplier = 200%
        /// </summary>
        public const int Bar5PercentDefault = 200;

        /// <summary>
        /// Graph bar #5 bar color
        /// </summary>
        public static System.Drawing.Color Bar5ColorDefault = System.Drawing.Color.FromKnownColor(KnownColor.LightYellow);

        /// <summary>
        /// Graph bar #5 enabled = true
        /// </summary>
        public static bool Bar5EnabledDefault = true;

        /// <summary>
        /// Graph bar #6 percentage multiplier = 400%
        /// </summary>
        public const int Bar6PercentDefault = 400;

        /// <summary>
        /// Graph bar #6 bar color
        /// </summary>
        public static System.Drawing.Color Bar6ColorDefault = System.Drawing.Color.FromKnownColor(KnownColor.LightSalmon);

        /// <summary>
        /// Graph bar #6 enabled = true
        /// </summary>
        public static bool Bar6EnabledDefault = true;

        /// <summary>
        /// Graph bar #7 percentage multiplier = 996%
        /// </summary>
        public const int Bar7PercentDefault = 996;

        /// <summary>
        /// Graph bar #7 bar color
        /// </summary>
        public static System.Drawing.Color Bar7ColorDefault = System.Drawing.Color.FromKnownColor(KnownColor.Coral);

        /// <summary>
        /// Graph bar #7 enabled = false
        /// </summary>
        public static bool Bar7EnabledDefault = false;

        /// <summary>
        /// Graph bar #8 percentage multiplier = 997%
        /// </summary>
        public const int Bar8PercentDefault = 997;

        /// <summary>
        /// Graph bar #8 bar color
        /// </summary>
        public static System.Drawing.Color Bar8ColorDefault = System.Drawing.Color.FromKnownColor(KnownColor.White);

        /// <summary>
        /// Graph bar #8 enabled = false
        /// </summary>
        public static bool Bar8EnabledDefault = false;

        /// <summary>
        /// Graph bar #9 percentage multiplier = 998%
        /// </summary>
        public const int Bar9PercentDefault = 998;

        /// <summary>
        /// Graph bar #9 bar color
        /// </summary>
        public static System.Drawing.Color Bar9ColorDefault = System.Drawing.Color.FromKnownColor(KnownColor.White);

        /// <summary>
        /// Graph bar #9 enabled = false
        /// </summary>
        public static bool Bar9EnabledDefault = false;

        /// <summary>
        /// Graph bar #10 percentage multiplier = 999%
        /// </summary>
        public const int Bar10PercentDefault = 999;

        /// <summary>
        /// Graph bar #10 bar color
        /// </summary>
        public static System.Drawing.Color Bar10ColorDefault = System.Drawing.Color.FromKnownColor(KnownColor.White);

        /// <summary>
        /// Graph bar #10 enabled = false
        /// </summary>
        public static bool Bar10EnabledDefault = false;

        #endregion

        #region Control Colors

        /// <summary>
        /// Background color brush to use when a normal TextBox is blank.
        /// </summary>
        public static System.Windows.Media.Brush TextBoxBackgroundBrushBlank = System.Windows.Media.Brushes.White;

        /// <summary>
        /// Background color brush to use when a normal TextBox is blank and read-only.
        /// </summary>
        public static System.Windows.Media.Brush TextBoxBackgroundBrushReadOnly = System.Windows.Media.Brushes.LightSteelBlue;

        /// <summary>
        /// Background color brush to use when a TextBox contains a valid value.
        /// </summary>
        public static System.Windows.Media.Brush TextBoxBackgroundBrushValid = System.Windows.Media.Brushes.PaleGreen;

        /// <summary>
        /// Background color brush to use when a TextBox contains an invalid value.
        /// </summary>
        public static System.Windows.Media.Brush TextBoxBackgroundBrushInvalid = System.Windows.Media.Brushes.LightSalmon;

        #endregion

        #region Default Wireshark Filters

        public static string DefaultWiresharkFilterEtherNetIPClass1Multicast = "(ip.dst>=239.192.0.0 && ip.dst<=239.192.255.255)";
        public static string DefaultWiresharkFilterIpSourcePart1 = "(ip.src==";
        public static string DefaultWiresharkFilterIpSourcePart2 = ")";

        #endregion

        #region Time Constants

        /// <summary>
        /// Number of milliseconds to sleep a task while waiting for it to complete its task
        /// </summary>
        public const int ThreadSleepTime = 100;

        /// <summary>
        /// Number of milliseconds to sleep while waiting for the analysis window to popup
        /// </summary>
        public const int AnalysisWindowPopup = 100;

        #endregion

        #region Files

        /// <summary>
        /// Environment variable to request when determining user temporary directory
        /// </summary>
        public const string TemporaryFileEnvironmentVariable = "TEMP";

        /// <summary>
        /// When generating IENetP files, use this header string
        /// </summary>
        public const string IENetPFileHeader = "IENetP_";

        /// <summary>
        /// When generating the Wireshark Fields file, use this filename
        /// </summary>
        public const string WiresharkFieldsFileName = "Wireshark_Fields";

        /// <summary>
        /// File extension for text files
        /// </summary>
        public const string TextFileExtension = ".txt";

        /// <summary>
        /// File extension for XML files
        /// </summary>
        public const string XmlFileExtension = ".xml";

        /// <summary>
        /// File extension to use when searching for any type of file
        /// </summary>
        public const string AnyFileExtension = ".*";

        #endregion

        #region Field Type Lengths

        /// <summary>
        /// Length of the Ethernet MAC address field type (in bytes)
        /// </summary>
        public const int FieldTypeEtherLen = 6;

        /// <summary>
        /// Length of the Globally Unique Identifier field type (in bytes)
        /// </summary>
        public const int FieldTypeGUIDLen = 16;

        /// <summary>
        /// Length of the Internet Protocol version 4 address field type (in bytes)
        /// </summary>
        public const int FieldTypeIPv4Len = 4;

        /// <summary>
        /// Length of the Internet Protocol version 6 address field type (in bytes)
        /// </summary>
        public const int FieldTypeIPv6Len = 16;

        /// <summary>
        /// Length of the IPX Network address field type (in bytes)
        /// </summary>
        public const int FieldTypeIPXNetLen = 4;

        #endregion

    }
}
