using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace IENetP.Generic
{
	/// <summary>
	/// Thread wrapper class to abstract more complex thread manipulation functions
	/// </summary>
	/// <remarks>
	/// Most of this code is based on the book titled "Pro .NET 2.0 Windows Forms and Custom Controls in C#" from Apress.  
	/// As such, the code contained in this class is subject to whatever licenses are relevant for that code.  
	/// (See http://www.apress.com/ for more information about this book and the source code license.)
	/// </remarks>
	public abstract class ThreadWrapper
	{
		#region Constructors

		#endregion

		#region Data Objects

		private Thread _thread;

		#endregion

		#region Properties

		private Enums.ThreadStatusState _status = Enums.ThreadStatusState.Unstarted;
		/// <summary>
		/// Status of the thread
		/// </summary>
        public Enums.ThreadStatusState Status
		{
			get { return _status; }
            protected set { _status = value; }
		}

		private Guid _id = Guid.NewGuid();
		/// <summary>
		/// Unique thread identifier
		/// </summary>
		public Guid ID
		{
			get { return _id; }
		}

		private bool _halt_requested = false;
		/// <summary>
		/// Flag to indicate that a halt has been requested by the user
		/// </summary>
		public bool HaltRequested
		{
			get { return _halt_requested; }
		}

		private TimeSpan _halt_wait_time = TimeSpan.Zero;
		/// <summary>
		/// Time that the thread will wait before aborting the thread if it hasn't stopped gracefully in response to a cancellation message
		/// </summary>
		/// <value><c>TimeSpan.Zero</c> should be used when a graceful shutdown of the thread is not necessary.</value>
		public TimeSpan HaltWaitTime
		{
			get { return _halt_wait_time; }
			set { _halt_wait_time = value; }
		}

		private int _halt_check_interval = 5;
		/// <summary>
		/// Number of seconds to wait between halt checks
		/// </summary>
		public int HaltCheckInterval
		{
			get { return _halt_check_interval; }
			set { _halt_check_interval = value; }
		}

		private bool _supports_progress = true;
		/// <summary>
		/// Does the thread support progress reporting?
		/// </summary>
		protected bool SupportsProgress
		{
			get { return _supports_progress; }
			set { _supports_progress = value; }
		}

		/// <summary>
		/// Local protected variable to hold the progress percentage
		/// </summary>
		protected int _progress = 0;
		/// <summary>
		/// Progress of the thread in percent
		/// </summary>
		/// <value>Between zero (0) and one hundred (100)</value>
		/// <remarks>Will throw an exception if the task does not support progress</remarks>
		public int Progress
		{
			get
			{
				if (!_supports_progress)
					throw new InvalidOperationException("This thread does not report progess.");
				else
					return _progress;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Function that defines the actual task to run
		/// </summary>
		/// <remarks>Override for each thread in order to define the task logic</remarks>
		protected abstract void DoTask();

		/// <summary>
		/// Function to call after the task completes
		/// </summary>
		/// <remarks>Override for each thread in order to define the callback logic</remarks>
		protected abstract void OnCompleted();

		/// <summary>
		/// Start the thread worker task
		/// </summary>
		public void StartTask()
		{
            if (_status == Enums.ThreadStatusState.InProgress)
			{
				throw new InvalidOperationException("Already in progress.");
			}
			else
			{
				// Initialize the new task.
                _status = Enums.ThreadStatusState.InProgress;

				// Create the thread and run it in the background,
				// so it will terminate automatically if the application ends.
				_thread = new Thread(StartTaskAsync);
				_thread.IsBackground = true;

				// Start the thread.
				_thread.Start();
			}
		}

		/// <summary>
		/// Basic procedure for the asynchronous thread task
		/// </summary>
		private void StartTaskAsync()
		{
			DoTask();
            _status = Enums.ThreadStatusState.Completed;
			OnCompleted();
		}

		/// <summary>
		/// Stop the thread task while running
		/// </summary>
		/// <remarks>Thread will attempt to wait for a certain amount of time before forcing a halt</remarks>
		public void StopTask()
		{
			// Perform no operation if task isn't running.
            if (_status != Enums.ThreadStatusState.InProgress) return;

			// Try the polite approach.
			if (_halt_wait_time != TimeSpan.Zero)
			{
				DateTime startTime = DateTime.Now;
				while (DateTime.Now.Subtract(startTime).TotalSeconds > 0)
				{
					// Still waiting for the time limit to pass.
					// Allow other threads to do some work.
					Thread.Sleep(TimeSpan.FromSeconds(_halt_check_interval));
				}
			}

			// Use the forced approach.
			_thread.Abort();
		}

		// TODO: Add in code for pause and resume here

		#endregion



	}
}
