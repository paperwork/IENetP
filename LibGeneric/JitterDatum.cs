﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IENetP.Wireshark
{
    /// <summary>
    /// Datum point for Jitter performance analysis
    /// </summary>
    public class JitterDatum
    {
		#region Constructors

		/// <summary>
		/// Default datum point for jitter performance analysis
		/// </summary>
		public JitterDatum()
		{
			_index = 0;
			_datum_value = 0;
		}

		/// <summary>
		/// Datum point for jitter performance analysis
		/// </summary>
		/// <param name="ind">An <c>int</c> representing the index number</param>
		/// <param name="datum">A <c>double</c> representing the datum value</param>
        public JitterDatum(int ind, double datum)
		{
			_index = ind;
			_datum_value = datum;
		}

		#endregion

        #region Data Objects

        #endregion

        #region Properties

        private int _index;
		/// <summary>
		/// Index number for the datum
		/// </summary>
		public int Index
		{
			get { return _index; }
			set { _index = value; }
		}

		private double _datum_value;
		/// <summary>
		/// Value of the datum
		/// </summary>
		public double DatumValue
		{
			get { return _datum_value; }
			set { _datum_value = value; }
		}

		#endregion

        #region Methods

        #endregion
    }
}
