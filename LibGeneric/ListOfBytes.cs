﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace IENetP.Generic
{
    /// <summary>
    /// Base class for all lists of bytes
    /// </summary>
    public abstract class ListOfBytes
    {

        #region Constructors

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        protected int _list_length = 0;
        /// <summary>
        /// Gets or sets the length of the list.
        /// </summary>
        /// <value>An <c>int</c> that represents the desired length of the list.</value>
        /// <remarks>This is useful when validating that a parsed list of bytes is required
        /// to be a certain length, as in a network address.</remarks>
        public int ListLength
        {
            get { return _list_length; }
            protected set { _list_length = value; }
        }

        protected List<byte> _byte_list = new List<byte>();
        /// <summary>
        /// Gets or sets a list containing the individual bytes.
        /// </summary>
        /// <value>A list of bytes containing the individual byte values.</value>
        public List<byte> ByteList
        {
            get { return _byte_list; }
            set
            {
                List<byte> list = new List<byte>();
                list = value;

                if (list.Count != ListLength)
                    IsValid = false;

                if (IsValid == true)
                    _byte_list = list;
                else
                    _byte_list = new List<byte>();
            }
        }

        protected List<char> _delimiter_list = new List<char>();
        /// <summary>
        /// Gets or sets a list containing the possible string delimiters for the list of bytes.
        /// </summary>
        /// <value>A list of chars containing all of the possible delimiters that can be used to separate
        /// the individual values in a string representation of the list of bytes.</value>
        public List<char> DelimiterList
        {
            get { return _delimiter_list; }
            protected set { _delimiter_list = value; }
        }

        protected int _delimiter_index = 0;
        /// <summary>
        /// Gets or sets the string delimiter index.
        /// </summary>
        /// <value>An zero-based <c>int</c> that represents the currently selected delimiter to use when
        /// generating a string representation of the list of bytes.</value>
        /// <remarks><c>DelimiterList</c> is required be set before the <c>DelimiterIndex</c> value is assigned.</remarks>
        public int DelimiterIndex
        {
            get { return _delimiter_index; }
            set
            {
                if (value < _delimiter_list.Count)
                    _delimiter_index = value;
                else
                    _delimiter_index = 0;
            }
        }

        protected int _number_of_bytes_per_delimiter = 1;
        /// <summary>
        /// Gets or sets the number of bytes to show in a string representation of the list between delimiters.
        /// </summary>
        /// <value>An <c>int</c> representing the number of bytes to show in a string representation of the list
        /// between delimiters.</value>
        /// <remarks>This may be used in cases like an IPv6 address where two bytes are displayed per delimiter.</remarks>
        public int NumberOfBytesPerDelimiter
        {
            get { return _number_of_bytes_per_delimiter; }
            protected set { _number_of_bytes_per_delimiter = value; }
        }

        protected bool _is_hex = false;
        /// <summary>
        /// Gets or sets a value indicating whether the list of bytes uses hexadecimal numbers or not.
        /// </summary>
        /// <value>A <c>bool</c> indicating whether the list of bytes uses hexadecimal numbers or not.</value>
        public bool IsHex
        {
            get { return _is_hex; }
            protected set { _is_hex = value; }
        }

        protected bool _padded = false;
        /// <summary>
        /// Gets or sets a value indicating whether leading zeros (0) should be included in a string representation 
        /// of the list of bytes.
        /// </summary>
        /// <value>A <c>bool</c> indicating whether leading zeros (0) should be included in a string representation
        /// of the list of bytes.</value>
        /// <remarks>For example, the IPv4 address 1.2.3.4 would be displayed as 001.002.003.004.</remarks>
        public bool Padded
        {
            get { return _padded; }
            set { _padded = value; }
        }

        protected bool? _is_valid = null;
        /// <summary>
        /// Gets or sets a value indicating whether the list of bytes is valid.
        /// </summary>
        /// <value>A <c>bool?</c> that represents whether the current list of bytes is valid or not.</value>
        public bool? IsValid
        {
            get { return _is_valid; }
            protected set { _is_valid = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Parse the string, output the list of bytes, and returns a value indicating whether the string was parsed correctly.
        /// </summary>
        /// <param name="address_string">A <c>string</c> representing the list of bytes</param>
        /// <param name="list">The list in which to store the bytes.</param>
        /// <returns>A <c>bool</c> representing whether the string was parsed correctly.</returns>
        protected virtual bool TryParse(string address_string, out List<byte> list)
        {
            bool output = false;
            list = new List<byte>();
            byte val;
            bool bad_data = false;
            List<byte> output_list = new List<byte>();

            // Convert the delimiter list to an array.
            char[] del_array = new char[_delimiter_list.Count];
            _delimiter_list.CopyTo(del_array);

            // Check to see if the address string is empty
            if (String.IsNullOrEmpty(address_string))
            {
                // If so, then the address could not be parsed correctly
                return false;
            }

            // Trim any white space and extraneous delimiters from the address string.
            address_string.Trim();
            address_string.Trim(del_array);

            // Recheck to see if the address string is empty after trimming
            if (string.IsNullOrEmpty(address_string))
            {
                // If so, then the address could not be parsed correctly
                return false;
            }

            // Split the address string into an array using the list of possible delimiters.
            string[] address_array = address_string.Split(del_array);

            // If there are no delimiters used in the network address.
            if (address_array.Length == 1)
            {
                // Only accept this if the value is in hexadecimal
                if (_is_hex)
                {
                    string addr = address_array[0];

                    // Check to make sure that an even number of digits is being interpreted. If not, pad the string with a leading zero (0).
                    if ((addr.Length % 2) == 1)
                        addr = addr.PadLeft(addr.Length + 1, '0');

                    // Work through the string 2 digits at a time interpreting each as a hex digit.
                    while (addr.Length > 2)
                    {
                        if (byte.TryParse(addr.Remove(2), NumberStyles.HexNumber, null, out val))
                        {
                            output_list.Add(val);
                            addr = addr.Substring(2);
                        }
                        else
                        {
                            bad_data = true;
                            break;
                        }
                    }

                    if (!bad_data)
                        if (byte.TryParse(addr, NumberStyles.HexNumber, null, out val))
                            output_list.Add(val);

                    // Check to see if all the bytes in the network address were interpreted correctly and if the network address
                    // is the correct length.
                    if (!bad_data && output_list.Count == ListLength)
                    {
                        list = output_list;
                        output = true;
                    }
                }
            }
            else
            {
                bool ret_val;
                int i;

                for (i = 0; i < address_array.Length; i++)
                {
                    if (_is_hex)
                        ret_val = byte.TryParse(address_array[i], NumberStyles.HexNumber, null, out val);
                    else
                        ret_val = byte.TryParse(address_array[i], out val);

                    if (ret_val)
                        output_list.Add(val);
                    else
                        break;
                }

                if (i == address_array.Length && address_array.Length == _list_length)
                {
                    list = output_list;
                    output = true;
                }
            }

            return output;
        }

        /// <summary>
        /// Output a string representing the network address.
        /// </summary>
        /// <returns>A <c>string</c> representing the network address in its desired format.</returns>
        public override string ToString()
        {
            string output = "";

            if (_is_valid == false)
                output = "Invalid!";
            else
            {
                int c = 0;
                char delimiter = _delimiter_list[_delimiter_index];
                string val;
                for (int i = 0; i < _byte_list.Count; i++)
                {
                    if (c >= _number_of_bytes_per_delimiter)
                    {
                        output += delimiter;
                        c = 0;
                    }

                    if (_is_hex)
                    {
                        val = string.Format("{0:X}", _byte_list[i]);
                        if (_padded)
                            val.PadLeft(2, '0');
                    }
                    else
                    {
                        val = string.Format("{0}", _byte_list[i]);
                        if (_padded)
                            val.PadLeft(3, '0');
                    }
                    output += val;
                    c++;
                }
            }

            return output;
        }

        /// <summary>
        /// Abstract function to initialize the delimiter list in each type of network address.
        /// </summary>
        protected abstract void SetDelimiterList();

        #endregion

    }

}
