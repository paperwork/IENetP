﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using IENetP.Generic;
using IENetP.Network;
using IENetP.Wireshark;

namespace IENetP
{
    /// <summary>
    /// Interaction logic for MetricsAndFilterWindow.xaml
    /// </summary>
    public partial class ProtocolProfilesWindow : Window
    {
        #region Constructors

        public ProtocolProfilesWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Data Objects

        private List<string> _protocols = new List<string>();
        private List<string> _header_fields = new List<string>();
        private List<string> _field_types = new List<string>();
        private List<TimeUnit> _time_units = new List<TimeUnit>();

        private ProtocolProfile _current_profile = new ProtocolProfile();
        private List<ProtocolProfile> _profiles = new List<ProtocolProfile>();

        private string valid_address_types = "";
        private string valid_timestamp_types = "";

        #endregion

        #region Properties

        private WiresharkFields _current_wireshark_fields = new WiresharkFields();
        /// <summary>
        /// Sets the current list of Wireshark protocols and header fields
        /// </summary>
        public WiresharkFields CurrentWiresharkFields
        {
            set { _current_wireshark_fields = value; }
        }

        private List<WiresharkFieldType> _wireshark_field_types = new List<WiresharkFieldType>();
        /// <summary>
        /// Sets the list of Wireshark field types
        /// </summary>
        public List<WiresharkFieldType> WiresharkFieldTypes
        {
            set { _wireshark_field_types = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Check the abbreviation for the core metric to make sure that it is a valid Wireshark header field.
        /// Colorize, populate, and enable the correct controls depending on that check.
        /// </summary>
        /// <returns><c>True</c> if the Wireshark protocol header field exists, <c>False</c> if not.</returns>
        /// <param name="check_box"><c>CheckBox</c> where the user decides to override the desired core metric.</param>
        /// <param name="abbr_box"><c>TextBox</c> where the user enters the abbreviation for the new desired core metric.</param>
        /// <param name="field_type"><c>TextBlock</c> where the software displays the proper Wireshark header field type for the desired new core metric.</param>
        /// <param name="validation_type">What type of validation should be used when evaluating the new desired core metric?</param>
        /// <param name="header_index">An <c>int</c> containing the index value pointing to the proper Wireshark header field.</param>
        /// <param name="type_index">An <c>int</c> containing the index value pointing to the proper Wireshark header field type.</param>
        private bool CheckWiresharkFieldCoreMetric(CheckBox check_box, TextBox abbr_box, TextBlock field_type, Enums.ValidateWiresharkFieldType validation_type, out int header_index, out int type_index)
        {
            bool enable_timestamp_units = comboBoxCoreMetricTimeOverrideUnits.IsEnabled;
            ToolTip tip = new ToolTip();
            header_index = -1;
            type_index = -1;
            if (check_box.IsChecked == true)
            {
                abbr_box.IsEnabled = true;
                field_type.IsEnabled = true;
                if (!CheckWiresharkProtocolMetric(abbr_box, null, null, null, out header_index, out type_index))
                {
                    tip.Content = "Please enter a valid Wireshark header field.";
                    abbr_box.ToolTip = tip;
                    abbr_box.Background = Constants.TextBoxBackgroundBrushInvalid;
                    field_type.Text = "";
                    if (Enums.ValidateWiresharkFieldType.Timestamp == validation_type)
                        enable_timestamp_units = false;
                    return false;
                }
                else
                {
                    field_type.Text = _wireshark_field_types[type_index].Name;
                    if (Enums.ValidateWiresharkFieldType.Address == validation_type)
                    {
                        if (_wireshark_field_types[type_index].IsValidAddress)
                        {
                            tip.Content = abbr_box.Text + " is a valid address.";
                            abbr_box.Background = Constants.TextBoxBackgroundBrushValid;
                        }
                        else
                        {
                            tip.Content = abbr_box.Text + " is not a valid address.\nValid address types include: " + valid_address_types + ".";
                            abbr_box.Background = Constants.TextBoxBackgroundBrushInvalid;
                            return false;
                        }
                    }
                    else if (Enums.ValidateWiresharkFieldType.Timestamp == validation_type)
                    {
                        if (_wireshark_field_types[type_index].IsValidTimestamp)
                        {
                            tip.Content = abbr_box.Text + " is a valid timestamp.";
                            abbr_box.Background = Constants.TextBoxBackgroundBrushValid;
                            enable_timestamp_units = true;
                        }
                        else
                        {
                            tip.Content = abbr_box.Text + " is not a valid timestamp.\nValid timestamp types include: " + valid_timestamp_types + ".";
                            abbr_box.Background = Constants.TextBoxBackgroundBrushInvalid;
                            enable_timestamp_units = false;
                            return false;
                        }
                    }
                    else
                    {
                        tip.Content = abbr_box.Text + " is a valid header field.";
                        abbr_box.Background = Constants.TextBoxBackgroundBrushValid;
                    }
                    abbr_box.ToolTip = tip;
                }
            }
            else
            {
                abbr_box.ToolTip = null;
                abbr_box.IsEnabled = false;
                field_type.IsEnabled = false;
                abbr_box.Background = Constants.TextBoxBackgroundBrushReadOnly;
                enable_timestamp_units = false;
                return false;
            }
            comboBoxCoreMetricTimeOverrideUnits.IsEnabled = enable_timestamp_units;
            return true;
        }

        /// <summary>
        /// Check the abbreviation for the core metric to make sure that it is a valid Wireshark header field.
        /// Colorize, populate, and enable the correct controls depending on that check.
        /// </summary>
        /// <returns><c>True</c> if the Wireshark protocol header field exists, <c>False</c> if not.</returns>
        /// <param name="check_box"><c>CheckBox</c> where the user decides to override the desired core metric.</param>
        /// <param name="abbr_box"><c>TextBox</c> where the user enters the abbreviation for the new desired core metric.</param>
        /// <param name="field_type"><c>TextBlock</c> where the software displays the proper Wireshark header field type for the desired new core metric.</param>
        /// <param name="validation_type">What type of validation should be used when evaluating the new desired core metric?</param>
        private bool CheckWiresharkFieldCoreMetric(CheckBox check_box, TextBox abbr_box, TextBlock field_type, Enums.ValidateWiresharkFieldType validation_type)
        {
            int header_index = -1;
            int type_index = -1;
            return CheckWiresharkFieldCoreMetric(check_box, abbr_box, field_type, validation_type, out header_index, out type_index);
        }
        
        /// <summary>
        /// Check the abbreviation for the wireshark metric to make sure that it is a valid Wireshark header field.
        /// Colorize and populate the correct controls depending on that check.
        /// </summary>
        /// <returns><c>True</c> if the Wireshark protocol header field exists, <c>False</c> if not.</returns>
        /// <param name="abbr_box"><c>TextBox</c> where the user enters the desired protocol metric abbreviation.</param>
        /// <param name="name_box"><c>TextBlock</c> where the software displays the proper Wireshark header field name for the desired protocol metric.</param>
        /// <param name="desc_box"><c>TextBlock</c> where the software displays the proper Wireshark header field description for the desired protocol metric.</param>
        /// <param name="type_box"><c>TextBlock</c> where the software displays the proper Wireshark header field type for the desired protocol metric.</param>
        /// <param name="header_index">An <c>int</c> containing the index value pointing to the proper Wireshark header field.</param>
        /// <param name="type_index">An <c>int</c> containing the index value pointing to the proper Wireshark header field type.</param>
        /// <remarks>Use 'null' for any of the output Textboxes that do not exist.</remarks>
        private bool CheckWiresharkProtocolMetric(TextBox abbr_box, TextBox name_box, TextBox desc_box, TextBox type_box, out int header_index, out int type_index)
        {
            bool ret_val = false;
            String name_text = "";
            String desc_text = "";
            String type_text = "";

            ToolTip tip = new ToolTip();
            abbr_box.Text = abbr_box.Text.Trim();
            if (string.IsNullOrEmpty(abbr_box.Text))
            {
                tip.Content = "Please enter a valid Wireshark header field.";
                abbr_box.ToolTip = tip;
                abbr_box.Background = Constants.TextBoxBackgroundBrushBlank;
                name_text = "";
                desc_text = "";
                type_text = "";
                header_index = -1;
                type_index = -1;

                ret_val = false;
            }
            else
            {
                if (_header_fields.Contains(abbr_box.Text))
                {
                    abbr_box.Background = Constants.TextBoxBackgroundBrushValid;
                    tip.Content = abbr_box.Text + " is a valid header field.";
                    abbr_box.ToolTip = tip;
                    header_index = _header_fields.IndexOf(abbr_box.Text);
                    name_text = _current_wireshark_fields.WiresharkHeaderFields[header_index].Name;
                    desc_text = _current_wireshark_fields.WiresharkHeaderFields[header_index].FieldDescription;
                    type_index = _field_types.IndexOf(_current_wireshark_fields.WiresharkHeaderFields[header_index].Type);
                    type_text = _wireshark_field_types[type_index].Name;

                    ret_val = true;
                }
                else
                {
                    tip.Content = abbr_box.Text + " is not a valid header field.";
                    abbr_box.ToolTip = tip;
                    abbr_box.Background = Constants.TextBoxBackgroundBrushInvalid;
                    name_text = "";
                    desc_text = "";
                    type_text = "";
                    header_index = -1;
                    type_index = -1;

                    ret_val = false;
                }
            }

            if (name_box != null)
                name_box.Text = name_text;
            if (desc_box != null)
                desc_box.Text = desc_text;
            if (type_box != null)
                type_box.Text = type_text;

            return ret_val;
        }

        /// <summary>
        /// Check the abbreviation for the wireshark metric to make sure that it is a valid Wireshark header field.
        /// Colorize and populate the correct controls depending on that check.
        /// </summary>
        /// <returns><c>True</c> if the Wireshark protocol header field exists, <c>False</c> if not.</returns>
        /// <param name="abbr_box"><c>TextBox</c> where the user enters the desired protocol metric abbreviation.</param>
        /// <param name="name_box"><c>TextBlock</c> where the software displays the proper Wireshark header field name for the desired protocol metric.</param>
        /// <param name="desc_box"><c>TextBlock</c> where the software displays the proper Wireshark header field description for the desired protocol metric.</param>
        /// <param name="type_box"><c>TextBlock</c> where the software displays the proper Wireshark header field type for the desired protocol metric.</param>
        /// <remarks>Use 'null' for any of the output Textboxes that do not exist.</remarks>
        private bool CheckWiresharkProtocolMetric(TextBox abbr_box, TextBox name_box, TextBox desc_box, TextBox type_box)
        {
            int hf_index, ft_index;

            return CheckWiresharkProtocolMetric(abbr_box, name_box, desc_box, type_box, out hf_index, out ft_index);
        }

        /// <summary>
        /// Clear all the fields in the window to enter a new protocol profile.
        /// </summary>
        private void ClearAllFields()
        {
            textBoxName.Text = "";

            checkBoxCoreMetricTimeOverride.IsChecked = false;
            textBoxCoreMetricTimeOverride.Background = Constants.TextBoxBackgroundBrushReadOnly;
            textBoxCoreMetricTimeOverride.Text = "";
            textBlockCoreMetricTimeFieldType.Text = "";
            comboBoxCoreMetricTimeOverrideUnits.SelectedIndex = -1;

            checkBoxCoreMetricSrcAddressOverride.IsChecked = false;
            textBoxCoreMetricSrcAddressOverride.Background = Constants.TextBoxBackgroundBrushReadOnly;
            textBoxCoreMetricSrcAddressOverride.Text = "";
            textBlockCoreMetricSrcAddressFieldType.Text = "";

            checkBoxCoreMetricDstAddressOverride.IsChecked = false;
            textBoxCoreMetricDstAddressOverride.Background = Constants.TextBoxBackgroundBrushReadOnly;
            textBoxCoreMetricDstAddressOverride.Text = "";
            textBlockCoreMetricDstAddressFieldType.Text = "";

            listBoxProtocolMetric.Items.Clear();
            ClearProtocolMetricFields();

            textBoxProtocolFilter.Text = "";

            buttonProtocolAdd.IsEnabled = false;
            buttonProtocolUpdate.IsEnabled = false;
            buttonProtocolDelete.IsEnabled = false;

            _current_profile = new ProtocolProfile();
        }

        /// <summary>
        /// Clear all the protocol metric fields
        /// </summary>
        private void ClearProtocolMetricFields()
        {
            textBoxProtocolMetricAbbreviation.Text = "";
            textBoxProtocolMetricName.Text = "";
            textBoxProtocolMetricDescription.Text = "";
            textBoxProtocolMetricType.Text = "";
        }

        private void PopulateAllFields(int index)
        {
            int header_index = -1;
            int type_index = -1;
            ProtocolProfile profile = _profiles[index];

            textBoxName.Text = profile.Ident.Name;

            checkBoxCoreMetricTimeOverride.IsChecked = profile.OverridePacketTime;
            if (profile.OverridePacketTime)
            {
                textBoxCoreMetricTimeOverride.Text = profile.NewPacketTime.Abbreviation;
                CheckWiresharkFieldCoreMetric(checkBoxCoreMetricTimeOverride, textBoxCoreMetricTimeOverride, textBlockCoreMetricTimeFieldType, Enums.ValidateWiresharkFieldType.Timestamp, out header_index, out type_index);
                comboBoxCoreMetricTimeOverrideUnits.SelectedItem = profile.NewPacketTimeUnit.Abbreviation;
            }

            checkBoxCoreMetricSrcAddressOverride.IsChecked = profile.OverrideSourceAddress;
            if (profile.OverrideSourceAddress)
            {
                textBoxCoreMetricSrcAddressOverride.Text = profile.NewSourceAddress.Abbreviation;
                CheckWiresharkFieldCoreMetric(checkBoxCoreMetricSrcAddressOverride, textBoxCoreMetricSrcAddressOverride, textBlockCoreMetricSrcAddressFieldType, Enums.ValidateWiresharkFieldType.Address, out header_index, out type_index);
            }

            checkBoxCoreMetricDstAddressOverride.IsChecked = profile.OverrideDestinationAddress;
            if (profile.OverrideDestinationAddress)
            {
                textBoxCoreMetricDstAddressOverride.Text = profile.NewDestinationAddress.Abbreviation;
                CheckWiresharkFieldCoreMetric(checkBoxCoreMetricDstAddressOverride, textBoxCoreMetricDstAddressOverride, textBlockCoreMetricDstAddressFieldType, Enums.ValidateWiresharkFieldType.Address, out header_index, out type_index);
            }

            for (int i = 0; i < profile.ProtocolMetrics.Count; i++)
                listBoxProtocolMetric.Items.Add(profile.ProtocolMetrics[i].Abbreviation);
            listBoxProtocolMetric.SelectedIndex = -1;

            textBoxProtocolFilter.Text = profile.ProtocolFilter;

            _current_profile = profile;

            buttonProtocolAdd.IsEnabled = false;
            buttonProtocolUpdate.IsEnabled = true;
            buttonProtocolDelete.IsEnabled = true;
        }

        #endregion

        #region Event Handlers

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ClearAllFields();
            textBoxProtocolMetricName.Background = Constants.TextBoxBackgroundBrushReadOnly;
            textBoxProtocolMetricType.Background = Constants.TextBoxBackgroundBrushReadOnly;
            textBoxProtocolMetricDescription.Background = Constants.TextBoxBackgroundBrushReadOnly;

            for (int i = 0; i < _current_wireshark_fields.WiresharkProtocols.Count; i++)
                _protocols.Add(_current_wireshark_fields.WiresharkProtocols[i].Abbreviation);
            for (int i = 0; i < _current_wireshark_fields.WiresharkHeaderFields.Count; i++)
                _header_fields.Add(_current_wireshark_fields.WiresharkHeaderFields[i].Abbreviation);
            for (int i = 0; i < _wireshark_field_types.Count; i++)
                _field_types.Add(_wireshark_field_types[i].WiresharkType);

            // Populate the Timestamp units ComboBox with the available units
            _time_units.Add(TimeUnit.Second);
            _time_units.Add(TimeUnit.Millisecond);
            _time_units.Add(TimeUnit.Microsecond);
            _time_units.Add(TimeUnit.Nanosecond);
            for (int i = 0; i < _time_units.Count; i++)
                comboBoxCoreMetricTimeOverrideUnits.Items.Add(_time_units[i].Abbreviation);

            // Build a string containing the valid address field types
            valid_address_types = "";
            for (int i = 0; i < _field_types.Count; i++)
            {
                if (_wireshark_field_types[i].IsValidAddress)
                    valid_address_types += _wireshark_field_types[i].Name + ", ";
            }
            if (valid_address_types.Length >= 2)
                valid_address_types = valid_address_types.Remove(valid_address_types.Length - 2);

            // Build a string containing the valid timestamp field types
            valid_timestamp_types = "";
            for (int i = 0; i < _field_types.Count; i++)
            {
                if (_wireshark_field_types[i].IsValidTimestamp)
                    valid_timestamp_types += _wireshark_field_types[i].Name + ", ";
            }
            if (valid_timestamp_types.Length >= 2)
                valid_timestamp_types = valid_timestamp_types.Remove(valid_timestamp_types.Length - 2);
        }

        private void buttonOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void buttonProtocolAdd_Click(object sender, RoutedEventArgs e)
        {
            _profiles.Add(_current_profile);
            listBoxProtocols.Items.Add(_current_profile.Ident.Name);
            listBoxProtocols.SelectedIndex = listBoxProtocols.Items.Count - 1;
            //ClearAllFields();
            //int index = _profiles.Count - 1;
            //PopulateAllFields(_profiles[index]);
        }
        private void buttonProtocolUpdate_Click(object sender, RoutedEventArgs e)
        {

        }
        private void buttonProtocolDelete_Click(object sender, RoutedEventArgs e)
        {

        }
        private void listBoxProtocols_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ClearAllFields();
            if (listBoxProtocols.SelectedIndex != -1)
            {
                buttonProtocolDelete.IsEnabled = true;
                PopulateAllFields(listBoxProtocols.SelectedIndex);
            }
            else
            {
                buttonProtocolDelete.IsEnabled = false;
            }
        }

        private void textBoxName_TextChanged(object sender, TextChangedEventArgs e)
        {
            textBoxName.Text = textBoxName.Text.Trim();
            if (!string.IsNullOrEmpty(textBoxName.Text))
            {
                _current_profile.Ident.Name = textBoxName.Text;
                if (listBoxProtocols.Items.Contains(textBoxName))
                {
                    buttonProtocolAdd.IsEnabled = false;
                    buttonProtocolUpdate.IsEnabled = true;
                }
                else
                {
                    buttonProtocolAdd.IsEnabled = true;
                    buttonProtocolUpdate.IsEnabled = false;
                }
            }
            else
            {
                buttonProtocolAdd.IsEnabled = false;
                buttonProtocolUpdate.IsEnabled = false;
            }
        }

        private void checkBoxCoreMetricTimeOverride_Checked(object sender, RoutedEventArgs e)
        {
            int header_index = -1;
            int type_index = -1;
            _current_profile.OverridePacketTime = true;
            if (CheckWiresharkFieldCoreMetric(checkBoxCoreMetricTimeOverride, textBoxCoreMetricTimeOverride, textBlockCoreMetricTimeFieldType, Enums.ValidateWiresharkFieldType.Timestamp, out header_index, out type_index))
                _current_profile.NewPacketTime = _current_wireshark_fields.WiresharkHeaderFields[header_index];
            else
                _current_profile.NewPacketTime = new WiresharkHeaderField();
        }
        private void checkBoxCoreMetricTimeOverride_Unchecked(object sender, RoutedEventArgs e)
        {
            _current_profile.OverridePacketTime = false;
            CheckWiresharkFieldCoreMetric(checkBoxCoreMetricTimeOverride, textBoxCoreMetricTimeOverride, textBlockCoreMetricTimeFieldType, Enums.ValidateWiresharkFieldType.Timestamp);
            _current_profile.NewPacketTime = new WiresharkHeaderField();
        }
        private void textBoxCoreMetricTimeOverride_TextChanged(object sender, TextChangedEventArgs e)
        {
            int header_index = -1;
            int type_index = -1;
            if (CheckWiresharkFieldCoreMetric(checkBoxCoreMetricTimeOverride, textBoxCoreMetricTimeOverride, textBlockCoreMetricTimeFieldType, Enums.ValidateWiresharkFieldType.Timestamp, out header_index, out type_index))
                _current_profile.NewPacketTime = _current_wireshark_fields.WiresharkHeaderFields[header_index];
            else
                _current_profile.NewPacketTime = new WiresharkHeaderField();
        }
        private void comboBoxCoreMetricTimeOverrideUnits_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TimeUnit time_unit = _time_units.Find(delegate(TimeUnit tu) { return tu.Abbreviation == comboBoxCoreMetricTimeOverrideUnits.Text; });
            _current_profile.NewPacketTimeUnit = time_unit;
        }

        private void checkBoxCoreMetricSrcAddressOverride_Checked(object sender, RoutedEventArgs e)
        {
            int header_index = -1;
            int type_index = -1;
            _current_profile.OverrideSourceAddress = true;
            if (CheckWiresharkFieldCoreMetric(checkBoxCoreMetricSrcAddressOverride, textBoxCoreMetricSrcAddressOverride, textBlockCoreMetricSrcAddressFieldType, Enums.ValidateWiresharkFieldType.Address, out header_index, out type_index))
                _current_profile.NewSourceAddress = _current_wireshark_fields.WiresharkHeaderFields[header_index];
            else
                _current_profile.NewSourceAddress = new WiresharkHeaderField();
        }
        private void checkBoxCoreMetricSrcAddressOverride_Unchecked(object sender, RoutedEventArgs e)
        {
            _current_profile.OverrideSourceAddress = false;
            CheckWiresharkFieldCoreMetric(checkBoxCoreMetricSrcAddressOverride, textBoxCoreMetricSrcAddressOverride, textBlockCoreMetricSrcAddressFieldType, Enums.ValidateWiresharkFieldType.Address);
            _current_profile.NewSourceAddress = new WiresharkHeaderField();
        }
        private void textBoxCoreMetricSrcAddressOverride_TextChanged(object sender, TextChangedEventArgs e)
        {
            int header_index = -1;
            int type_index = -1;
            if (CheckWiresharkFieldCoreMetric(checkBoxCoreMetricSrcAddressOverride, textBoxCoreMetricSrcAddressOverride, textBlockCoreMetricSrcAddressFieldType, Enums.ValidateWiresharkFieldType.Address, out header_index, out type_index))
                _current_profile.NewSourceAddress = _current_wireshark_fields.WiresharkHeaderFields[header_index];
            else
                _current_profile.NewSourceAddress = new WiresharkHeaderField();
        }

        private void checkBoxCoreMetricDstAddressOverride_Checked(object sender, RoutedEventArgs e)
        {
            int header_index = -1;
            int type_index = -1;
            _current_profile.OverrideDestinationAddress = true;
            if ( CheckWiresharkFieldCoreMetric(checkBoxCoreMetricDstAddressOverride, textBoxCoreMetricDstAddressOverride, textBlockCoreMetricDstAddressFieldType, Enums.ValidateWiresharkFieldType.Address, out header_index, out type_index))
                _current_profile.NewDestinationAddress = _current_wireshark_fields.WiresharkHeaderFields[header_index];
            else
                _current_profile.NewDestinationAddress = new WiresharkHeaderField();
        }
        private void checkBoxCoreMetricDstAddressOverride_Unchecked(object sender, RoutedEventArgs e)
        {
            _current_profile.OverrideDestinationAddress = false;
            CheckWiresharkFieldCoreMetric(checkBoxCoreMetricDstAddressOverride, textBoxCoreMetricDstAddressOverride, textBlockCoreMetricDstAddressFieldType, Enums.ValidateWiresharkFieldType.Address);
            _current_profile.NewDestinationAddress = new WiresharkHeaderField();
        }
        private void textBoxCoreMetricDstAddressOverride_TextChanged(object sender, TextChangedEventArgs e)
        {
            int header_index = -1;
            int type_index = -1;
            if (CheckWiresharkFieldCoreMetric(checkBoxCoreMetricDstAddressOverride, textBoxCoreMetricDstAddressOverride, textBlockCoreMetricDstAddressFieldType, Enums.ValidateWiresharkFieldType.Address, out header_index, out type_index))
                _current_profile.NewDestinationAddress = _current_wireshark_fields.WiresharkHeaderFields[header_index];
            else
                _current_profile.NewDestinationAddress = new WiresharkHeaderField();
        }

        private void textBoxProtocolMetricAbbreviation_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckWiresharkProtocolMetric(textBoxProtocolMetricAbbreviation, textBoxProtocolMetricName, textBoxProtocolMetricDescription, textBoxProtocolMetricType);
        }
        private void buttonProtocolMetricAdd_Click(object sender, RoutedEventArgs e)
        {
            int header_index;
            int type_index;
            if (CheckWiresharkProtocolMetric(textBoxProtocolMetricAbbreviation, null, null, null, out header_index, out type_index))
            {
                if (!listBoxProtocolMetric.Items.Contains(_current_wireshark_fields.WiresharkHeaderFields[header_index].Abbreviation))
                {
                    _current_profile.ProtocolMetrics.Add(_current_wireshark_fields.WiresharkHeaderFields[header_index]);
                    listBoxProtocolMetric.Items.Add(_current_wireshark_fields.WiresharkHeaderFields[header_index].Abbreviation);
                }
            }
            else
            {
                MessageBox.Show("Header abbreviation is not a valid Wireshark protocol header field.  Please reenter the abbreviation.", "ERROR: Invalid Abbreviation", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void buttonProtocolMetricDelete_Click(object sender, RoutedEventArgs e)
        {
            int index = listBoxProtocolMetric.SelectedIndex;
            if (index != -1)
            {
                _current_profile.ProtocolMetrics.RemoveAt(index);
                listBoxProtocolMetric.Items.RemoveAt(index);
            }
        }

        private void listBoxProtocolMetric_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ClearProtocolMetricFields();
            if (listBoxProtocolMetric.SelectedIndex != -1)
            {
                textBoxProtocolMetricAbbreviation.Text = _current_profile.ProtocolMetrics[listBoxProtocolMetric.SelectedIndex].Abbreviation;
                CheckWiresharkProtocolMetric(textBoxProtocolMetricAbbreviation, textBoxProtocolMetricName, textBoxProtocolMetricDescription, textBoxProtocolMetricType);
            }
        }

        private void textBoxProtocolFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            _current_profile.ProtocolFilter = textBoxProtocolFilter.Text;
        }

        #endregion

    }
}
