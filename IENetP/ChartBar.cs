﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace IENetP
{
    /// <summary>
    /// Chart Colored Bar
    /// </summary>
    public class ChartBar
    {
        #region Constructors

        /// <summary>
        /// Create a default <c>ChartBar</c>
        /// </summary>
        public ChartBar()
        {
        }

        /// <summary>
        /// Create an <c>ChartBar</c> with the desired parameters
        /// </summary>
        /// <param name="color"><c>System.Drawing.Color</c> representing the background color</param>
        /// <param name="percent"><c>double</c> representing the maximum percentage</param>
        public ChartBar(Color color, double percent)
        {
            _back_color = color;
            _max_percent = percent;
        }

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        private Color _back_color = new Color();
        /// <summary>
        /// Gets or sets the background color for the colored bar
        /// </summary>
        public Color BackColor
        {
            get { return _back_color; }
            set { _back_color = value; }
        }

        private double _max_percent = 1000;
        /// <summary>
        /// Gets or sets the maximum percentage of the mean for the colored bar
        /// </summary>
        public double MaxPercent
        {
            get { return _max_percent; }
            set { _max_percent = value; }
        }

        #endregion

        #region Methods

        #endregion
    }
}
