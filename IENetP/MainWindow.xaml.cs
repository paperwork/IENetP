﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Schema;


using IENetP.Generic;
using IENetP.Network;
using IENetP.Data;
using IENetP.Wireshark;

namespace IENetP
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// 
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            _background_worker = ((BackgroundWorker)this.FindResource("background_worker"));
            _background_worker.DoWork += new DoWorkEventHandler(background_worker_DoWork);
            _background_worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(background_worker_RunWorkerCompleted);
            _background_worker.ProgressChanged += new ProgressChangedEventHandler(background_worker_ProgressChanged);

            CommandBinding command_binding_page_setup = new CommandBinding(CommandPageSetup, ExecutedPageSetup, CanExecutePageSetup);
            this.CommandBindings.Add(command_binding_page_setup);

            CommandBinding command_binding_build_wireshark_fields = new CommandBinding(CommandBuildWiresharkFields, ExecutedBuildWiresharkFields, CanExecuteBuildWiresharkFields);
            this.CommandBindings.Add(command_binding_build_wireshark_fields);

            CommandBinding command_binding_protocol_profiles = new CommandBinding(CommandProtocolProfiles, ExecutedProtocolProfiles, CanExecuteProtocolProfiles);
            this.CommandBindings.Add(command_binding_protocol_profiles);
            
            CommandBinding command_binding_view_report = new CommandBinding(CommandViewReport, ExecutedViewReport, CanExecuteViewReport);
            this.CommandBindings.Add(command_binding_view_report);

            CommandBinding command_binding_about = new CommandBinding(CommandAbout, ExecutedAbout, CanExecuteAbout);
            this.CommandBindings.Add(command_binding_about);

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string[] args = Environment.GetCommandLineArgs();
            if (args.Length == 5 && args.Contains("/pcap") && args.Contains("/ip"))
            {
                _command_line_called = true;
            }
            this.Cursor = System.Windows.Input.Cursors.AppStarting;

            ClearResults();
            BuildDefaultWiresharkFilter();
            BuildWiresharkFieldTypes();
            _activate_save = false;
            _temporary_directory = Environment.GetEnvironmentVariable(Constants.TemporaryFileEnvironmentVariable);

            System.Windows.Forms.Integration.WindowsFormsHost host = new System.Windows.Forms.Integration.WindowsFormsHost();
            host.Child = _chart_main;
            this.gridChart.Children.Add(host);

            _chart_main.BackGradientStyle = GradientStyle.TopBottom;
            _chart_main.BackColor = System.Drawing.Color.AliceBlue;
            _chart_main.BackSecondaryColor = System.Drawing.Color.Azure;
            BuildChartBars();

            InitializeIENetPWindow init_dlg = new InitializeIENetPWindow();
            init_dlg.TemporaryDirectory = _temporary_directory;
            init_dlg.ApplicationDirectory = AppDomain.CurrentDomain.BaseDirectory;
            init_dlg.CapinfosApp = _capinfos;
            init_dlg.TSharkApp = _tshark;

            bool? result = init_dlg.ShowDialog();
            if (result == true)
            {
                if (init_dlg.BuildNewFieldsFile && (_command_line_called || (
                    System.Windows.MessageBoxResult.Yes == System.Windows.MessageBox.Show("A different version of Wireshark has been detected since the last time the Wireshark Fields File was built.\nWould you like to rebuild the Fields File now?\n\nYou can rebuild it later by choosing the option in the Tools menu.", "Rebuild Wireshark Fields File?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes))))
                    {
                        BuildWiresharkFieldsWindow build_fields_dlg = new BuildWiresharkFieldsWindow();
                        build_fields_dlg.TemporaryDirectory = _temporary_directory;
                        build_fields_dlg.ApplicationDirectory = AppDomain.CurrentDomain.BaseDirectory;
                        build_fields_dlg.TSharkApp = _tshark;

                        result = build_fields_dlg.ShowDialog();
                        if (result == true)
                        {
                            _wireshark_fields = build_fields_dlg.CurrentWiresharkFields;
                        }
                        else
                        {
                            string error_message = "An error occured while trying to build the Wireshark fields file. ";
                            error_message += "Please refer to the error below for more information.\n\n";
                            error_message += "BUILD WIRESHARK FIELDS: " + build_fields_dlg.ErrorMessage;
                            System.Windows.MessageBox.Show(error_message, "ERROR: Building Wireshark Fields", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                        }
                }
                else
                {
                    _wireshark_fields = init_dlg.CurrentWiresharkFields;
                }
            }
            else
            {
                string error_message = "An error occured while trying to initialize the application. ";
                error_message += "The application will quit.  Please refer to the error below for more information.\n\n";
                error_message += "INITALIZATION: " + init_dlg.ErrorMessage;
                System.Windows.MessageBox.Show(error_message, "ERROR: Initializing Application", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                this.Close();
            }

            this.Cursor = System.Windows.Input.Cursors.Arrow;

            /* try to automatic start analyse if command line parameters are available */
            if (_command_line_called)
            {
                for (int index = 0; index < args.Length; index++)
                {
                    if ("/ip" == args[index]) textBoxIpAddress.Text = args[index + 1];
                    if ("/pcap" == args[index]) textBoxCaptureFile.Text = args[index + 1];
                }
                buttonAnalyze_Click(this, new RoutedEventArgs());
            }
        }

        #region Data Objects

        public static RoutedCommand CommandPageSetup = new RoutedCommand();
        public static RoutedCommand CommandBuildWiresharkFields = new RoutedCommand();
        public static RoutedCommand CommandProtocolProfiles = new RoutedCommand();
        public static RoutedCommand CommandViewReport = new RoutedCommand();
        public static RoutedCommand CommandAbout = new RoutedCommand();

        private bool _activate_save = false;
        private bool _command_line_called = false;
        private string _report_filename = "";
        private string _temporary_directory = "";

        private Chart _chart_main = new Chart();
        private Plotter _plot = new Plotter();
        private List<ChartBar> _chart_bar = new List<ChartBar>();

        private Capinfos _capinfos = new Capinfos();
        private TShark _tshark = new TShark();
        private List<Identification> _indices = new List<Identification>();
        private List<JitterDatum> _full_data = new List<JitterDatum>();
        private ProgressData _progress = new ProgressData();
        private static Enums.ProcessStatusState _process_status = Enums.ProcessStatusState.Unstarted;
        private static List<string> _process_log = new List<string>();
        private WiresharkFields _wireshark_fields = new WiresharkFields();
        private List<WiresharkFieldType> _wireshark_field_types = new List<WiresharkFieldType>();

        private BackgroundWorker _background_worker;

        #endregion

        #region Properties

        private AnalysisSettings _analysis_settings = new AnalysisSettings();
        /// <summary>
        /// Settings used by the analysis process
        /// </summary>
        public AnalysisSettings Settings
        {
            get { return _analysis_settings; }
            set { _analysis_settings = value; }
        }

        private AnalysisResults _analysis_results = new AnalysisResults();
        /// <summary>
        /// Results of the analysis process
        /// </summary>
        public AnalysisResults Results
        {
            get { return _analysis_results; }
            set { _analysis_results = value; }
        }

        #endregion

        #region File Menu

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CanExecuteNew(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecutedNew(object sender, ExecutedRoutedEventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CanExecuteOpen(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecutedOpen(object sender, ExecutedRoutedEventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CanExecuteSave(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = _activate_save;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecutedSave(object sender, ExecutedRoutedEventArgs e)
        {
            _activate_save = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CanExecuteSaveAs(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecutedSaveAs(object sender, ExecutedRoutedEventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CanExecutePageSetup(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecutedPageSetup(object sender, ExecutedRoutedEventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CanExecutePrintPreview(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecutedPrintPreview(object sender, ExecutedRoutedEventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CanExecutePrint(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecutedPrint(object sender, ExecutedRoutedEventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CanExecuteClose(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecutedClose(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, CancelEventArgs e)
        {
            //Properties.Settings.Default.Save();
            if (!CleanupTemporaryFiles() && !_command_line_called)
                System.Windows.MessageBox.Show("Trouble cleaning up temporary files.", "WARNING: Cleaning Up");
        }

        /// <summary>
        /// Remove all temporary files created while running
        /// </summary>
        /// <returns>A <c>bool</c> indicating whether all the temporary files were deleted correctly</returns>
        private bool CleanupTemporaryFiles()
        {
            DirectoryInfo temp_dir = new DirectoryInfo(_temporary_directory);

            FileInfo[] ienetp_temp_files = temp_dir.GetFiles(Constants.IENetPFileHeader + "*" + Constants.AnyFileExtension);

            try
            {
                for (int i = 0; i < ienetp_temp_files.Count(); i++)
                {
                    ienetp_temp_files[i].Delete();
                }
            }
            catch
            {
                return false;
            }

            ienetp_temp_files = temp_dir.GetFiles(Constants.IENetPFileHeader + "*" + Constants.AnyFileExtension);

            if (ienetp_temp_files.Count() != 0)
                return false;
            else
                return true;
        }

        #endregion

        #region Edit Menu

        #endregion

        #region Tools Menu

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CanExecuteBuildWiresharkFields(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecutedBuildWiresharkFields(object sender, ExecutedRoutedEventArgs e)
        {
            BuildWiresharkFieldsWindow build_fields_dlg = new BuildWiresharkFieldsWindow();
            build_fields_dlg.TemporaryDirectory = _temporary_directory;
            build_fields_dlg.ApplicationDirectory = AppDomain.CurrentDomain.BaseDirectory;
            build_fields_dlg.TSharkApp = _tshark;

            bool? result = build_fields_dlg.ShowDialog();
            if (result == true)
            {
                _wireshark_fields = build_fields_dlg.CurrentWiresharkFields;
            }
            else
            {
                string error_message = "An error occured while trying to build the Wireshark fields file. ";
                error_message += "Please refer to the error below for more information.\n\n";
                error_message += "BUILD WIRESHARK FIELDS: " + build_fields_dlg.ErrorMessage;
                System.Windows.MessageBox.Show(error_message, "ERROR: Building Wireshark Fields", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CanExecuteProtocolProfiles(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecutedProtocolProfiles(object sender, ExecutedRoutedEventArgs e)
        {
            ProtocolProfilesWindow dlg = new ProtocolProfilesWindow();
            dlg.CurrentWiresharkFields = _wireshark_fields;
            dlg.WiresharkFieldTypes = _wireshark_field_types;

            Nullable<bool> dlg_result = dlg.ShowDialog();

            if (dlg_result == true)
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CanExecuteViewReport(object sender, CanExecuteRoutedEventArgs e)
        {
            bool report_file_exists = false;
            if (!String.IsNullOrEmpty(_report_filename))
            {
                FileInfo report_file = new FileInfo(_report_filename);
                report_file_exists = report_file.Exists;
            }
            e.CanExecute = report_file_exists;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecutedViewReport(object sender, ExecutedRoutedEventArgs e)
        {
            Process.Start("file:///" + _report_filename);
        }

        #endregion

        #region Help Menu

        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CanExecuteHelp(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecutedHelp(object sender, ExecutedRoutedEventArgs e)
        {
        }

        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CanExecuteAbout(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecutedAbout(object sender, ExecutedRoutedEventArgs e)
        {
            AboutBox box = new AboutBox(this);

            box.ShowDialog();
        }

        #endregion

        #region Analysis Settings

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxIpAddress_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxIpAddress.Text))
            {
                textBoxIpAddress.Background = Constants.TextBoxBackgroundBrushBlank;
            }
            else
            {
                IPv4Addr addr = new IPv4Addr(textBoxIpAddress.Text.Trim());

                if (addr.IsValid == true)
                    textBoxIpAddress.Background = Constants.TextBoxBackgroundBrushValid;
                else
                    textBoxIpAddress.Background = Constants.TextBoxBackgroundBrushInvalid;

                _activate_save = true;
            }

            BuildDefaultWiresharkFilter();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxIpAddress_GotFocus(object sender, RoutedEventArgs e)
        {
            textBoxIpAddress.SelectAll();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAdditionalInfo_Click(object sender, RoutedEventArgs e)
        {
            AdditionalInfoWindow dlg = new AdditionalInfoWindow();
            dlg.Settings = _analysis_settings;

            Nullable<bool> dlg_result = dlg.ShowDialog();

            if (dlg_result == true)
            {
                _analysis_settings = dlg.Settings;
                _activate_save = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCaptureFileBrowse_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
 
            dlg.Title = "Please select a Wireshark File to open.";
            dlg.Filter = "Wireshark Files (*.pcap, *.cap, *.snoop, *.enc)|*.pcap;*.cap;*.snoop;*.enc|All Files (*.*)|*.*";
            dlg.FilterIndex = 1;

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                string filename = dlg.FileName;
                textBoxCaptureFile.Text = filename;
                _activate_save = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxCaptureFile_TextChanged(object sender, TextChangedEventArgs e)
        {
            _activate_save = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxCaptureFile_GotFocus(object sender, RoutedEventArgs e)
        {
            textBoxCaptureFile.SelectAll();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxWiresharkFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            _activate_save = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxWiresharkFilter_GotFocus(object sender, RoutedEventArgs e)
        {
            if (checkBoxWiresharkFilterOverride.IsChecked == true)
                textBoxWiresharkFilter.SelectAll();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxWiresharkFilterOverride_Click(object sender, RoutedEventArgs e)
        {
            Nullable<bool> filter_override = checkBoxWiresharkFilterOverride.IsChecked;

            if (filter_override == true)
            {
                textBoxWiresharkFilter.Background = Constants.TextBoxBackgroundBrushBlank;
                textBoxWiresharkFilter.IsReadOnly = false;
                textBoxWiresharkFilter.IsTabStop = true;
            }
            else if (filter_override == false)
            {
                BuildDefaultWiresharkFilter();
                textBoxWiresharkFilter.Background = Constants.TextBoxBackgroundBrushReadOnly;
                textBoxWiresharkFilter.IsReadOnly = true;
                textBoxWiresharkFilter.IsTabStop = false;
            }

            _activate_save = true;
        }

        #endregion

        #region Analyze

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAnalyze_Click(object sender, RoutedEventArgs e)
        {
            string line = "";
            _process_log = new List<string>();

            if (textBoxCaptureFile.Text == "")
            {
                line = "No capture file selected.";
                _process_log.Add(DateTime.Now.ToString() + ":\t" + line);
                if (_command_line_called)
                    System.Windows.Application.Current.Shutdown(-1);
                else
                    System.Windows.MessageBox.Show(line, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                FileInfo cap_file = new FileInfo(textBoxCaptureFile.Text);

                if (cap_file.Exists)
                {
                    if (_process_status != Enums.ProcessStatusState.InProgress && _process_status != Enums.ProcessStatusState.Paused)
                    {
                        this.Cursor = System.Windows.Input.Cursors.Wait;
                        _process_status = Enums.ProcessStatusState.InProgress;
                        buttonAnalyze.IsEnabled = false;

                        _analysis_settings.DeviceIp = new IPv4Addr(textBoxIpAddress.Text);
                        _analysis_settings.CaptureFileName = textBoxCaptureFile.Text;
                        _analysis_settings.WiresharkFilterString = textBoxWiresharkFilter.Text;

                        ClearResults();
                        _background_worker.RunWorkerAsync();
                    }
                    else
                    {
                        line = "Analysis process already started.";
                        _process_log.Add(DateTime.Now.ToString() + ":\t" + line);
                        System.Windows.MessageBox.Show(line, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    line = "Capture file does not exist.";
                    _process_log.Add(DateTime.Now.ToString() + ":\t" + line);
                    if (_command_line_called)
                        System.Windows.Application.Current.Shutdown(-2);
                    else
                        System.Windows.MessageBox.Show(line, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void background_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            Enums.AnalysisStatus status = Enums.AnalysisStatus.Error;

            status = ValidateCaptureFile(_analysis_settings.CaptureFileName);
            if (status == Enums.AnalysisStatus.Success)
                status = ProcessCaptureFile(_analysis_settings.CaptureFileName);
            if (status == Enums.AnalysisStatus.Success)
                status = ProcessWiresharkOutput();
            if (status == Enums.AnalysisStatus.Success)
                status = BuildDatasets();

            e.Result = status;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void background_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((Enums.AnalysisStatus)e.Result == Enums.AnalysisStatus.Success)
            {
                comboBoxResultsDataset.Items.Clear();
                for (int i = 0; i < Results.Datasets.Count; i++)
                    comboBoxResultsDataset.Items.Add(Results.Datasets[i].DatasetIdent.Name);
                comboBoxResultsDataset.SelectedIndex = 0;
                textBoxSuccess.Text = "SUCCESS";
                textBoxSuccess.Background = Constants.TextBoxBackgroundBrushValid;
                PopulateResultsTextBox(comboBoxResultsDataset.SelectedIndex);
                BuildChart(comboBoxResultsDataset.SelectedIndex);
                buttonResultsChartResetZoom.IsEnabled = true;
            }
            else
            {
                _process_status = Enums.ProcessStatusState.Error;
            }
            this.Cursor = System.Windows.Input.Cursors.Arrow;
            buttonAnalyze.IsEnabled = true;
            _process_status = Enums.ProcessStatusState.Completed;

            if (_command_line_called) this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void background_worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBarAnalyze.Value = e.ProgressPercentage;
        }

        /// <summary>
        /// Validate the capture file
        /// </summary>
        /// <param name="filename">A <c>string</c> containing the full path to the Wireshark capture file.</param>
        /// <returns>An <c>AnalysisStatus</c> enumeration indicating whether the analysis process completed successfuly and the capture file is valid.</returns>
        private Enums.AnalysisStatus ValidateCaptureFile(string filename)
        {
            Enums.AnalysisStatus status = Enums.AnalysisStatus.Error;
            string status_error = "";
            DateTime start_step = new DateTime();
            DateTime end_step = new DateTime();
            TimeSpan time_step = new TimeSpan();

            if (_capinfos.Status != Enums.ThreadStatusState.InProgress || _capinfos.Status != Enums.ThreadStatusState.Paused)
            {
                _process_log.Add(DateTime.Now.ToString() + ": Validate Capture File: Started.");
                status = Enums.AnalysisStatus.Success;
                start_step = DateTime.Now;
                _progress.Current = _progress.Minimum;
                _background_worker.ReportProgress(_progress.Current);
                _capinfos.Progress = _progress;

                // Validate the Capinfos process before trying to determine capture file parameters
                _capinfos.ValidateApplication();
                _progress.Current = (int)(((_progress.Maximum - _progress.Minimum) / 2) + _progress.Minimum);
                _background_worker.ReportProgress(_progress.Current);

                // Validate the capture file
                if (_capinfos.IsValid == true)
                {
                    _capinfos.GetCaptureFileInformation(filename);
                }
                else
                {
                    status = Enums.AnalysisStatus.Error;
                    status_error = "Capinfos does not exist.";
                }

                if (_capinfos.IsValidCaptureFile)
                {
                    Results.CaptureFileInfo = _capinfos.CaptureFileInfo;
                }
                else
                {
                    status = Enums.AnalysisStatus.Error;
                    status_error = "Not a valid Wireshark capture file.";
                }

                if (status == Enums.AnalysisStatus.Error)
                {
                    _process_log.Add(DateTime.Now.ToString() + ": Validate Capture File: ERROR!");
                    _process_log.Add(status_error);
                }
                else
                {
                    _process_log.Add(DateTime.Now.ToString() + ": Validate Capture File: " + Results.CaptureFileInfo.FilePackets + " packets");
                }

                _progress.Current = _progress.Maximum;
                _background_worker.ReportProgress(_progress.Current);
                end_step = DateTime.Now;
                time_step = TimeSpan.FromTicks(end_step.Ticks - start_step.Ticks);
                _process_log.Add(DateTime.Now.ToString() + ": Validate Capture File: Completed in " + time_step.TotalSeconds.ToString() + " s");
            }
            else
            {
                _process_log.Add(DateTime.Now.ToString() + ": Validate Capture File: Process already started.");
            }

            return status;
        }

        /// <summary>
        /// Filter and process the packets in the capture file
        /// </summary>
        /// <param name="filename">A <c>string</c> containing the full path to the Wireshark capture file.</param>
        /// <returns>An <c>AnalysisStatus</c> enumeration indicating whether the analysis process completed successfuly.</returns>
        private Enums.AnalysisStatus ProcessCaptureFile(string filename)
        {
            Enums.AnalysisStatus status = Enums.AnalysisStatus.Success;
            string status_error = "";
            DateTime start_step = new DateTime();
            DateTime end_step = new DateTime();
            TimeSpan time_step = new TimeSpan();

            _process_log.Add(DateTime.Now.ToString() + ": Process Capture File: Started.");
            start_step = DateTime.Now;

            _progress.Current = _progress.Minimum;
            _background_worker.ReportProgress(_progress.Current);
            _tshark.Progress = _progress;

            // Validate the Tshark process before trying to determine capture file parameters
            _tshark.ValidateApplication();
            _progress.Current = ((_progress.Maximum - _progress.Minimum) / 4) + _progress.Minimum;
            _background_worker.ReportProgress(_progress.Current);

            if (_tshark.IsValid == true)
            {
                _tshark.CaptureFileInfo = Results.CaptureFileInfo;
                _tshark.WiresharkFilter = Settings.WiresharkFilterString;
                long temp_code = DateTime.Now.ToFileTime();
                string temp_filename = _temporary_directory + "\\" + Constants.IENetPFileHeader + temp_code.ToString() + Constants.XmlFileExtension;
                _tshark.RedirectToTemporaryFile = true;
                _tshark.TemporaryFile = new FileInfo(temp_filename);
                status = _tshark.ProcessCaptureFile();
                _progress.Current = ((_progress.Maximum - _progress.Minimum) / 4) * 3 + _progress.Minimum;
                _background_worker.ReportProgress(_progress.Current);
            }
            else
            {
                status = Enums.AnalysisStatus.Error;
                status_error = "Tshark does not exist.";
            }

            if (status == Enums.AnalysisStatus.Error)
            {
                _process_log.Add(DateTime.Now.ToString() + ": Process Capture File: ERROR!");
                _process_log.Add(status_error);
                _process_log.Add(_tshark.ErrorMessage);
            }

            _progress.Current = _progress.Maximum;
            _background_worker.ReportProgress(_progress.Current);
            end_step = DateTime.Now;
            time_step = TimeSpan.FromTicks(end_step.Ticks - start_step.Ticks);
            _process_log.Add(DateTime.Now.ToString() + ": Process Capture File: Completed in " + time_step.TotalSeconds.ToString() + " s");

            return status;
        }

        /// <summary>
        /// Process the Wireshark packets and determine the number of datasets
        /// </summary>
        /// <returns>An <c>AnalysisStatus</c> enumeration indicating whether the analysis process completed successfuly.</returns>
        private Enums.AnalysisStatus ProcessWiresharkOutput()
        {
            Enums.AnalysisStatus status = Enums.AnalysisStatus.Success;
            DateTime start_step = new DateTime();
            DateTime end_step = new DateTime();
            TimeSpan time_step = new TimeSpan();
            WiresharkPSMLStructure psml_structure = new WiresharkPSMLStructure();
            WiresharkPSMLPacket psml_packet = new WiresharkPSMLPacket();

			string[] delimiters = {" ",",",":"};
            int num;
            double time;
            EthernetMACAddr mac_src = new EthernetMACAddr();
            EthernetMACAddr mac_dst = new EthernetMACAddr();
            IPv4Addr ip_src = new IPv4Addr();
            IPv4Addr ip_dst = new IPv4Addr();
            int enip_cid = 0;
            int enip_seq = 0;
            string index_name;
            List<string> indices_names = new List<string>();
            int index;
            NetworkPair pair = new NetworkPair();
            JitterDatum datum = new JitterDatum();

            _process_log.Add(DateTime.Now.ToString() + ": Process Wireshark Output: Started.");
            start_step = DateTime.Now;

            _progress.Current = _progress.Minimum;
            _background_worker.ReportProgress(_progress.Current);
            _tshark.Progress = _progress;

            XmlReader reader = XmlReader.Create(_tshark.TemporaryFile.FullName);
            XmlDocument doc = new XmlDocument();
            doc.Load(reader);

            XmlNodeList structure;
            XmlNode root = doc.DocumentElement;
            structure = root.SelectNodes("//structure");
            if (structure.Count == 1)
            {
                XmlNode structure_node = structure.Item(0);
                List<string> sections = new List<string>();
                for (int i = 0; i < structure_node.ChildNodes.Count; i++)
                {
                    sections.Add(structure_node.ChildNodes[i].InnerText);
                }
                int temp = sections.FindIndex(delegate(string title) { return title == psml_structure.NumberTitle; });
                if (temp == -1)
                    status = Enums.AnalysisStatus.Error;
                else
                    psml_structure.NumberIndex = temp;
                temp = sections.FindIndex(delegate(string title) { return title == psml_structure.TimeTitle; });
                if (temp == -1)
                    status = Enums.AnalysisStatus.Error;
                else
                    psml_structure.TimeIndex = temp;
                temp = sections.FindIndex(delegate(string title) { return title == psml_structure.SourceTitle; });
                if (temp == -1)
                    status = Enums.AnalysisStatus.Error;
                else
                    psml_structure.SourceIndex = temp;
                temp = sections.FindIndex(delegate(string title) { return title == psml_structure.DestinationTitle; });
                if (temp == -1)
                    status = Enums.AnalysisStatus.Error;
                else
                    psml_structure.DestinationIndex = temp;
                temp = sections.FindIndex(delegate(string title) { return title == psml_structure.ProtocolTitle; });
                if (temp == -1)
                    status = Enums.AnalysisStatus.Error;
                else
                    psml_structure.ProtocolIndex = temp;
                temp = sections.FindIndex(delegate(string title) { return title == psml_structure.EnipCidTitle; });
                if (temp == -1)
                    status = Enums.AnalysisStatus.Error;
                else
                    psml_structure.EnipCidIndex = temp;
                temp = sections.FindIndex(delegate(string title) { return title == psml_structure.EnipSeqTitle; });
                if (temp == -1)
                    status = Enums.AnalysisStatus.Error;
                else
                    psml_structure.EnipSeqIndex = temp;
                temp = sections.FindIndex(delegate(string title) { return title == psml_structure.InfoTitle; });
                if (temp == -1)
                    status = Enums.AnalysisStatus.Error;
                else
                    psml_structure.InfoIndex = temp;
            }
            else
            {
                status = Enums.AnalysisStatus.Error;
            }

            if (status == Enums.AnalysisStatus.Error)
            {
                _process_log.Add(DateTime.Now.ToString() + ": Process Wireshark Output: Error reading structure of Wireshark PSML output.");
            }
            else
            {
                XmlNodeList packets = root.SelectNodes("//packet");
                XmlNode packet;
                if (packets.Count > 0)
                {
                    _process_log.Add(DateTime.Now.ToString() + ": Process Wireshark Output: " + packets.Count + " filtered packets");
                    for (int i = 0; i < packets.Count; i++)
                    {
                        packet = packets[i];
                        psml_packet = new WiresharkPSMLPacket();
                        psml_packet.Number = packet.ChildNodes[psml_structure.NumberIndex].InnerText;
                        psml_packet.Time = packet.ChildNodes[psml_structure.TimeIndex].InnerText;
                        psml_packet.Source = packet.ChildNodes[psml_structure.SourceIndex].InnerText;
                        psml_packet.Destination = packet.ChildNodes[psml_structure.DestinationIndex].InnerText;
                        psml_packet.Protocol = packet.ChildNodes[psml_structure.ProtocolIndex].InnerText;
                        psml_packet.EnipCid = packet.ChildNodes[psml_structure.EnipCidIndex].InnerText;
                        psml_packet.EnipSeq = packet.ChildNodes[psml_structure.EnipSeqIndex].InnerText;
                        psml_packet.Info = packet.ChildNodes[psml_structure.InfoIndex].InnerText;

                        pair = new NetworkPair();
                        int.TryParse(psml_packet.Number, out num);
                        double.TryParse(psml_packet.Time, out time);
                        mac_src = new EthernetMACAddr(psml_packet.Source);
                        pair.MacSource = mac_src;
                        ip_src = new IPv4Addr(psml_packet.Source);
                        pair.IpSource = ip_src;
                        mac_dst = new EthernetMACAddr(psml_packet.Destination);
                        pair.MacDestination = mac_dst;
                        ip_dst = new IPv4Addr(psml_packet.Destination);
                        pair.IpDestination = ip_dst;
                        if (psml_packet.Protocol == "ENIP")
                        {
                            if (!string.IsNullOrEmpty(psml_packet.EnipCid))
                            {
                                // remove the extra "0x" that appears on the EtherNet/IP Connection ID
                                int.TryParse(psml_packet.EnipCid.Remove(0, 2), NumberStyles.HexNumber, null, out enip_cid);
                                pair.UseEtherNetIP = true;
                                pair.EtherNetIPConnID = enip_cid;
                            }
                            if (!string.IsNullOrEmpty(psml_packet.EnipSeq))
                            {
                                int.TryParse(psml_packet.EnipSeq, out enip_seq);
                            }
                        }

                        // Determine index for this packet
                        index_name = pair.ToString();
                        if (indices_names.Contains(index_name))
                        {
                            index = indices_names.IndexOf(index_name);
                        }
                        else
                        {
                            index = indices_names.Count;
                            indices_names.Add(index_name);
                            string comment = "IP Source == " + ip_src.ToString() + "\nIP Destination == " + ip_dst.ToString();
                            if (pair.UseEtherNetIP)
                                comment += "\nEtherNet/IP Connection ID == 0x" + enip_cid.ToString("X");
                            Identification new_index = new Identification(indices_names.Count, index_name, "Network Address Pair", comment);
                            _indices.Add(new_index);
                        }

                        datum = new JitterDatum(index, time);
                        _full_data.Add(datum);

                        _progress.Current = (int)(i / packets.Count * 100);
                        _background_worker.ReportProgress(_progress.Current);
                    }

                    if (_indices.Count == 1)
                        _process_log.Add(DateTime.Now.ToString() + ": Process Wireshark Output: " + _indices.Count + " dataset");
                    else
                        _process_log.Add(DateTime.Now.ToString() + ": Process Wireshark Output: " + _indices.Count + " datasets");
                }
                else
                {
                    status = Enums.AnalysisStatus.NoPacketsCaptured;
                    _process_log.Add(DateTime.Now.ToString() + ": Process Wireshark Output: No packets captured that match the desired filter.");
                }
            }

            _progress.Current = _progress.Maximum;
            _background_worker.ReportProgress(_progress.Current);
            end_step = DateTime.Now;
            time_step = TimeSpan.FromTicks(end_step.Ticks - start_step.Ticks);
            _process_log.Add(DateTime.Now.ToString() + ": Process Wireshark Output: Completed in " + time_step.TotalSeconds.ToString() + " s");

            return status;
        }

        private static void WiresharkPSMLValidationEventHandler(object sender, ValidationEventArgs e)
        {
            _process_log.Add(e.Message);
        }

        /// <summary>
        /// Build the datasets and analyze the statistics for each dataset
        /// </summary>
        /// <returns>An <c>AnalysisStatus</c> enumeration indicating whether the analysis process completed successfuly.</returns>
        private Enums.AnalysisStatus BuildDatasets()
        {
            Enums.AnalysisStatus status = Enums.AnalysisStatus.Success;
            DateTime start_step = new DateTime();
            DateTime end_step = new DateTime();
            TimeSpan time_step = new TimeSpan();

            List<double> set;
            List<double> diffs;
            List<Point> data_set;
            TestDataset test_dataset;

            _process_log.Add(DateTime.Now.ToString() + ": Build Datasets: Started.");
            start_step = DateTime.Now;

            _progress.Current = _progress.Minimum;
            _background_worker.ReportProgress(_progress.Current);

            for (int i = 0; i < _indices.Count; i++)
            {
                // Build the individual dataset from the full dataset
                set = new List<double>();
                for (int j = 0; j < _full_data.Count; j++)
                {
                    if (i == _full_data[j].Index)
                        set.Add(Math.Round(_full_data[j].DatumValue, Constants.StatisticsDecimalPlaces));
                    _progress.Current = (int)((i * _full_data.Count + j) / (_indices.Count * _full_data.Count) * 100 / 2);
                    _background_worker.ReportProgress(_progress.Current);
                }

                // If the individual dataset has a statistically significant number of points, add the dataset to the results
                if (set.Count >= Constants.StatisticsThreshholdDefault)
                {
                    data_set = new List<Point>();
                    diffs = new List<double>();
                    for (int j = 1; j < set.Count; j++)
                    {
                        // Offset the data to the desired number of decimal places and then round-off properly
                        double d = (set[j] - set[j - 1]) / Math.Pow(10, Constants.StatisticsDecimalOffsetExponentDefault);
                        d = Math.Round(d, Constants.StatisticsDecimalPlaces + Constants.StatisticsDecimalOffsetExponentDefault);
                        diffs.Add(d);
                    }
                    // Remove the first couple points that tend to have errors relative 
                    // to the rest of the data set
                    set.RemoveRange(0, Constants.StatisticsRemoveLeadingDefault + 1);
                    diffs.RemoveRange(0, Constants.StatisticsRemoveLeadingDefault);

                    for (int j = 0; j < set.Count; j++)
                    {
                        data_set.Add(new Point(set[j], diffs[j]));
                    }
                    test_dataset = new TestDataset();
                    test_dataset.Settings = _analysis_settings;
                    test_dataset.DatasetIdent = _indices[i];
                    test_dataset.Points = data_set;
                    test_dataset.JitterStats.Dataset = data_set;
                    test_dataset.BuildHistogram();
                    Results.Datasets.Add(test_dataset);
                }
            }

            _progress.Current = _progress.Maximum;
            _background_worker.ReportProgress(_progress.Current);
            end_step = DateTime.Now;
            time_step = TimeSpan.FromTicks(end_step.Ticks - start_step.Ticks);
            _process_log.Add(DateTime.Now.ToString() + ": Build Datasets: Completed in " + time_step.TotalSeconds.ToString() + " s");

            return status;
        }

        #endregion

        #region Results

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxResultsDataset_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBoxResultsDataset.Items.Count >= 1)
            {
                PopulateResultsTextBox(comboBoxResultsDataset.SelectedIndex);
                BuildChart(comboBoxResultsDataset.SelectedIndex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonResultsChartOptions_Click(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonResultsChartResetZoom_Click(object sender, RoutedEventArgs e)
        {
            _plot.ResetZoom();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Build the default Wireshark filter for data analysis
        /// </summary>
        private void BuildDefaultWiresharkFilter()
        {
            string text = Constants.DefaultWiresharkFilterEtherNetIPClass1Multicast;
            IPv4Addr ip_addr = new IPv4Addr(textBoxIpAddress.Text);

            if (ip_addr.IsValid == true)
            {
                text += " && " + Constants.DefaultWiresharkFilterIpSourcePart1 + 
                    ip_addr.ToString() + Constants.DefaultWiresharkFilterIpSourcePart2;
            }

            if (checkBoxWiresharkFilterOverride.IsChecked == false)
            {
                textBoxWiresharkFilter.Text = text;
            }

        }

        /// <summary>
        /// Clear the results controls main window
        /// </summary>
        private void ClearResults()
        {
            _analysis_results = new AnalysisResults();
            _full_data = new List<JitterDatum>();
            _indices = new List<Identification>();
            _process_log = new List<string>();

            _report_filename = "";
            comboBoxResultsDataset.Items.Clear();
            _plot.ClearChart();
            _chart_main.Titles.Clear();
            _chart_main.Titles.Add(new Title("No Dataset Selected!", Docking.Top, new System.Drawing.Font("Trebuchet MS", 24, System.Drawing.FontStyle.Bold), System.Drawing.Color.DarkBlue));
            textBoxResults.Text = "No Dataset Selected!";
            textBoxSuccess.Text = "NO DATA!";
            textBoxSuccess.Background = Constants.TextBoxBackgroundBrushBlank;
        }

        /// <summary>
        /// Populate the results textbox with the appropriate items
        /// </summary>
        /// <param name="index">An <c>int</c> value representing the dataset results to display</param>
        private void PopulateResultsTextBox(int index)
        {
            string text = "";
            double temp = 0;

            text += "File: " + Results.CaptureFileInfo.CaptureFile.Name + "\n";
            text += "Data Points: " + Results.Datasets[index].Points.Count + "\n\n";
            temp = Results.Datasets[index].JitterStats.Mean;
            text += "Mean: " + temp.ToString() + " ns\n";
            temp = Results.Datasets[index].JitterStats.Minimum;
            text += "Min: " + temp.ToString() + " ns";
            temp = Math.Round((Results.Datasets[index].JitterStats.Mean - Results.Datasets[index].JitterStats.Minimum) / Results.Datasets[index].JitterStats.Mean * 100,3);
            text += " (" + temp.ToString() + "%)\n";
            temp = Results.Datasets[index].JitterStats.Maximum;
            text += "Max: " + temp.ToString() + " ns";
            temp = Math.Round((Results.Datasets[index].JitterStats.Maximum - Results.Datasets[index].JitterStats.Mean) / Results.Datasets[index].JitterStats.Mean * 100, 3);
            text += " (" + temp.ToString() + "%)\n";
            temp = Results.Datasets[index].JitterStats.StandardDeviation;
            text += "StdDev: " + temp.ToString() + " ns";
            temp = Math.Round(Results.Datasets[index].JitterStats.StandardDeviation / Results.Datasets[index].JitterStats.Mean * 100, 6);
            text += " (" + temp.ToString() + "%)\n";

            textBoxResults.Text = text;
        }

        /// <summary>
        /// Build the chart with the appropriate data set and chart parameters
        /// </summary>
        /// <param name="index">An <c>int</c> value representing the dataset results to display</param>
        private void BuildChart(int index)
        {
            _plot.ChartBars = _chart_bar;
            _plot.ChartMain = _chart_main;
            _plot.Dataset = Results.Datasets[index];
            _plot.ProcessLog = _process_log;
            _plot.PlotResults();
            _report_filename = _plot.BuildReport(textBoxCaptureFile.Text);
        }

        /// <summary>
        /// Build the chart bars list based on the desired colors and percentages
        /// </summary>
        private void BuildChartBars()
        {
            _chart_bar = new List<ChartBar>();

            if (Constants.Bar1EnabledDefault)
                _chart_bar.Add(new ChartBar(Constants.Bar1ColorDefault, Constants.Bar1PercentDefault));
            if (Constants.Bar2EnabledDefault)
                _chart_bar.Add(new ChartBar(Constants.Bar2ColorDefault, Constants.Bar2PercentDefault));
            if (Constants.Bar3EnabledDefault)
                _chart_bar.Add(new ChartBar(Constants.Bar3ColorDefault, Constants.Bar3PercentDefault));
            if (Constants.Bar4EnabledDefault)
                _chart_bar.Add(new ChartBar(Constants.Bar4ColorDefault, Constants.Bar4PercentDefault));
            if (Constants.Bar5EnabledDefault)
                _chart_bar.Add(new ChartBar(Constants.Bar5ColorDefault, Constants.Bar5PercentDefault));
            if (Constants.Bar6EnabledDefault)
                _chart_bar.Add(new ChartBar(Constants.Bar6ColorDefault, Constants.Bar6PercentDefault));
            if (Constants.Bar7EnabledDefault)
                _chart_bar.Add(new ChartBar(Constants.Bar7ColorDefault, Constants.Bar7PercentDefault));
            if (Constants.Bar8EnabledDefault)
                _chart_bar.Add(new ChartBar(Constants.Bar8ColorDefault, Constants.Bar8PercentDefault));
            if (Constants.Bar9EnabledDefault)
                _chart_bar.Add(new ChartBar(Constants.Bar9ColorDefault, Constants.Bar9PercentDefault));
            if (Constants.Bar10EnabledDefault)
                _chart_bar.Add(new ChartBar(Constants.Bar10ColorDefault, Constants.Bar10PercentDefault));
        }

        /// <summary>
        /// Build the list of Wireshark field types
        /// </summary>
        private void BuildWiresharkFieldTypes()
        {
            _wireshark_field_types = new List<WiresharkFieldType>();

            _wireshark_field_types.Add(WiresharkFieldType.None);
            _wireshark_field_types.Add(WiresharkFieldType.Protocol);
            _wireshark_field_types.Add(WiresharkFieldType.Boolean);
            _wireshark_field_types.Add(WiresharkFieldType.UInt8);
            _wireshark_field_types.Add(WiresharkFieldType.UInt16);
            _wireshark_field_types.Add(WiresharkFieldType.UInt24);
            _wireshark_field_types.Add(WiresharkFieldType.UInt32);
            _wireshark_field_types.Add(WiresharkFieldType.UInt64);
            _wireshark_field_types.Add(WiresharkFieldType.Int8);
            _wireshark_field_types.Add(WiresharkFieldType.Int16);
            _wireshark_field_types.Add(WiresharkFieldType.Int24);
            _wireshark_field_types.Add(WiresharkFieldType.Int32);
            _wireshark_field_types.Add(WiresharkFieldType.Int64);
            _wireshark_field_types.Add(WiresharkFieldType.Float);
            _wireshark_field_types.Add(WiresharkFieldType.Double);
            _wireshark_field_types.Add(WiresharkFieldType.AbsoluteTime);
            _wireshark_field_types.Add(WiresharkFieldType.RelativeTime);
            _wireshark_field_types.Add(WiresharkFieldType.String);
            _wireshark_field_types.Add(WiresharkFieldType.StringZ);
            _wireshark_field_types.Add(WiresharkFieldType.EBCDIC);
            _wireshark_field_types.Add(WiresharkFieldType.UIntString);
            _wireshark_field_types.Add(WiresharkFieldType.Ether);
            _wireshark_field_types.Add(WiresharkFieldType.Bytes);
            _wireshark_field_types.Add(WiresharkFieldType.UIntBytes);
            _wireshark_field_types.Add(WiresharkFieldType.IPv4);
            _wireshark_field_types.Add(WiresharkFieldType.IPv6);
            _wireshark_field_types.Add(WiresharkFieldType.IPXNet);
            _wireshark_field_types.Add(WiresharkFieldType.FrameNum);
            _wireshark_field_types.Add(WiresharkFieldType.PCRE);
            _wireshark_field_types.Add(WiresharkFieldType.GUID);
            _wireshark_field_types.Add(WiresharkFieldType.OID);
            _wireshark_field_types.Add(WiresharkFieldType.NumTypes);
        }

        #endregion
    }
}
