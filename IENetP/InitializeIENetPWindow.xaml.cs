﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml.Serialization;

using IENetP.Generic;
using IENetP.Network;
using IENetP.Wireshark;

namespace IENetP
{
    /// <summary>
    /// Interaction logic for InitializeIENetP.xaml
    /// </summary>
    public partial class InitializeIENetPWindow : Window
    {
        #region Constructors

        public InitializeIENetPWindow()
        {
            InitializeComponent();

            _worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            _worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
        }

        #endregion

        #region Data Objects

        BackgroundWorker _worker = new BackgroundWorker();
        DispatcherTimer _timer = new DispatcherTimer();
        bool? _result = false;
        string _status_message = "";

        #endregion

        #region Properties

        private string _application_directory = "";
        /// <summary>
        /// Sets the running application's directory
        /// </summary>
        public string ApplicationDirectory
        {
            set { _application_directory = value; }
        }

        private string _temporary_directory = "";
        /// <summary>
        /// Sets the environment's temporary directory
        /// </summary>
        public string TemporaryDirectory
        {
            set { _temporary_directory = value; }
        }

        private Capinfos _capinfos_app = new Capinfos();
        /// <summary>
        /// Gets or sets the Capinfos instance
        /// </summary>
        public Capinfos CapinfosApp
        {
            get { return _capinfos_app; }
            set { _capinfos_app = value; }
        }

        private TShark _tshark_app = new TShark();
        /// <summary>
        /// Gets or sets the TShark instance
        /// </summary>
        public TShark TSharkApp
        {
            get { return _tshark_app; }
            set { _tshark_app = value; }
        }

        private bool _build_new_fields_file = true;
        /// <summary>
        /// Gets a value indicating whether the application should generate a new Wireshark fields file after initializing the application
        /// </summary>
        public bool BuildNewFieldsFile
        {
            get { return _build_new_fields_file; }
        }

        private WiresharkFields _current_wireshark_fields = new WiresharkFields();
        /// <summary>
        /// Gets the current list of wireshark protocols and header fields
        /// </summary>
        /// <remarks>Only valid if the Wireshark fields file was valid and current.  Otherwise, returns a default WiresharkFields class instance.</remarks>
        public WiresharkFields CurrentWiresharkFields
        {
            get { return _current_wireshark_fields; }
        }

        private string _error_message = "";
        /// <summary>
        /// Gets any error messages
        /// </summary>
        /// <value>Returns a <c>string</c> containing any error messages, or an empty <c>string</c> if no errors occured.</value>
        public string ErrorMessage
        {
            get { return _error_message; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initialize the IENetP application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (InitializeWiresharkApplications())
            {
                if (ReadWiresharkFieldsFile())
                {
                    _result = true;
                }
            }
        }

        /// <summary>
        /// Runs after the process is finished
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.DialogResult = _result;
            this.Close();
        }

        /// <summary>
        /// Initialize Wireshark applications and report whether they are valid or not
        /// </summary>
        /// <returns>A <c>bool</c> indicating whether the capinfos and tshark applications run correctly</returns>
        private bool InitializeWiresharkApplications()
        {
            _status_message = "Validating Wireshark Applications";

            _capinfos_app.ValidateApplication();
            if (_capinfos_app.IsValid == false)
            {
                _error_message = "Capinfos Not Valid";
                return false;
            }

            _tshark_app.ValidateApplication();
            if (_tshark_app.IsValid == false)
            {
                _error_message = "TShark Not Valid";
                return false;
            }

            return true;
        }

        /// <summary>
        /// Read the IENetP Wireshark fields file.
        /// If the file exists, read the file and check the version against the currently installed version of Tshark.
        /// If the file doesn't exist or the version disagree, build a new version of the Wireshark fields file.
        /// </summary>
        /// <returns>A <c>bool</c> indicating whether the Wireshark fields file was read correctly and/or a new version was built correctly</returns>
        private bool ReadWiresharkFieldsFile()
        {
            _status_message = "Reading Wireshark Fields File";

            FileInfo fields_file = new FileInfo(_application_directory + "\\" + Constants.IENetPFileHeader + Constants.WiresharkFieldsFileName + Constants.XmlFileExtension);
            if (fields_file.Exists)
            {
                // Deserialize the fields file
                XmlSerializer deserializer = new XmlSerializer(typeof(WiresharkFields));
                TextReader reader = new StreamReader(fields_file.FullName);
                _current_wireshark_fields = (WiresharkFields)deserializer.Deserialize(reader);
                reader.Close();

                if (_current_wireshark_fields.WiresharkVersion == _tshark_app.VersionString)
                    _build_new_fields_file = false;
            }

            return true;
        }

        #endregion

        #region Event Handlers

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _timer.Interval = TimeSpan.FromMilliseconds(10);
            _timer.Tick += timer_Task;
            _timer.Start();

            _worker.WorkerSupportsCancellation = true;
            _worker.RunWorkerAsync();
        }

        private void timer_Task(object sender, EventArgs e)
        {
            textBlockStatus.Text = _status_message;
        }

        #endregion
    }
}
