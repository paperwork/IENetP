################################################################################
################################################################################
##                                                                            ##
##                              IENetP CHANGE LOG                             ##
##                                                                            ##
################################################################################
################################################################################

This file contains a change log of all the features and bugs that have been 
added or fixed in each version of the IENetP software.

If you would like to request a feature, please visit the IENetP Feature Request 
Tracker (https://sourceforge.net/tracker/?group_id=222438&atid=1055616). 

If you would like to report a report a bug, please visit the IENetP Bug Tracker 
(https://sourceforge.net/tracker/?group_id=222438&atid=1055613).

For more information on IENetP, please visit the IENetP Website 
(http://ienetp.sourceforge.net).

--------------------------------------------------------------------------------
  Things To Do/Fix
--------------------------------------------------------------------------------
Short-Term Things To Do</h3>
  * Add chart and report options.
  * Fix New/Open/Save feature.
  * Add downloadable user guide and in-program help.
  * Add protocol definition window and application settings. These will allow
    the user to develop their own protocol filters, sorting fields, and override
    analysis variables. Specifically, add the ability to override the time
    value.
  * Add full packet or packet data in to PSML output.

Long-Term Things To Do
  * Add in PSML schema checking.
  * Add mathematical modeling of data to report more than just Gaussian
    statistical data, since it doesn't apply for many cases.
  * Improve performance of charting package.  The Microsoft charting feature as
    it's currently implemented takes a long time to display for older machines.
  * See if there's some way to label points in the graph.  This is possibly
    disabled using the Microsoft Charting FastPoint plot.

Bugs To Fix
  * Software crashes when there are not enough packets detected.  Trying to
    access an array with no members.
  * Software crashes when the report file and/or image can't be written due to
    permission issues.
  * Software doesn't work properly unless the language is set to English (United
    States).  This seems to be an issue with the time and date format.
  * 64-bit installer may have errors.  Received a report that the installer
    returns the error "This installation package could not be opened. Contact
    the application vendor to verify that this is a valid Windows Installer
    package."

--------------------------------------------------------------------------------
  Version 1.2.0 (Current)
--------------------------------------------------------------------------------
Published: 2011/10/14

Features Added
  * Deletes temporary files on close.
  * Generates an XML-formatted file containing the list of protocols and field
    headers for the currently installed and used version of Wireshark and
    writes that file to the application's installation directory.
  * Runs initialization dialog window on startup to check for Wireshark and
    perform other initialization operations.
  * Changed build script so that it should work on both 32-bit and 64-bit
    systems.

Bugs Fixed
  * Removed absolute HTML link to graphic image file in report file.

--------------------------------------------------------------------------------
Version 1.1.2
--------------------------------------------------------------------------------
Published: 2011/02/11

Features Added
  * Changed TShark to force specific columns in the output.  This resolves a
    long term issue when people had non-standard Wireshark installations with
    different columns defined in their preferences.

Bugs Fixed
  * Set TShark to not resolve addresses.  This error led to difficulties in
    interpreting MAC addresses for known manufacturers.

--------------------------------------------------------------------------------
  Version 1.1.1
--------------------------------------------------------------------------------
Published: 2011/02/10

Features Added
  * Added change log to code and website.
  * Moved analysis functionality to MainWindow to reduce complexity of code and 
    improve usage of software.
  * Added basic CSS to report file.
  * Added "Additional Information" to report file.
  * Improved linkage between Wireshark and IENetP. Saving intermediate PSML 
    file then importing packets from that file. Makes packet interpretation 
    more robust, since it eliminates much of the touchy text processing.

Bugs Fixed
  * Default Wireshark filter now updates properly if the IP address box is 
    cleared.

--------------------------------------------------------------------------------
  Version 1.1.0
--------------------------------------------------------------------------------
Published: 2011/01/18

Features Added
  * Converted software from Microsoft .NET 2.0 and Windows Forms to .NET 4.0 
    and Windows Presentation Foundation (WPF).
  * Removed proprietary charting software and replaced it with Microsoft 
    charting.
  * Simplified interface by removing the multiple screens to enter data and 
    removing the required fields.
  * Added ability to override the default Wireshark filter.

Bugs Fixed
  * 

--------------------------------------------------------------------------------
  Version 1.0.1
--------------------------------------------------------------------------------
Published: 2009/04/02

Features Added
  * 

Bugs Fixed
  * Fixed problem with histogram graphing  code that caused the software to 
    crash occasionally.
  * Fixed naming convention for some of the graphics resources that caused 
    compiler warnings.

--------------------------------------------------------------------------------
  Version 1.0.0
--------------------------------------------------------------------------------
Published: 2009/03/04

Features Added
  * Initial version of software.

Bugs Fixed
  * 
