﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml.Serialization;

using IENetP.Generic;
using IENetP.Network;
using IENetP.Wireshark;

namespace IENetP
{
    /// <summary>
    /// Interaction logic for BuildWiresharkFields.xaml
    /// </summary>
    public partial class BuildWiresharkFieldsWindow : Window
    {
        #region Constructors

        public BuildWiresharkFieldsWindow()
        {
            InitializeComponent();

            _worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            _worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
        }

        #endregion

        #region Data Objects

        BackgroundWorker _worker = new BackgroundWorker();
        DispatcherTimer _timer = new DispatcherTimer();
        bool? _result = false;
        string _status_message = "";
        #endregion

        #region Properties

        private string _application_directory = "";
        /// <summary>
        /// Sets the running application's directory
        /// </summary>
        public string ApplicationDirectory
        {
            set { _application_directory = value; }
        }

        private string _temporary_directory = "";
        /// <summary>
        /// Sets the environment's temporary directory
        /// </summary>
        public string TemporaryDirectory
        {
            set { _temporary_directory = value; }
        }

        private TShark _tshark_app = new TShark();
        /// <summary>
        /// Gets or sets the TShark instance to use when generating the fields file
        /// </summary>
        public TShark TSharkApp
        {
            get { return _tshark_app; }
            set { _tshark_app = value; }
        }

        private string _output_filename = "";
        /// <summary>
        /// Gets a string containing the fully qualified name of the wireshark fields XML file
        /// </summary>
        /// <value>A <c>string</c> containing the fully qualified name of the wireshark fields XML file.
        /// Returns an empty string if the process did not complete correctly</value>
        public string OutputFilename
        {
            get { return _output_filename; }
        }

        private WiresharkFields _current_wireshark_fields = new WiresharkFields();
        /// <summary>
        /// Gets the current list of Wireshark protocols and header fields
        /// </summary>
        public WiresharkFields CurrentWiresharkFields
        {
            get { return _current_wireshark_fields; }
        }

        private string _error_message = "";
        /// <summary>
        /// Gets any error messages
        /// </summary>
        /// <value>Returns a <c>string</c> containing any error messages, or an empty <c>string</c> if no errors occured.</value>
        public string ErrorMessage
        {
            get { return _error_message; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Build the Wireshark fields XML file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (BuildBaseFieldsFile())
            {
                if (ReadBaseFieldsFile())
                {
                    if (ExportXmlFieldsFile())
                    {
                        _result = true;
                    }
                    else
                        _error_message = "Export XML Fields failed";
                }
            }
        }

        /// <summary>
        /// Runs after the process is finished
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.DialogResult = _result;
            this.Close();
        }

        /// <summary>
        /// Use TShark to generate the fields text file in Wireshark format
        /// </summary>
        /// <returns>A <c>bool</c> indicating whether the operation completed successfully</returns>
        private bool BuildBaseFieldsFile()
        {
            if (_tshark_app.IsValid == null)
            {
                _status_message = "Validating TShark";
                _tshark_app.ValidateApplication();
            }

            if (_tshark_app.IsValid == true)
            {
                _status_message = "Generating Fields File";
                _tshark_app.RedirectToTemporaryFile = true;
                _tshark_app.TemporaryFile = new FileInfo(_temporary_directory + "\\" + Constants.IENetPFileHeader + Constants.WiresharkFieldsFileName + Constants.TextFileExtension);
                if (_tshark_app.BuildWiresharkFields() != Enums.AnalysisStatus.Success)
                {
                    _error_message = "Problem Generating Fields File";
                    return false;
                }
            }
            else
            {
                _error_message = "TShark Not Valid";
                return false;
            }

            return true;
        }

        /// <summary>
        /// Read the TShark formatted fields file and build the lists of protocols and header fields
        /// </summary>
        /// <returns>A <c>bool</c> indicating whether the operation completed successfully</returns>
        private bool ReadBaseFieldsFile()
        {
            _status_message = "Reading Wireshark Fields File";

            StreamReader _sr_in = File.OpenText(_tshark_app.TemporaryFile.FullName);
            string _line = "";

            _current_wireshark_fields.WiresharkVersion = _tshark_app.VersionString;
            int p_fail = 0;
            int f_fail = 0;
            while (_sr_in.Peek() >= 0)
            {
                _line = _sr_in.ReadLine();
                if (_line.StartsWith("P", true, null))
                {
                    WiresharkProtocol _p = new WiresharkProtocol();
                    if (_p.TryParse(_line))
                    {
                        _current_wireshark_fields.WiresharkProtocols.Add(_p);
                    }
                    else
                        p_fail++;
                }
                else if (_line.StartsWith("F", true, null))
                {
                    WiresharkHeaderField _f = new WiresharkHeaderField(_tshark_app.VersionNumber);
                    if (_f.TryParse(_line))
                    {
                        _current_wireshark_fields.WiresharkHeaderFields.Add(_f);
                    }
                    else
                    {
                        f_fail++;
                    }
                }
            }



            _sr_in.Close();

            return true;
        }

        /// <summary>
        /// Export an XML formatted file containing the protocols and header fields
        /// </summary>
        /// <returns>A <c>bool</c> indicating whether the operation completed successfully</returns>
        private bool ExportXmlFieldsFile()
        {
            _status_message = "Exporting XML Fields File";

            string new_fields_file = _application_directory + _tshark_app.TemporaryFile.Name.Remove(_tshark_app.TemporaryFile.Name.LastIndexOf(_tshark_app.TemporaryFile.Extension)) + Constants.XmlFileExtension;
            XmlSerializer serializer = new XmlSerializer(typeof(WiresharkFields));
            TextWriter writer = new StreamWriter(new_fields_file);
            serializer.Serialize(writer, _current_wireshark_fields);
            writer.Close();

            return true;
        }

        #endregion

        #region Event Handlers

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _timer.Interval = TimeSpan.FromMilliseconds(10);
            _timer.Tick += timer_Task;
            _timer.Start();

            _worker.WorkerSupportsCancellation = true;
            _worker.RunWorkerAsync();
        }

        private void timer_Task(object sender, EventArgs e)
        {
            textBlockStatus.Text = _status_message;
            textBlockProtocols.Text = _current_wireshark_fields.WiresharkProtocols.Count.ToString();
            textBlockHeaderFields.Text = _current_wireshark_fields.WiresharkHeaderFields.Count.ToString();
        }

        #endregion
    }
}
