﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

//using IENetP.Generic;
//using IENetP.Network;
using IENetP.Data;

namespace IENetP
{
    /// <summary>
    /// Interaction logic for AdditionalInfoWindow.xaml
    /// </summary>
    public partial class AdditionalInfoWindow : Window
    {

        #region Constructors

        public AdditionalInfoWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Data Objects

        private AnalysisSettings _analysis_settings = new AnalysisSettings();
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        public AnalysisSettings Settings
        {
            get { return _analysis_settings; }
            set { _analysis_settings = value; }
        }

        #endregion

        #region Event Handlers

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            textBoxTestName.Text = _analysis_settings.TestInfo.Name;
            //textBoxTestId.Text = analysis_settings.TestInfo.IdNumber.ToString();
            textBoxTestDescription.Text = _analysis_settings.TestInfo.Description;
            textBoxTestComment.Text = _analysis_settings.TestInfo.Comment;

            textBoxDeviceName.Text = _analysis_settings.DeviceInfo.Name;
            textBoxDeviceManufacturer.Text = _analysis_settings.DeviceInfo.Manufacturer;
            //textBoxDeviceId.Text = analysis_settings.DeviceInfo.IdNumber.ToString();
            textBoxDeviceDescription.Text = _analysis_settings.DeviceInfo.Description;
            textBoxDeviceModel.Text = _analysis_settings.DeviceInfo.Model;
            textBoxDeviceSerialNumber.Text = _analysis_settings.DeviceInfo.SerialNumber;
            textBoxDeviceFirmwareVersion.Text = _analysis_settings.DeviceInfo.FirmwareVersion;
            textBoxDeviceComment.Text = _analysis_settings.DeviceInfo.Comment;
        }

        private void buttonOK_Click(object sender, RoutedEventArgs e)
        {
            //int temp;
            //Nullable<bool> result;

            _analysis_settings.TestInfo.Name = textBoxTestName.Text;
            //result = int.TryParse(textBoxTestId.Text, out temp);
            //if (result == true)
            //    analysis_settings.TestInfo.IdNumber = temp;
            _analysis_settings.TestInfo.Description = textBoxTestDescription.Text;
            _analysis_settings.TestInfo.Comment = textBoxTestComment.Text;

            _analysis_settings.DeviceInfo.Name = textBoxDeviceName.Text;
            _analysis_settings.DeviceInfo.Manufacturer = textBoxDeviceManufacturer.Text;
            //result = int.TryParse(textBoxDeviceId.Text, out temp);
            //if (result == true)
            //    analysis_settings.DeviceInfo.IdNumber = temp;
            _analysis_settings.DeviceInfo.Description = textBoxDeviceDescription.Text;
            _analysis_settings.DeviceInfo.Model = textBoxDeviceModel.Text;
            _analysis_settings.DeviceInfo.SerialNumber = textBoxDeviceSerialNumber.Text;
            _analysis_settings.DeviceInfo.FirmwareVersion = textBoxDeviceFirmwareVersion.Text;
            _analysis_settings.DeviceInfo.Comment = textBoxDeviceComment.Text;

            this.DialogResult = true;
            this.Close();
        }

        #endregion

        private void textBoxTestName_GotFocus(object sender, RoutedEventArgs e)
        {
            textBoxTestName.SelectAll();
        }

        //private void textBoxTestId_GotFocus(object sender, RoutedEventArgs e)
        //{
        //    textBoxTestId.SelectAll();
        //}

        private void textBoxTestDescription_GotFocus(object sender, RoutedEventArgs e)
        {
            textBoxTestDescription.SelectAll();
        }

        private void textBoxTestComment_GotFocus(object sender, RoutedEventArgs e)
        {
            textBoxTestComment.SelectAll();
        }

        private void textBoxDeviceName_GotFocus(object sender, RoutedEventArgs e)
        {
            textBoxDeviceName.SelectAll();
        }

        private void textBoxDeviceManufacturer_GotFocus(object sender, RoutedEventArgs e)
        {
            textBoxDeviceManufacturer.SelectAll();
        }

        //private void textBoxDeviceId_GotFocus(object sender, RoutedEventArgs e)
        //{
        //    textBoxDeviceId.SelectAll();
        //}

        private void textBoxDeviceDescription_GotFocus(object sender, RoutedEventArgs e)
        {
            textBoxDeviceDescription.SelectAll();
        }

        private void textBoxDeviceModel_GotFocus(object sender, RoutedEventArgs e)
        {
            textBoxDeviceModel.SelectAll();
        }

        private void textBoxDeviceSerialNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            textBoxDeviceSerialNumber.SelectAll();
        }

        private void textBoxDeviceFirmwareVersion_GotFocus(object sender, RoutedEventArgs e)
        {
            textBoxDeviceFirmwareVersion.SelectAll();
        }

        private void textBoxDeviceComment_GotFocus(object sender, RoutedEventArgs e)
        {
            textBoxDeviceComment.SelectAll();
        }
    }
}
