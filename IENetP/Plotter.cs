﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;        // Manually browsed for reference c:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\System.Web.dll 
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using IENetP.Generic;
using IENetP.Network;
using IENetP.Data;

namespace IENetP
{
    /// <summary>
    /// 
    /// </summary>
    public class Plotter
    {
        #region Constructors

        #endregion

        #region Data Objects

        private Title _chart_title;
        private ChartArea _chart_area_xy;
        private Series _chart_series_xy;
        private ChartArea _chart_area_histogram;
        private Series _chart_series_histogram;

        #endregion

        #region Properties

        private TestDataset _dataset = new TestDataset();
        /// <summary>
        /// Dataset containing the dataset identification, the dataset points, and the analyzed results
        /// </summary>
        public TestDataset Dataset
        {
            get { return _dataset; }
            set { _dataset = value; }
        }

        private List<string> _process_log = new List<string>();
        /// <summary>
        /// Process log that goes along with the dataset
        /// </summary>
        public List<string> ProcessLog
        {
            get { return _process_log; }
            set { _process_log = value; }
        }

        private Chart _chart_main = new Chart();
        /// <summary>
        /// Chart to manipulate when plotting data
        /// </summary>
        public Chart ChartMain
        {
            get { return _chart_main; }
            set { _chart_main = value; }
        }

        private List<ChartBar> _chart_bars = new List<ChartBar>();
        /// <summary>
        /// List of colored bars for the chart
        /// </summary>
        public List<ChartBar> ChartBars
        {
            get { return _chart_bars; }
            set { _chart_bars = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Clear the chart before generating a new plot
        /// </summary>
        public void ClearChart()
        {
            _chart_main.Titles.Clear();
            _chart_main.ChartAreas.Clear();
            _chart_main.Series.Clear();

            _chart_title = new Title();
            _chart_area_xy = new ChartArea("ChartAreaXY");
            _chart_series_xy = new Series("SeriesXY");
            _chart_area_histogram = new ChartArea("ChartAreaHistogram");
            _chart_series_histogram = new Series("SeriesHistogram");
            _chart_main.Invalidate();
        }

        /// <summary>
        /// Reset the zoom factor for the chart
        /// </summary>
        public void ResetZoom()
        {
            _chart_area_histogram.AxisX.ScaleView.ZoomReset(0);
            _chart_area_histogram.AxisY.ScaleView.ZoomReset(0);
            _chart_area_xy.AxisX.ScaleView.ZoomReset(0);
            _chart_area_xy.AxisY.ScaleView.ZoomReset(0);
        }

        /// <summary>
        /// Add colored bars to the graph to represent different percentages from the statistical mean of the data
        /// </summary>
        private void AddColoredBars()
        {
            StripLine bar;

            double interval1, interval2;
            double offset;
            double width;
            double mean = _dataset.JitterStats.Mean;

            for (int i = 0; i < _chart_bars.Count; i++)
            {
                interval1 = mean * _chart_bars[i].MaxPercent / 100;
                if (i == 0)
                    interval2 = 0;
                else
                    interval2 = mean * _chart_bars[i - 1].MaxPercent / 100;
                width = interval1 - interval2;

                offset = mean - interval1;
                bar = new StripLine();
                bar.BackColor = _chart_bars[i].BackColor;
                bar.IntervalOffset = offset;
                bar.StripWidth = width;
                _chart_area_xy.AxisY.StripLines.Add(bar);
                bar = new StripLine();
                bar.BackColor = _chart_bars[i].BackColor;
                bar.IntervalOffset = offset;
                bar.StripWidth = width;
                _chart_area_histogram.AxisY.StripLines.Add(bar);

                offset = mean + interval2;
                bar = new StripLine();
                bar.BackColor = _chart_bars[i].BackColor;
                bar.IntervalOffset = offset;
                bar.StripWidth = width;
                _chart_area_xy.AxisY.StripLines.Add(bar);
                bar = new StripLine();
                bar.BackColor = _chart_bars[i].BackColor;
                bar.IntervalOffset = offset;
                bar.StripWidth = width;
                _chart_area_histogram.AxisY.StripLines.Add(bar);
            }

        }

        /// <summary>
        /// Plot the results
        /// </summary>
        public void PlotResults()
        {
            ClearChart();

            _chart_title = new Title(_dataset.DatasetIdent.Name, Docking.Top, new Font("Trebuchet MS", 16, System.Drawing.FontStyle.Bold), System.Drawing.Color.DarkBlue);
            _chart_main.Titles.Add(_chart_title);
            _chart_main.AntiAliasing = AntiAliasingStyles.All;

            _chart_area_histogram.AlignWithChartArea = "ChartAreaXY";
            _chart_area_histogram.AlignmentOrientation = AreaAlignmentOrientations.Horizontal;
            _chart_area_histogram.AxisX.IsReversed = true;
            _chart_area_histogram.AxisX.IsStartedFromZero = true;
            _chart_area_histogram.AxisX.MajorGrid.LineColor = System.Drawing.Color.LightGray;
            _chart_area_histogram.AxisX.Minimum = 0;
            _chart_area_histogram.AxisX.Title = "Histogram Count";
            _chart_area_histogram.AxisX.TitleFont = new Font("Trebuchet MS", 12, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic);
            _chart_area_histogram.AxisY.IsStartedFromZero = false;
            _chart_area_histogram.AxisY.LabelStyle.Enabled = false;
            _chart_area_histogram.AxisY.MajorGrid.LineColor = System.Drawing.Color.LightGray;
            _chart_area_histogram.Position = new ElementPosition(3, 15, 17, 85);
            _chart_area_xy.AxisX.IsStartedFromZero = false;
            _chart_area_xy.AxisX.LabelStyle.Font = new Font("Trebuchet MS", 10, System.Drawing.FontStyle.Bold);
            _chart_area_xy.AxisX.MajorGrid.LineColor = System.Drawing.Color.LightGray;
            _chart_area_xy.AxisX.ScaleView.Zoomable = true;
            _chart_area_xy.AxisX.Title = "Test Time (s)";
            _chart_area_xy.AxisX.TitleFont = new Font("Trebuchet MS", 12, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic);
            _chart_area_xy.AxisY.IsStartedFromZero = false;
            _chart_area_xy.AxisY.LabelStyle.Font = new Font("Trebuchet MS", 10, System.Drawing.FontStyle.Bold);
            _chart_area_xy.AxisY.MajorGrid.LineColor = System.Drawing.Color.LightGray;
            _chart_area_xy.AxisY.ScaleView.MinSize = 0;
            _chart_area_xy.AxisY.ScaleView.Zoomable = true;
            _chart_area_xy.AxisY.Title = "Measured Packet Interval (ns)";
            _chart_area_xy.AxisY.TitleFont = new Font("Trebuchet MS", 12, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic);
            _chart_area_xy.CursorX.IsUserEnabled = true;
            _chart_area_xy.CursorX.IsUserSelectionEnabled = true;
            _chart_area_xy.CursorY.Interval = Math.Pow(10, -(Constants.StatisticsDecimalPlaces + Constants.StatisticsDecimalOffsetExponentDefault));
            _chart_area_xy.CursorY.IsUserEnabled = true;
            _chart_area_xy.CursorY.IsUserSelectionEnabled = true;
            _chart_area_xy.Position = new ElementPosition(20, 15, 77, 85);

            _chart_series_xy.ChartArea = "ChartAreaXY";
            _chart_series_xy.ChartType = SeriesChartType.FastPoint;
            _chart_series_xy.Color = System.Drawing.Color.Navy;
            _chart_series_xy.MarkerSize = 2;
            for (int i = 0; i < _dataset.Points.Count; i++)
                _chart_series_xy.Points.Add(new DataPoint(_dataset.Points[i].X, _dataset.Points[i].Y));

            _chart_series_histogram.ChartArea = "ChartAreaHistogram";
            _chart_series_histogram.ChartType = SeriesChartType.Line;
            _chart_series_histogram.Color = System.Drawing.Color.Navy;
            // Reverse the X/Y points for the histogram since it is being displayed veritically against the time-domain plot instead of horizontally
            for (int i = 0; i < _dataset.Histogram.Count; i++)
                _chart_series_histogram.Points.Add(new DataPoint(_dataset.Histogram[i].Y, _dataset.Histogram[i].X));


            _chart_main.ChartAreas.Add(_chart_area_xy);
            _chart_main.ChartAreas.Add(_chart_area_histogram);
            _chart_main.Series.Add(_chart_series_xy);
            _chart_main.Series.Add(_chart_series_histogram);

            AddColoredBars();
        }

        /// <summary>
        /// Build a HTML report of the results containing the calculated statistics and the data chart
        /// </summary>
        /// <param name="filename"><c>string</c> representing the full path to the Wireshark capture file</param>
        /// <returns><c>string</c> representing the report filename</returns>
        /// <remarks>Right now, the image is exported as the same size it appears in the main screen.</remarks>
        public string BuildReport(string filename)
        {
            // Export the image to the same directory as the capture file
            FileInfo capture_file = new FileInfo(filename);
            string dir = capture_file.DirectoryName;
            string name = capture_file.Name;
            string ext = capture_file.Extension;
            string basename = name.Remove(name.LastIndexOf(ext));
            string chart_filename = dir + "\\" + basename + ".png";
            _chart_main.SaveImage(chart_filename, ChartImageFormat.Png);

            // ----------------------------------------------------------------
            // Header for Report File
            // ----------------------------------------------------------------
            string html_filename = dir + "\\" + basename + ".html";
            StreamWriter writer = new StreamWriter(html_filename, false);
            HtmlTextWriter html_writer = new HtmlTextWriter(writer);
            html_writer.RenderBeginTag(HtmlTextWriterTag.Html);         // <html>
            html_writer.RenderBeginTag(HtmlTextWriterTag.Head);         // <head>
            html_writer.RenderBeginTag(HtmlTextWriterTag.Title);        // <title>
            html_writer.Write("IENetP Report - " + filename);
            html_writer.RenderEndTag();                                 // </title>

            // Stylesheet
            html_writer.RenderBeginTag(HtmlTextWriterTag.Style);        // <style>
            html_writer.Write("body { ");
            html_writer.WriteStyleAttribute("font-family", "Arial, Helvetica, sans-serif");
            html_writer.WriteLine(" }");
            html_writer.Write("h1 { ");
            html_writer.WriteStyleAttribute("font-weight", "bold");
            html_writer.WriteStyleAttribute("text-transform", "uppercase");
            html_writer.WriteStyleAttribute("text-align", "center");
            html_writer.WriteStyleAttribute("padding", "18px");
            html_writer.WriteStyleAttribute("border", "thick solid #FFFF99");
            html_writer.WriteStyleAttribute("background-color", "#004000");
            html_writer.WriteStyleAttribute("color", "#CCFFFF");
            html_writer.WriteLine(" }");
            html_writer.Write("h2 { ");
            html_writer.WriteStyleAttribute("font-weight", "bold");
            html_writer.WriteStyleAttribute("background-color", "#FFFF99");
            html_writer.WriteLine(" }");
            html_writer.Write("h3 { ");
            html_writer.WriteStyleAttribute("text-decoration", "underline");
            html_writer.WriteLine(" }");
            html_writer.Write("table.info { ");
            html_writer.WriteLine(" }");
            html_writer.Write("th.info { ");
            html_writer.WriteStyleAttribute("background-color", "#004000");
            html_writer.WriteStyleAttribute("color", "#EEFFEE");
            html_writer.WriteStyleAttribute("padding", "2px");
            html_writer.WriteStyleAttribute("width", "100px");
            html_writer.WriteLine(" }");
            html_writer.Write("td.info_description { ");
            html_writer.WriteStyleAttribute("background-color", "#E0F0E0");
            html_writer.WriteStyleAttribute("padding", "1px");
            html_writer.WriteLine(" }");
            html_writer.Write("td.info_value { ");
            html_writer.WriteStyleAttribute("background-color", "#E8F8E8");
            html_writer.WriteStyleAttribute("padding", "1px");
            html_writer.WriteLine(" }");
            html_writer.Write("table.results { ");
            html_writer.WriteLine(" }");
            html_writer.Write("th.results { ");
            html_writer.WriteStyleAttribute("background-color", "#000040");
            html_writer.WriteStyleAttribute("color", "#EEEEFF");
            html_writer.WriteStyleAttribute("padding", "2px");
            html_writer.WriteStyleAttribute("width", "100px");
            html_writer.WriteLine(" }");
            html_writer.Write("td.results_description { ");
            html_writer.WriteStyleAttribute("background-color", "#E8E8F8");
            html_writer.WriteStyleAttribute("padding", "1px");
            html_writer.WriteLine(" }");
            html_writer.Write("td.results_value { ");
            html_writer.WriteStyleAttribute("background-color", "#EEEEFF");
            html_writer.WriteStyleAttribute("padding", "1px");
            html_writer.WriteStyleAttribute("text-align", "center");
            html_writer.WriteLine(" }");
            html_writer.Write("p.log { ");
            html_writer.WriteStyleAttribute("font-family", "Courier New, monospace");
            html_writer.WriteLine(" }");
            html_writer.RenderEndTag();                                 // </style>

            html_writer.RenderEndTag();                                 // </head>
            html_writer.RenderBeginTag(HtmlTextWriterTag.Body);         // <body>

            html_writer.RenderBeginTag(HtmlTextWriterTag.H1);           // <h1>
            html_writer.Write("IENetP Test Tool Report");
            html_writer.RenderEndTag();                                 // </h1>
            html_writer.RenderBeginTag(HtmlTextWriterTag.P);            // <p>
            html_writer.Write("This report was automatically generated by the IENetP Test Tool.  For more information about the software, please visit the ");
            html_writer.AddAttribute(HtmlTextWriterAttribute.Href, "http://ienetp.sourceforge.net");
            html_writer.AddAttribute(HtmlTextWriterAttribute.Target, "_blank");
            html_writer.RenderBeginTag(HtmlTextWriterTag.A);
            html_writer.Write("IENetP SourceForge Page");
            html_writer.RenderEndTag();                                 // </a>
            html_writer.Write(".");
            html_writer.RenderEndTag();                                 // </p>

            // ----------------------------------------------------------------
            // Information and Settings
            // ----------------------------------------------------------------
            html_writer.RenderBeginTag(HtmlTextWriterTag.H2); html_writer.Write("Information and Settings"); html_writer.RenderEndTag();

            // ----------------------------------------------------------------
            // General Information
            // ----------------------------------------------------------------
            html_writer.RenderBeginTag(HtmlTextWriterTag.H3); html_writer.Write("General Information"); html_writer.RenderEndTag();

            html_writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "5");
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Table);                                                                    // <table>

            html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Th);                                                                       // <td class="...">
            html_writer.Write("Description");
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Th);                                                                       // <td class="...">
            html_writer.Write("Value");
            html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </th></tr>

            html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_description");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.RenderBeginTag(HtmlTextWriterTag.Strong); html_writer.Write("Capture File:"); html_writer.RenderEndTag();
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.Write(name);
            html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </td></tr>

            html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_description");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.RenderBeginTag(HtmlTextWriterTag.Strong); html_writer.Write("Report Generated:"); html_writer.RenderEndTag();
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.Write(DateTime.Now.ToLongDateString() + " @ " + DateTime.Now.ToLongTimeString());
            html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </td></tr>

            html_writer.RenderEndTag();                                                                                             // </table>

            // ----------------------------------------------------------------
            // Additional Information
            // ----------------------------------------------------------------
            html_writer.RenderBeginTag(HtmlTextWriterTag.H3); html_writer.Write("Additional Information"); html_writer.RenderEndTag();

            html_writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "5");
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Table);                                                                    // <table>

            html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Th);                                                                       // <td class="...">
            html_writer.Write("Description");
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Th);                                                                       // <td class="...">
            html_writer.Write("Value");
            html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </th></tr>

            //html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            //html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_description");
            //html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            //html_writer.RenderBeginTag(HtmlTextWriterTag.Strong); html_writer.Write("Test ID:"); html_writer.RenderEndTag();
            //html_writer.RenderEndTag();                                                                                             // </td>
            //html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_value");
            //html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            //html_writer.Write(dataset.Settings.TestInfo.IdNumber);
            //html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </td></tr>

            html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_description");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.RenderBeginTag(HtmlTextWriterTag.Strong); html_writer.Write("Test Name:"); html_writer.RenderEndTag();
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.Write(_dataset.Settings.TestInfo.Name);
            html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </td></tr>

            html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_description");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.RenderBeginTag(HtmlTextWriterTag.Strong); html_writer.Write("Test Description:"); html_writer.RenderEndTag();
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.Write(_dataset.Settings.TestInfo.Description);
            html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </td></tr>

            html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_description");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.RenderBeginTag(HtmlTextWriterTag.Strong); html_writer.Write("Test Comment:"); html_writer.RenderEndTag();
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.Write(_dataset.Settings.TestInfo.Comment);
            html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </td></tr>

            //html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            //html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_description");
            //html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            //html_writer.RenderBeginTag(HtmlTextWriterTag.Strong); html_writer.Write("Device ID:"); html_writer.RenderEndTag();
            //html_writer.RenderEndTag();                                                                                             // </td>
            //html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_value");
            //html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            //html_writer.Write(dataset.Settings.DeviceInfo.IdNumber);
            //html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </td></tr>

            html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_description");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.RenderBeginTag(HtmlTextWriterTag.Strong); html_writer.Write("Device Name:"); html_writer.RenderEndTag();
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.Write(_dataset.Settings.DeviceInfo.Name);
            html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </td></tr>

            html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_description");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.RenderBeginTag(HtmlTextWriterTag.Strong); html_writer.Write("Device Manufacturer:"); html_writer.RenderEndTag();
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.Write(_dataset.Settings.DeviceInfo.Manufacturer);
            html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </td></tr>

            html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_description");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.RenderBeginTag(HtmlTextWriterTag.Strong); html_writer.Write("Device Description:"); html_writer.RenderEndTag();
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.Write(_dataset.Settings.DeviceInfo.Description);
            html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </td></tr>

            html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_description");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.RenderBeginTag(HtmlTextWriterTag.Strong); html_writer.Write("Device Model:"); html_writer.RenderEndTag();
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.Write(_dataset.Settings.DeviceInfo.Model);
            html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </td></tr>

            html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_description");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.RenderBeginTag(HtmlTextWriterTag.Strong); html_writer.Write("Device Serial Number:"); html_writer.RenderEndTag();
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.Write(_dataset.Settings.DeviceInfo.SerialNumber);
            html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </td></tr>

            html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_description");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.RenderBeginTag(HtmlTextWriterTag.Strong); html_writer.Write("Device Firmware Version:"); html_writer.RenderEndTag();
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.Write(_dataset.Settings.DeviceInfo.FirmwareVersion);
            html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </td></tr>

            html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_description");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.RenderBeginTag(HtmlTextWriterTag.Strong); html_writer.Write("Device Comment:"); html_writer.RenderEndTag();
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "info_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.Write(_dataset.Settings.DeviceInfo.Comment);
            html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </td></tr>

            html_writer.RenderEndTag();                                                                                             // </table>

            // ----------------------------------------------------------------
            // Results
            // ----------------------------------------------------------------
            html_writer.RenderBeginTag(HtmlTextWriterTag.H2); html_writer.Write("Results"); html_writer.RenderEndTag();

            // ----------------------------------------------------------------
            // Jitter Statistics
            // ----------------------------------------------------------------
            html_writer.RenderBeginTag(HtmlTextWriterTag.H3); html_writer.Write("Jitter Statistics"); html_writer.RenderEndTag();

            html_writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "5");
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "results");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Table);                                                                    // <table>

            html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "results");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Th);                                                                       // <td class="...">
            html_writer.Write("Statistic");
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "results");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Th);                                                                       // <td class="...">
            html_writer.Write("Value");
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "results");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Th);                                                                       // <td class="...">
            html_writer.Write("Percentage");
            html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </th></tr>

            html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "results_description");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.RenderBeginTag(HtmlTextWriterTag.Strong); html_writer.Write("Mean:"); html_writer.RenderEndTag();
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "results_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            string temp = _dataset.JitterStats.Mean + " ns"; html_writer.Write(temp);
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "results_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.Write("&nbsp;");
            html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </td></tr>

            html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "results_description");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.RenderBeginTag(HtmlTextWriterTag.Strong); html_writer.Write("Minimum:"); html_writer.RenderEndTag();
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "results_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            temp = _dataset.JitterStats.Minimum + " ns"; html_writer.Write(temp);
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "results_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            temp = Math.Round((_dataset.JitterStats.Mean - _dataset.JitterStats.Minimum) / _dataset.JitterStats.Mean * 100, 3).ToString(); html_writer.Write(temp + "%");
            html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </td></tr>

            html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "results_description");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.RenderBeginTag(HtmlTextWriterTag.Strong); html_writer.Write("Maximum:"); html_writer.RenderEndTag();
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "results_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            temp = _dataset.JitterStats.Maximum + " ns"; html_writer.Write(temp);
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "results_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            temp = Math.Round((_dataset.JitterStats.Maximum - _dataset.JitterStats.Mean) / _dataset.JitterStats.Mean * 100, 3).ToString(); html_writer.Write(temp + "%");
            html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </td></tr>

            html_writer.RenderBeginTag(HtmlTextWriterTag.Tr);                                                                       // <tr>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "results_description");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.RenderBeginTag(HtmlTextWriterTag.Strong); html_writer.Write("Standard Deviation:"); html_writer.RenderEndTag();
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "results_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            html_writer.Write(_dataset.JitterStats.StandardDeviation + " ns");
            html_writer.RenderEndTag();                                                                                             // </td>
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "results_value");
            html_writer.RenderBeginTag(HtmlTextWriterTag.Td);                                                                       // <td class="...">
            temp = Math.Round(_dataset.JitterStats.StandardDeviation / _dataset.JitterStats.Mean * 100, 6).ToString(); html_writer.Write(temp + "%");
            html_writer.RenderEndTag(); html_writer.RenderEndTag();                                                                 // </td></tr>
            html_writer.RenderEndTag();                                                                                             // </table>

            // ----------------------------------------------------------------
            // Jitter Graph
            // ----------------------------------------------------------------
            html_writer.RenderBeginTag(HtmlTextWriterTag.H3); html_writer.Write("Jitter Graph"); html_writer.RenderEndTag();

            // Use relative file naming for chart filename
            FileInfo chart_file_info = new FileInfo(chart_filename);
            html_writer.AddAttribute(HtmlTextWriterAttribute.Src, chart_file_info.Name);
            html_writer.RenderBeginTag(HtmlTextWriterTag.Img);          // <img src="..."
            html_writer.RenderEndTag();                                 // />

            // ----------------------------------------------------------------
            // Process Log
            // ----------------------------------------------------------------
            html_writer.RenderBeginTag(HtmlTextWriterTag.H2); html_writer.Write("Process Log"); html_writer.RenderEndTag();
            html_writer.AddAttribute(HtmlTextWriterAttribute.Class, "log");
            html_writer.RenderBeginTag(HtmlTextWriterTag.P);            // <p style="...">
            html_writer.WriteLine("");
            for (int i = 0; i < _process_log.Count; i++)
            {
                html_writer.WriteLine(_process_log[i]); html_writer.WriteBreak();
            }
            html_writer.RenderEndTag();                                 // </p>
            
            html_writer.RenderEndTag();                                 // </body>
            html_writer.RenderEndTag();                                 // </html>
            html_writer.Close();

            // Show the HTML file
            //System.Diagnostics.Process.Start("file:///" + html_filename);

            return html_filename;
        }

        #endregion
    }
}
