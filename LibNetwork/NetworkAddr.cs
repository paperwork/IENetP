﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

using IENetP.Generic;

namespace IENetP.Network
{
    /// <summary>
    /// Generic network address
    /// </summary>
    public class NetworkAddr : ListOfBytes
    {
        /// <summary>
        /// Function to define the list of acceptable delimiters in a generic network addres.
        /// </summary>
        protected override void SetDelimiterList()
        {
            _delimiter_list = new List<char>();
            _delimiter_list.Add(':');
            _delimiter_list.Add('-');
            _delimiter_list.Add(' ');
            _delimiter_list.Add('.');
        }
    }

    /// <summary>
    /// Ethernet Media Access Control (MAC) Address
    /// </summary>
    [Serializable]
    public class EthernetMACAddr : ListOfBytes
    {
        #region Constructors

        /// <summary>
        /// Create a default Ethernet MAC address.
        /// </summary>
        public EthernetMACAddr()
        {
            _list_length = 6;
            _number_of_bytes_per_delimiter = 1;
            _is_hex = true;
            SetDelimiterList();
            _delimiter_index = 0;
            List<byte> list = new List<byte>();
            _is_valid = TryParse("0:0:0:0:0:0", out list);
            if (_is_valid == true)
                _byte_list = list;
        }

        /// <summary>
        /// Create an Ethernet MAC address from the desired string.
        /// </summary>
        /// <param name="address_string">A <c>string</c> containing the desired Ethernet MAC address.</param>
        public EthernetMACAddr(string address_string)
        {
            _list_length = 6;
            _number_of_bytes_per_delimiter = 1;
            _is_hex = true;
            SetDelimiterList();
            _delimiter_index = 0;
            List<byte> list = new List<byte>();
            _is_valid = TryParse(address_string, out list);
            if (_is_valid == true)
                _byte_list = list;
        }

        /// <summary>
        /// Create an Ethernet MAC address from desired bytes.
        /// </summary>
        /// <param name="a1">A <c>byte</c> containing the first byte in the desired Ethernet MAC address.</param>
        /// <param name="a2">A <c>byte</c> containing the second byte in the desired Ethernet MAC address.</param>
        /// <param name="a3">A <c>byte</c> containing the third byte in the desired Ethernet MAC address.</param>
        /// <param name="a4">A <c>byte</c> containing the fourth byte in the desired Ethernet MAC address.</param>
        /// <param name="a5">A <c>byte</c> containing the fifth byte in the desired Ethernet MAC address.</param>
        /// <param name="a6">A <c>byte</c> containing the sixth byte in the desired Ethernet MAC address.</param>
        public EthernetMACAddr(byte a1, byte a2, byte a3, byte a4, byte a5, byte a6)
        {
            _list_length = 6;
            _number_of_bytes_per_delimiter = 1;
            _is_hex = true;
            SetDelimiterList();
            _delimiter_index = 0;
            string delim = _delimiter_list[_delimiter_index].ToString();
            string address_string = a1.ToString() + delim + a2.ToString() + delim + a3.ToString() + delim + a4.ToString() + delim + a5.ToString() + delim + a6.ToString();
            List<byte> list = new List<byte>();
            _is_valid = TryParse(address_string, out list);
            if (_is_valid == true)
                ByteList = list;
        }

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        #endregion

        #region Methods

        /// <summary>
        /// Function to define the list of acceptable delimiters in a MAC addres.
        /// </summary>
        protected override void SetDelimiterList()
        {
            _delimiter_list = new List<char>();
            _delimiter_list.Add(':');
            _delimiter_list.Add('-');
            _delimiter_list.Add(' ');
            _delimiter_list.Add('.');
        }

        /// <summary>
        /// Returns a value indicating whether this instance is equal to a specified <c>EthernetMACAddr</c>.
        /// </summary>
        /// <param name="obj">An <c>EthernetMACAddr</c> to compare to this instance.</param>
        /// <returns><c>true</c> if <c>obj</c> has the same value as this instance; otherwise, <c>false</c>.</returns>
        public override bool Equals(object obj)
        {
            if (obj != null && obj is EthernetMACAddr)
            {
                if (this.ToString() == obj.ToString())
                    return true;
                else
                    return false;
            }

            return false;
        }

        /// <summary>
        /// Returns the hash code for the instance.
        /// </summary>
        /// <returns>A 32-bit signed integer hash code.</returns>
        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        #endregion

        #region Static Read-Only

        /// <summary>
        /// Provides the Ethernet MAC Broadcast address. This field is read-only.
        /// </summary>
        public static readonly EthernetMACAddr Broadcast = new EthernetMACAddr("FF:FF:FF:FF:FF:FF");

        #endregion
    }

    /// <summary>
    /// Internet Protocol (IP) version 4 address
    /// </summary>
    [Serializable]
    public class IPv4Addr : ListOfBytes
    {
        #region Constructors

        /// <summary>
        /// Create a default IP version 4 address
        /// </summary>
        public IPv4Addr()
        {
            _list_length = 4;
            _number_of_bytes_per_delimiter = 1;
            _is_hex = false;
            SetDelimiterList();
            _delimiter_index = 0;
            List<byte> list = new List<byte>();
            _is_valid = TryParse("0.0.0.0", out list);
            if (_is_valid == true)
                _byte_list = list;
        }

        /// <summary>
        /// Create an IP version 4 address from the desired string.
        /// </summary>
        /// <param name="address_string">A <c>string</c> containing the desired IP version 4 address.</param>
        public IPv4Addr(string address_string)
        {
            _list_length = 4;
            _number_of_bytes_per_delimiter = 1;
            _is_hex = false;
            SetDelimiterList();
            _delimiter_index = 0;
            List<byte> list = new List<byte>();
            _is_valid = TryParse(address_string, out list);
            if (_is_valid == true)
                _byte_list = list;
        }

        /// <summary>
        /// Create an IP version 4 address from the desired bytes
        /// </summary>
        /// <param name="a1">A <c>byte</c> containing the first byte in the desired IP version 4 address.</param>
        /// <param name="a2">A <c>byte</c> containing the second byte in the desired IP version 4 address.</param>
        /// <param name="a3">A <c>byte</c> containing the third byte in the desired IP version 4 address.</param>
        /// <param name="a4">A <c>byte</c> containing the fourth byte in the desired IP version 4 address.</param>
        public IPv4Addr(byte a1, byte a2, byte a3, byte a4)
        {
            _list_length = 4;
            _number_of_bytes_per_delimiter = 1;
            _is_hex = false;
            SetDelimiterList();
            _delimiter_index = 0;
            string delim = _delimiter_list[_delimiter_index].ToString();
            string address_string = a1.ToString() + delim + a2.ToString() + delim + a3.ToString() + delim + a4.ToString();
            List<byte> list = new List<byte>();
            _is_valid = TryParse(address_string, out list);
            if (_is_valid == true)
                _byte_list = list;
        }

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        #endregion

        #region Methods

        /// <summary>
        /// Function to define the list of acceptable delimiters in an IP version 4 addres.
        /// </summary>
        protected override void SetDelimiterList()
        {
            _delimiter_list = new List<char>();
            _delimiter_list.Add('.');
            _delimiter_list.Add('-');
            _delimiter_list.Add(' ');
        }

        /// <summary>
        /// Returns a value indicating whether this instance is equal to a specified <c>IPv4Addr</c>.
        /// </summary>
        /// <param name="obj">A <c>IPv4Addr</c> to compare to this instance.</param>
        /// <returns><c>true</c> if <c>obj</c> has the same value as this instance; otherwise, <c>false</c>.</returns>
        public override bool Equals(object obj)
        {
            if (obj != null && obj is IPv4Addr)
            {
                if (this.ToString() == obj.ToString())
                    return true;
                else
                    return false;
            }

            return false;
        }

        /// <summary>
        /// Returns the hash code for the instance.
        /// </summary>
        /// <returns>A 32-bit signed integer hash code.</returns>
        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        /// <summary>
        /// Generate a multicast MAC address based on the IP address.
        /// </summary>
        /// <returns>If this instance contains a valid multicast IP address, then it returns a <c>MACAddr</c> containing a multicast MAC address based on this instance, otherwise it returns a <c>MACAddr</c> containing all zeros.</returns>
        public EthernetMACAddr MulticastMAC()
        {
            EthernetMACAddr output = new EthernetMACAddr();
            if (_is_valid == true)
            {
                byte addr0 = _byte_list[0];
                if (addr0 >= 224 && addr0 < 240)
                {
                    output.ByteList[0] = 0x01;
                    output.ByteList[1] = 0x00;
                    output.ByteList[2] = 0x5E;
                    byte temp = ByteList[1];
                    if (temp >= 0x80)
                        temp -= 0x80;
                    output.ByteList[3] = temp;
                    output.ByteList[4] = ByteList[2];
                    output.ByteList[5] = ByteList[3];
                }
            }

            return output;
        }

        #endregion

        #region Static Read-Only

        /// <summary>
        /// Provides the IP Broadcast address. This field is real-only.
        /// </summary>
        public static readonly IPv4Addr Broadcast = new IPv4Addr("255.255.255.255");

        /// <summary>
        /// Provides the IP Loopback address. This field is read-only.
        /// </summary>
        public static readonly IPv4Addr Loopback = new IPv4Addr("127.0.0.1");

        #endregion
    }
}
