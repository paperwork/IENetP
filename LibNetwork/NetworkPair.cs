﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IENetP.Generic;

namespace IENetP.Network
{
    /// <summary>
    /// Class to define a network pairing between two systems
    /// </summary>
    public class NetworkPair
    {
        #region Constructors

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        private EthernetMACAddr _mac_src = new EthernetMACAddr();
        /// <summary>
        /// Gets or sets the Ethernet MAC source address
        /// </summary>
        public EthernetMACAddr MacSource
        {
            get { return _mac_src; }
            set { _mac_src = value; }
        }

        private EthernetMACAddr _mac_dst = new EthernetMACAddr();
        /// <summary>
        /// Gets or sets the Ethernet MAC destination address
        /// </summary>
        public EthernetMACAddr MacDestination
        {
            get { return _mac_dst; }
            set { _mac_dst = value; }
        }

        private IPv4Addr _ip_src = new IPv4Addr();
        /// <summary>
        /// Gets or sets the IP source address
        /// </summary>
        public IPv4Addr IpSource
        {
            get { return _ip_src; }
            set { _ip_src = value; }
        }

        private IPv4Addr _ip_dst = new IPv4Addr();
        /// <summary>
        /// Gets or sets the IP destination address
        /// </summary>
        public IPv4Addr IpDestination
        {
            get { return _ip_dst; }
            set { _ip_dst = value; }
        }

        private int _enip_cid = 0;
        /// <summary>
        /// Gets or sets the EtherNet/IP Connection ID
        /// </summary>
        public int EtherNetIPConnID
        {
            get { return _enip_cid; }
            set { _enip_cid = value; }
        }

        private bool _use_enip = false;
        /// <summary>
        /// Does the network pair represent an EtherNet/IP Connected Class 1 connection?
        /// </summary>
        public bool UseEtherNetIP
        {
            get { return _use_enip; }
            set { _use_enip = value; }
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            string output = "";

            if (_ip_src.IsValid == true)
            {
                output = _ip_src.ToString() + " -> " + _ip_dst.ToString();
                if (_use_enip)
                    output += " | CID=0x" + _enip_cid.ToString("X");
            }
            else if (_mac_src.IsValid == true)
            {
                output = _mac_src.ToString() + " -> " + _mac_dst.ToString();
            }
            else
            {
                output = "Invalid Network Pair";
            }

            return output;
        }

        #endregion
    }
}
