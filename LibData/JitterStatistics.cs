﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;

using IENetP.Generic;
using IENetP.Network;
using IENetP.Wireshark;

namespace IENetP.Data
{
    /// <summary>
    /// Set of statistics for the dataset
    /// </summary>
    public class JitterStatistics
    {
        #region Constructors

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        private Enums.AnalysisMethod _analysis_method = Enums.AnalysisMethod.StandardDeviation;
        /// <summary>
        /// Analysis method to use when calculating the statistics
        /// </summary>
        public Enums.AnalysisMethod AnalysisMethod
        {
            get { return _analysis_method; }
            set { _analysis_method = value; }
        }

        private List<Point> _dataset = new List<Point>();
        /// <summary>
        /// Dataset used to calculate the statistics
        /// </summary>
        public List<Point> Dataset
        {
            get { return _dataset; }
            set { _dataset = value; Calculate(); }
        }

        private DataTable _set = new DataTable();
        /// <summary>
        /// Data table containing the results
        /// </summary>
        public DataTable Set
        {
            get { return _set; }
            set { _set = value; }
        }

        private double _mean = 0;
        /// <summary>
        /// Mean value of dataset
        /// </summary>
        /// <value>A <c>double</c> representing the mean value of the data set</value>
        public double Mean
        {
            get { return _mean; }
            protected set { _mean = value; }
        }

        private double _median = 0;
        /// <summary>
        /// Median value of dataset
        /// </summary>
        /// <value>A <c>double</c> representing the median value of the dataset</value>
        public double Median
        {
            get { return _median; }
            protected set { _median = value; }
        }

        private double _min = 0;
        /// <summary>
        /// Minimum value of the dataset
        /// </summary>
        /// <value>A <c>double</c> value representing the minimum value of the dataset</value>
        public double Minimum
        {
            get { return _min; }
            protected set { _min = value; }
        }

        private double _max = 0;
        /// <summary>
        /// Minimum value of the dataset
        /// </summary>
        /// <value>A <c>double</c> value representing the maximum value of the dataset</value>
        public double Maximum
        {
            get { return _max; }
            protected set { _max = value; }
        }

        private double _average_deviation = 0;
        /// <summary>
        /// Average deviation value of dataset
        /// </summary>
        /// <value>A <c>double</c> value representing the average deviation value of the dataset assuming a Gaussian normal distribution</value>
        public double AverageDeviation
        {
            get { return _average_deviation; }
            protected set { _average_deviation = value; }
        }

        private double _variance = 0;
        /// <summary>
        /// Variance value of dataset
        /// </summary>
        /// <value>A <c>double</c> value representing the variance of the dataset assuming a Gaussian normal distribution</value>
        public double Variance
        {
            get { return _variance; }
            protected set { _variance = value; }
        }

        private double _standard_deviation = 0;
        /// <summary>
        /// Standard deviation value of dataset
        /// </summary>
        /// <value>A <c>double</c> representing the standard deviation value of the dataset assuming a Gaussian normal distribution</value>
        public double StandardDeviation
        {
            get { return _standard_deviation; }
            protected set { _standard_deviation = value; }
        }

        private double _skewness = 0;
        /// <summary>
        /// Skewness or third moment of the dataset
        /// </summary>
        /// <value>A <c>double</c> value representing the skewness or third moment of the dataset.  A positive value of skewness signifies a distribution whith an asymetric tail extending out towards more positive x; a negative value signifies a distribution whose tail extends out towards more negative x.</value>
        public double Skewness
        {
            get { return _skewness; }
            protected set { _skewness = value; }
        }

        private double _kurtosis = 0;
        /// <summary>
        /// Kurtosis or forth moment of the dataset
        /// </summary>
        /// <value>A <c>double</c> value representing the kurtosis or forth moment of the dataset.  Kurtosis measures the relative peakedness or flatness of a distribution.  A distribution with positive kurtosis is termed leptokurtic, with a sharp peak.  A distribution with negative kurtosis is termed platykurtic, with a flat peak like a plateau.</value>
        public double Kurtosis
        {
            get { return _kurtosis; }
            protected set { _kurtosis = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// String representation of the test results
        /// </summary>
        /// <returns>A <c>string</c> representing the test results</returns>
        public override string ToString()
        {
            string output = "Mean=" + Mean.ToString();
            output += " | StdDev=" + StandardDeviation.ToString();

            return output;
        }

        /// <summary>
        /// Calculate the statistics
        /// </summary>
        private void Calculate()
        {
            List<double> data = new List<double>();
            for (int i = 0; i < _dataset.Count; i++)
                data.Add(_dataset[i].Y);

            CalculateMinimum(data);
            CalculateMaximum(data);

            if (_analysis_method == Enums.AnalysisMethod.StandardDeviation)
            {
                CalculateMean(data);
                CalculateMedian(data);
                CalculateMoments(data);
            }
        }

        /// <summary>
        /// Calculate the mean of the dataset
        /// </summary>
        /// <param name="set">An <c>ArrayList</c> of <c>double</c> values</param>
        private void CalculateMean(List<double> set)
        {
            double sum = 0;

            for (int i = 0; i < set.Count; i++)
                sum += (double)set[i];

            if (set.Count == 0)
                _mean = 0;
            else
                _mean = Math.Round((double)sum / set.Count, Constants.StatisticsDecimalPlaces + Constants.StatisticsDecimalOffsetExponentDefault);
        }

        /// <summary>
        /// Calculate the median of the dataset
        /// </summary>
        /// <param name="set">An <c>ArrayList</c> of <c>double</c> values</param>
        private void CalculateMedian(List<double> set)
        {
            List<double> temp = new List<double>();
            for (int i = 0; i < set.Count; i++)
                temp.Add(set[i]);
            temp.Sort();

            int index = (int)Math.Floor((double)temp.Count / 2);

            if (temp.Count == 0)
                _median = 0;
            else
                _median = Math.Round((double)temp[index], Constants.StatisticsDecimalPlaces + Constants.StatisticsDecimalOffsetExponentDefault);
        }

        /// <summary>
        /// Calculate the minimum of the dataset
        /// </summary>
        /// <param name="set">An <c>ArrayList</c> of <c>double</c> values</param>
        private void CalculateMinimum(List<double> set)
        {
            _min = double.MaxValue;

            for (int i = 0; i < set.Count; i++)
                if (set[i] < _min)
                    _min = set[i];
        }

        /// <summary>
        /// Calculate the maximum of the dataset
        /// </summary>
        /// <param name="set">An <c>ArrayList</c> of <c>double</c> values</param>
        private void CalculateMaximum(List<double> set)
        {
            _max = double.MinValue;

            for (int i = 0; i < set.Count; i++)
                if (set[i] > _max)
                    _max = set[i];
        }

        /// <summary>
        /// Calculate the moments (standard deviation, skewness, and kurtosis) of the data set
        /// </summary>
        /// <param name="set">An <c>ArrayList</c> of <c>double</c> values</param>
        /// <remarks>Basic code is from "Numerical Recipes in C"</remarks>
        private void CalculateMoments(List<double> set)
        {
            double ep, s, p;
            double adev, var, sdev, skew, kurt;

            ep = s = p = adev = var = sdev = skew = kurt = 0.0;

            if (set.Count > 1)
            {
                // Get the first (absolute), second, third, and forth moment of the deviation from the mean
                for (int i = 1; i < set.Count; i++)
                {
                    s = (double)set[i] - Mean;
                    ep += s;
                    adev += Math.Abs(s);
                    var += (p = s * s);
                    skew += (p *= s);
                    kurt += (p *= s);
                }

                adev /= set.Count;

                // Corrected two-pass formula (first is to calculate mean)
                var = (var - ep * ep / set.Count) / (set.Count - 1);

                // Put the pieces together according to the conventional defintions
                sdev = Math.Sqrt(var);
                if (var != 0.0)
                {
                    skew /= (set.Count * var * sdev);
                    kurt = kurt / (set.Count * var * var) - 3.0;
                }
            }

            _average_deviation = Math.Round(adev, Constants.StatisticsDecimalPlaces + Constants.StatisticsDecimalOffsetExponentDefault);
            _variance = Math.Round(var, Constants.StatisticsDecimalPlaces + Constants.StatisticsDecimalOffsetExponentDefault);
            _standard_deviation = Math.Round(sdev, Constants.StatisticsDecimalPlaces + Constants.StatisticsDecimalOffsetExponentDefault);
            _skewness = Math.Round(skew, Constants.StatisticsDecimalPlaces + Constants.StatisticsDecimalOffsetExponentDefault);
            _kurtosis = Math.Round(kurt, Constants.StatisticsDecimalPlaces + Constants.StatisticsDecimalOffsetExponentDefault);
        }

        #endregion
    }
}
