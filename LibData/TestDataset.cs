﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;

using IENetP.Generic;
using IENetP.Network;
using IENetP.Wireshark;

namespace IENetP.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class TestDataset
    {
        #region Constructors

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        private AnalysisSettings _settings = new AnalysisSettings();
        /// <summary>
        /// Analysis settings applied to the dataset
        /// </summary>
        public AnalysisSettings Settings
        {
            get { return _settings; }
            set { _settings = value; }
        }

        private Identification _dataset_ident = new Identification();
        /// <summary>
        /// Identification information for this particular dataset
        /// </summary>
        public Identification DatasetIdent
        {
            get { return _dataset_ident; }
            set { _dataset_ident = value; }
        }

        private List<Point> _points = new List<Point>();
        /// <summary>
        /// List of points for the dataset
        /// </summary>
        public List<Point> Points
        {
            get { return _points; }
            set { _points = value; }
        }

        private JitterStatistics _jitter_stats = new JitterStatistics();
        /// <summary>
        /// Jitter statistics
        /// </summary>
        public JitterStatistics JitterStats
        {
            get { return _jitter_stats; }
            set { _jitter_stats = value; }
        }

        private List<Point> _histogram = new List<Point>();
        /// <summary>
        /// Gets a table containing a histogram based on the results
        /// </summary>
        public List<Point> Histogram
        {
            get { return _histogram; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Build a histogram based on the dataset and jitter statistics
        /// </summary>
        public void BuildHistogram()
        {
            // Build the histogram sets
            double interval = (_jitter_stats.Maximum - _jitter_stats.Minimum) / _points.Count;
            double min_interval = Math.Pow(10, -(Constants.StatisticsDecimalPlaces + Constants.StatisticsDecimalOffsetExponentDefault));
            if (interval < min_interval)
                interval = min_interval;

            List<double> bins = new List<double>();
            List<int> numbers = new List<int>();

            int i = 0;
            while ((_jitter_stats.Minimum + interval * i) <= _jitter_stats.Maximum)
            {
                bins.Add(_jitter_stats.Minimum + interval * i++);
                numbers.Add(0);
            }
            bins.Add(_jitter_stats.Maximum + interval);
            numbers.Add(0);

            List<double> temp = new List<double>();
            for (i = 0; i < _points.Count; i++)
                temp.Add(_points[i].Y);
            temp.Sort();

            int current_bin = 0;
            for (i = 0; i < temp.Count; i++)
            {
                while (temp[i] > bins[current_bin])
                    current_bin++;
                numbers[current_bin]++;
            }

            _histogram = new List<Point>();
            for (i = 0; i < bins.Count; i++)
                _histogram.Add(new Point(bins[i], numbers[i]));
        }

        #endregion
    }
}
