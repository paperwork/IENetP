﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IENetP.Generic;
using IENetP.Network;
using IENetP.Wireshark;

namespace IENetP.Data
{
    public class AnalysisResults
    {

        #region Constructors

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        private WiresharkFileInfo _capture_file_info = new WiresharkFileInfo();
        /// <summary>
        /// Wireshark specific information about the capture file, including file type, number of packets, capture start time, etc.
        /// </summary>
        public WiresharkFileInfo CaptureFileInfo
        {
            get { return _capture_file_info; }
            set { _capture_file_info = value; }
        }

        private List<TestDataset> _datasets = new List<TestDataset>();
        /// <summary>
        /// List of test datasets generated from the capture file.
        /// </summary>
        public List<TestDataset> Datasets
        {
            get { return _datasets; }
            set { _datasets = value; }
        }

        #endregion

        #region Methods

        #endregion
    }
}
