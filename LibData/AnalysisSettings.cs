﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using IENetP.Generic;
using IENetP.Network;

namespace IENetP.Data
{
    /// <summary>
    /// Applications settings that are needed to analyze the desired capture file.
    /// </summary>
    public class AnalysisSettings
    {
        #region Constructors

        /// <summary>
        /// Initialize the analysis settings
        /// </summary>
        public AnalysisSettings()
        {
        }

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        private Identification _test_info = new Identification();
        /// <summary>
        /// Identification information used to document the test being analyzed.
        /// </summary>
        /// <value>An <c>Identification</c> class containing basic information about the test.</value>
        public Identification TestInfo
        {
            get { return _test_info; }
            set { _test_info = value; }
        }

        private DeviceIdentification _device_info = new DeviceIdentification();
        /// <summary>
        /// Identification information about the device under test.
        /// </summary>
        /// <value>A <c>DeviceIdentification</c> class containing basic information about the device under test.</value>
        public DeviceIdentification DeviceInfo
        {
            get { return _device_info; }
            set { _device_info = value; }
        }

        private IPv4Addr _device_ip = new IPv4Addr();
        /// <summary>
        /// IP address of the device under test.
        /// </summary>
        /// <value>An <c>IPv4Addr</c> class containing the IP address of the device under test.</value>
        public IPv4Addr DeviceIp
        {
            get { return _device_ip; }
            set { _device_ip = value; }
        }

        private string _capture_file_name = "";
        /// <summary>
        /// Capture file name.
        /// </summary>
        /// <value>A <c>string</c> containing the full path to the capture file.</value>
        public string CaptureFileName
        {
            get { return _capture_file_name; }
            set { _capture_file_name = value; }
        }

        private bool _wireshark_filter_override = false;
        /// <summary>
        /// Should the default Wireshark filter be overridden?
        /// </summary>
        /// <value>A <c>bool</c> indicating whether or not the Wireshark filter should be overridden.</value>
        public bool WiresharkFilterOverride
        {
            get { return _wireshark_filter_override; }
            set { _wireshark_filter_override = value; }
        }

        private string _wireshark_filter_string = "";
        /// <summary>
        /// Wireshark filter string.
        /// </summary>
        /// <value>A <c>string</c> containing the complete wireshark packet filter.</value>
        public string WiresharkFilterString
        {
            get { return _wireshark_filter_string; }
            set { _wireshark_filter_string = value; }
        }

        #endregion

        #region Methods

        #endregion
    }
}
