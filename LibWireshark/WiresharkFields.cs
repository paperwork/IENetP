﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace IENetP.Wireshark
{
    /// <summary>
    /// Base class for all Wireshark fields
    /// </summary>
    public abstract class WiresharkField
    {
        #region Constructors

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        protected string _name = "";
        /// <summary>
        /// Descriptive Name
        /// </summary>
        public abstract string Name { get; set; }

        protected string _abbreviation = "";
        /// <summary>
        /// Field Abbreviation
        /// </summary>
        public abstract string Abbreviation { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Try to parse the string to determine the field parameters
        /// </summary>
        /// <param name="s">A <c>string</c> containing a single line from the Wireshark fields output to interpret</param>
        /// <returns>True if the strin was correctly parsed, false if not</returns>
        public abstract bool TryParse(string s);

        /// <summary>
        /// Output a <c>string</c> representing the Wireshark field
        /// </summary>
        /// <returns>A <c>string</c> representing the Wireshark field</returns>
        public override string ToString()
        {
            string output = "";

            output += _abbreviation + " | " + _name;

            return output;
        }


        #endregion
    }

    public class WiresharkProtocol : WiresharkField
    {
        #region Constructors

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        public override string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public override string Abbreviation
        {
            get { return _abbreviation; }
            set { _abbreviation = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Try to parse the string to determine the field parameters
        /// </summary>
        /// <param name="s">A <c>string</c> containing a single line from the Wireshark fields output to interpret</param>
        /// <returns>True if the strin was correctly parsed, false if not</returns>
        public override bool TryParse(string s)
        {
            if (!s.Trim().StartsWith("P"))
                return false;

            s = s.Trim();
            string[] parts = s.Split('\t');
            if (parts.Count() != 3)
                return false;
                
            _name = parts[1];
            _abbreviation = parts[2];

            return true;
        }

        /// <summary>
        /// Output a <c>string</c> representing the Wireshark field
        /// </summary>
        /// <returns>A <c>string</c> representing the Wireshark field</returns>
        public override string ToString()
        {
            string output = "";

            output += _abbreviation + " | " + _name;

            return output;
        }

        #endregion
    }

    public class WiresharkHeaderField : WiresharkField
    {
        #region Constructors

        public WiresharkHeaderField()
            : base()
        {
            this._version = new Version(1, 8);
        }

        public WiresharkHeaderField(Version version): base()
        {
            this._version = version;
        }
        #endregion

        #region Data Objects
        Version _version;
        #endregion

        #region Properties

        public override string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public override string Abbreviation
        {
            get { return _abbreviation; }
            set { _abbreviation = value; }
        }

        private string _type = "";
        /// <summary>
        /// Wireshark header field type
        /// </summary>
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        private string _parent_protocol_abbreviation = "";
        /// <summary>
        /// Parent protocol abbreviation
        /// </summary>
        public string ParentProtocolAbbreviation
        {
            get { return _parent_protocol_abbreviation; }
            set { _parent_protocol_abbreviation = value; }
        }

        private string _field_description = "";
        /// <summary>
        /// Header field description
        /// </summary>
        public string FieldDescription
        {
            get { return _field_description; }
            set { _field_description = value; }
        }

        private string _base_for_display = "";
        /// <summary>
        /// Base unit used to display the header field value
        /// </summary>
        public string BaseForDisplay
        {
            get { return _base_for_display; }
            set { _base_for_display = value; }
        }

        private int _bitmask = 0;
        /// <summary>
        /// Bitmask used to display the value properly
        /// </summary>
        public int Bitmask
        {
            get { return _bitmask; }
            set { _bitmask = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Try to parse the string to determine the field parameters
        /// </summary>
        /// <param name="s">A <c>string</c> containing a single line from the Wireshark fields output to interpret</param>
        /// <returns>True if the strin was correctly parsed, false if not</returns>
        public override bool TryParse(string s)
        {
            if (!s.Trim().StartsWith("F"))
                return false;

            s = s.Trim();
            string[] parts = s.Split('\t');
            if ((_version.Major == 1 && _version.Minor >= 10) || (_version.Major >= 2))
            {
                if (parts.Count() == 5 || parts.Count() == 7 || parts.Count() == 8)
                {
                    _name = parts[1];
                    _abbreviation = parts[2];
                    _type = parts[3];
                    _parent_protocol_abbreviation = parts[4];
                    if (parts.Count() == 7)
                    {
                        _base_for_display = parts[5];
                        int result = 0;
                        if (TryParseBitmask(parts[6], out result))
                            _bitmask = result;
                        else
                            return false;
                    }
                    if (parts.Count() == 8)
                    {
                        _field_description = parts[7];
                    }
                    else
                    {
                        _field_description = "";
                    }
                }
                else
                    return false;
            }
            else
            {
                if (parts.Count() == 6 || parts.Count() == 8)
                {
                    _name = parts[1];
                    _abbreviation = parts[2];
                    _type = parts[3];
                    _parent_protocol_abbreviation = parts[4];
                    _field_description = parts[5];
                    if (parts.Count() == 8)
                    {
                        _base_for_display = parts[6];
                        int result = 0;
                        if (TryParseBitmask(parts[7], out result))
                            _bitmask = result;
                        else
                            return false;
                    }
                }
                else
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Output a <c>string</c> representing the Wireshark header field
        /// </summary>
        /// <returns>A <c>string</c> representing the Wireshark field</returns>
        public override string ToString()
        {
            string output = "";

            output += _abbreviation + " | " + _name + " | " + _type + " | " + _field_description;

            return output;
        }

        private bool TryParseBitmask(string s, out int result)
        {
            result = 0;

            s = s.Trim();
            if (s.StartsWith("0x"))
            {
                int loc = s.IndexOf('x');
                if (loc >= 0)
                    s = s.Substring(loc + 1);
            }

            return int.TryParse(s, NumberStyles.HexNumber, null, out result);
        }

        #endregion
    }

    public class WiresharkFields
    {
        #region Constructors

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        private string _wireshark_version = "";
        /// <summary>
        /// Gets or sets the version of Wireshark used to generate the current list of protocols and header fields
        /// </summary>
        public string WiresharkVersion
        {
            get { return _wireshark_version; }
            set { _wireshark_version = value; }
        }

        private List<WiresharkProtocol> _wireshark_protocols = new List<WiresharkProtocol>();
        /// <summary>
        /// Gets or sets the current list of Wireshark protocols
        /// </summary>
        public List<WiresharkProtocol> WiresharkProtocols
        {
            get { return _wireshark_protocols; }
            set { _wireshark_protocols = value; }
        }

        private List<WiresharkHeaderField> _wireshark_header_fields = new List<WiresharkHeaderField>();
        /// <summary>
        /// Gets or sets the current list of Wireshark header fields
        /// </summary>
        public List<WiresharkHeaderField> WiresharkHeaderFields
        {
            get { return _wireshark_header_fields; }
            set { _wireshark_header_fields = value; }
        }

        #endregion

        #region Methods

        #endregion
    }

}
