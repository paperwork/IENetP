﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IENetP.Generic;
using IENetP.Network;

namespace IENetP.Wireshark
{
    /// <summary>
    /// 
    /// </summary>
    public class WiresharkPSMLPacket
    {
        #region Constructors

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        private string _number = "";
        /// <summary>
        /// Gets or sets the packet number
        /// </summary>
        public string Number
        {
            get { return _number; }
            set { _number = value; }
        }

        private string _time = "";
        /// <summary>
        /// Gets or sets the packet time
        /// </summary>
        public string Time
        {
            get { return _time; }
            set { _time = value; }
        }

        private string _src = "";
        /// <summary>
        /// Gets or sets the packet source address
        /// </summary>
        public string Source
        {
            get { return _src; }
            set { _src = value; }
        }

        private string _dst = "";
        /// <summary>
        /// Gets or sets the packet destination address
        /// </summary>
        public string Destination
        {
            get { return _dst; }
            set { _dst = value; }
        }

        private string _protocol = "";
        /// <summary>
        /// Gets or sets the packet protocol
        /// </summary>
        public string Protocol
        {
            get { return _protocol; }
            set { _protocol = value; }
        }

        private string _enip_cid = "";
        /// <summary>
        /// Gets or sets the EtherNet/IP Connction ID
        /// </summary>
        public string EnipCid
        {
            get { return _enip_cid; }
            set { _enip_cid = value; }
        }

        private string _enip_seq = "";
        /// <summary>
        /// Gets or sets the EtherNet/IP Sequence Number
        /// </summary>
        public string EnipSeq
        {
            get { return _enip_seq; }
            set { _enip_seq = value; }
        }

        private string _info = "";
        /// <summary>
        /// Gets or sets the packet info
        /// </summary>
        public string Info
        {
            get { return _info; }
            set { _info = value; }
        }

        #endregion

        #region Methods

        #endregion
    }
}
