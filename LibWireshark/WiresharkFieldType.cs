﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IENetP.Generic;
using IENetP.Network;

namespace IENetP.Wireshark
{
    /// <summary>
    /// Class to relate Wireshark field types to IENetP variable types
    /// </summary>
    public class WiresharkFieldType
    {
        #region Constructors

        /// <summary>
        /// Define a default Wireshark field type
        /// </summary>
        public WiresharkFieldType()
        {
        }

        /// <summary>
        /// Define a Wireshark field type with the desired parameters
        /// </summary>
        /// <param name="name">Human-readable name for the field type</param>
        /// <param name="wireshark_type"><c>String</c> value representing the wireshark ftenum value</param>
        /// <param name="system_type"><c>Type</c> value representing the correct variable type</param>
        /// <param name="is_valid_timestamp">Can the field type represent a valid timestamp value?</param>
        /// <param name="is_valid_address">Can the field type represent a valid address value?</param>
        public WiresharkFieldType(string name, string wireshark_type, Type system_type, bool is_valid_timestamp, bool is_valid_address)
        {
            _name = name;
            _wireshark_type = wireshark_type;
            _system_type = system_type;
            _is_valid_timestamp = is_valid_timestamp;
            _is_valid_address = is_valid_address;
        }

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        private string _name = "";
        /// <summary>
        /// Human-readable name describing the field type
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _wireshark_type = "";
        /// <summary>
        /// <c>String</c> value representing the wireshark ftenum value
        /// </summary>
        public string WiresharkType
        {
            get { return _wireshark_type; }
            set { _wireshark_type = value; }
        }
        
        private Type _system_type = null;
        /// <summary>
        /// <c>Type</c> value representing the correct variable type
        /// </summary>
        public Type SystemType
        {
            get { return _system_type; }
            set { _system_type = value; }
        }

        private bool _is_valid_timestamp = false;
        /// <summary>
        /// Can the field type represent a valid timestamp value?
        /// </summary>
        /// <value><c>True</c> if the field type can represent a valid timestamp value, <c>False</c> if not.</value>
        public bool IsValidTimestamp
        {
            get { return _is_valid_timestamp; }
            protected set { _is_valid_timestamp = value; }
        }

        private bool _is_valid_address = false;
        /// <summary>
        /// Can the field type represent a valid address value?
        /// </summary>
        /// <value><c>True</c> if the field type can represent a valid address value, <c>False</c> if not.</value>
        public bool IsValidAddress
        {
            get { return _is_valid_address; }
            protected set { _is_valid_address = value; }
        }

        #endregion

        #region Methods

        #endregion

        #region Static Read-Only

        /// <summary>
        /// Label
        /// </summary>
        /// <remarks>Used for text labels with no value</remarks>
        public static readonly WiresharkFieldType None = new WiresharkFieldType("Label", "FT_NONE", typeof(string), false, false);

        /// <summary>
        /// Protocol
        /// </summary>
        public static readonly WiresharkFieldType Protocol = new WiresharkFieldType("Protocol", "FT_PROTOCOL", typeof(string), false, false);

        /// <summary>
        /// Boolean
        /// </summary>
        /// <remarks>TRUE or FALSE</remarks>
        public static readonly WiresharkFieldType Boolean = new WiresharkFieldType("Boolean", "FT_BOOLEAN", typeof(bool), false, false);

        /// <summary>
        /// Unsigned 8-bit integer
        /// </summary>
        public static readonly WiresharkFieldType UInt8 = new WiresharkFieldType("UInt8", "FT_UINT8", typeof(Byte), false, true);

        /// <summary>
        /// Unsigned 16-bit integer
        /// </summary>
        public static readonly WiresharkFieldType UInt16 = new WiresharkFieldType("UInt16", "FT_UINT16", typeof(UInt16), false, true);

        /// <summary>
        /// Unsigned 24-bit integer
        /// </summary>
        /// <remarks>Really a UInt32, but displayed as 3 hex-digits</remarks>
        public static readonly WiresharkFieldType UInt24 = new WiresharkFieldType("UInt24", "FT_UINT24", typeof(UInt32), false, true);

        /// <summary>
        /// Unsigned 32-bt integer
        /// </summary>
        public static readonly WiresharkFieldType UInt32 = new WiresharkFieldType("UInt32", "FT_UINT32", typeof(UInt32), true, true);

        /// <summary>
        /// Unsigned 64-bit integer
        /// </summary>
        public static readonly WiresharkFieldType UInt64 = new WiresharkFieldType("UInt64", "FT_UINT64", typeof(UInt64), true, true);

        /// <summary>
        /// Signed 8-bit integer
        /// </summary>
        public static readonly WiresharkFieldType Int8 = new WiresharkFieldType("Int8", "FT_INT8", typeof(SByte), false, true);

        /// <summary>
        /// Signed 16-bit integer
        /// </summary>
        public static readonly WiresharkFieldType Int16 = new WiresharkFieldType("Int16", "FT_INT16", typeof(Int16), false, true);

        /// <summary>
        /// Signed 24-bit integer
        /// </summary>
        /// <remarks>Really a Int32, but displayed as 3 hex-digits</remarks>
        public static readonly WiresharkFieldType Int24 = new WiresharkFieldType("Int24", "FT_INT24", typeof(Int32), false, true);

        /// <summary>
        /// Signed 32-bit integer
        /// </summary>
        public static readonly WiresharkFieldType Int32 = new WiresharkFieldType("Int32", "FT_INT32", typeof(Int32), true, true);

        /// <summary>
        /// Signed 64-bit integer
        /// </summary>
        public static readonly WiresharkFieldType Int64 = new WiresharkFieldType("Int64", "FT_INT64", typeof(Int64), true, true);

        /// <summary>
        /// Floating point (single-precision)
        /// </summary>
        public static readonly WiresharkFieldType Float = new WiresharkFieldType("Float", "FT_FLOAT", typeof(float), true, false);

        /// <summary>
        /// Floating point (double-precision)
        /// </summary>
        public static readonly WiresharkFieldType Double = new WiresharkFieldType("Double", "FT_DOUBLE", typeof(double), true, false);

        /// <summary>
        /// Date/Time
        /// </summary>
        public static readonly WiresharkFieldType AbsoluteTime = new WiresharkFieldType("Date/Time", "FT_ABSOLUTE_TIME", typeof(DateTime), true, false);

        /// <summary>
        /// Time offset
        /// </summary>
        public static readonly WiresharkFieldType RelativeTime = new WiresharkFieldType("Time Offset", "FT_RELATIVE_TIME", typeof(TimeSpan), true, false);

        /// <summary>
        /// Character string
        /// </summary>
        public static readonly WiresharkFieldType String = new WiresharkFieldType("String", "FT_STRING", typeof(string), false, false);

        /// <summary>
        /// Character string
        /// </summary>
        /// <remarks>For use with proto_tree_add_item()</remarks>
        public static readonly WiresharkFieldType StringZ = new WiresharkFieldType("StringZ", "FT_STRINGZ", typeof(string), false, false);

        /// <summary>
        /// EBCDIC character string
        /// </summary>
        /// <remarks>For use with proto_tree_add_item()</remarks>
        public static readonly WiresharkFieldType EBCDIC = new WiresharkFieldType("EBCDIC String", "FT_EBCDIC", typeof(string), false, false);

        /// <summary>
        /// Character string
        /// </summary>
        /// <remarks>For use with proto_tree_add_item()</remarks>
        public static readonly WiresharkFieldType UIntString = new WiresharkFieldType("UInt String", "FT_UINT_STRING", typeof(string), false, false);

        /// <summary>
        /// Ethernet or other MAC address 
        /// </summary>
        public static readonly WiresharkFieldType Ether = new WiresharkFieldType("Ethernet MAC", "FT_ETHER", typeof(EthernetMACAddr), false, true);

        /// <summary>
        /// Sequence of bytes
        /// </summary>
        public static readonly WiresharkFieldType Bytes = new WiresharkFieldType("List of Bytes", "FT_BYTES", typeof(ListOfBytes), true, true);

        /// <summary>
        /// Sequence of bytes
        /// </summary>
        public static readonly WiresharkFieldType UIntBytes = new WiresharkFieldType("List of UInt Bytes", "FT_UINT_BYTES", typeof(ListOfBytes), true, true);

        /// <summary>
        /// Internet Protocol version 4 address
        /// </summary>
        public static readonly WiresharkFieldType IPv4 = new WiresharkFieldType("IPv4", "FT_IPv4", typeof(IPv4Addr), false, true);

        /// <summary>
        /// Internet Protocol version 6 address
        /// </summary>
        public static readonly WiresharkFieldType IPv6 = new WiresharkFieldType("IPv6", "FT_IPv6", typeof(NetworkAddr), false, true);

        /// <summary>
        /// IPX network address
        /// </summary>
        public static readonly WiresharkFieldType IPXNet = new WiresharkFieldType("IPX Address", "FT_IPXNET", typeof(ListOfBytes), false, true);

        /// <summary>
        /// A UInt32, but if selected lets you go to frame with that number
        /// </summary>
        public static readonly WiresharkFieldType FrameNum = new WiresharkFieldType("Frame Num", "FT_FRAMENUM", typeof(UInt32), false, false);

        /// <summary>
        /// Compiled Perl-Compatible Regular Expression object
        /// </summary>
        public static readonly WiresharkFieldType PCRE = new WiresharkFieldType("Perl CRE", "FT_PCRE", typeof(string), false, false);

        /// <summary>
        /// Globally Unique Identifier
        /// </summary>
        public static readonly WiresharkFieldType GUID = new WiresharkFieldType("GUID", "FT_GUID", typeof(Guid), false, true);

        /// <summary>
        /// Object Identifier
        /// </summary>
        public static readonly WiresharkFieldType OID = new WiresharkFieldType("OID", "FT_OID", typeof(string), false, true);

        /// <summary>
        /// Last item number plus one
        /// </summary>
        public static readonly WiresharkFieldType NumTypes = new WiresharkFieldType("Num of Types", "FT_NUM_TYPES", typeof(int), false, false);

        #endregion
    }
}
