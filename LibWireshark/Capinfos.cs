using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using Microsoft.Win32;

using IENetP.Generic;

namespace IENetP.Wireshark
{
	/// <summary>
	/// Wireshark Capinfos (capture information) application interaction
	/// </summary>
	public class Capinfos
	{

		#region Constructors

		#endregion

		#region Data Objects

		private static string _application_name = "capinfos.exe";
        private static string _application_verb = "Capinfos";
		private FileInfo _application_file;
		private DirectoryInfo _application_path;
		private ProcessTask _capinfos_task = new ProcessTask();

		#endregion

		#region Properties

		private bool? _is_valid = null;
		/// <summary>
		/// Does the application file exist?
		/// </summary>
		public bool? IsValid
		{
			get { return _is_valid; }
			protected set { _is_valid = value; }
		}

		private Version _version_number = new Version();
		/// <summary>
		/// Gets or sets the version of the application
		/// </summary>
		public Version VersionNumber
		{
			get { return _version_number; }
			protected set { _version_number = value; }
		}
		/// <summary>
		/// Gets the version of the application represented as a string
		/// </summary>
		public string VersionString
		{
			get { return _version_number.ToString(); }
		}

		private WiresharkFileInfo _capture_file_info = new WiresharkFileInfo();
		/// <summary>
		/// Wireshark capture file information
		/// </summary>
		public WiresharkFileInfo CaptureFileInfo
		{
			get { return _capture_file_info; }
			protected set { _capture_file_info = value; }
		}

		private bool _is_valid_capture_file = false;
		/// <summary>
		/// Is the file a valid Wireshark capture file?
		/// </summary>
		public bool IsValidCaptureFile
		{
			get { return _is_valid_capture_file; }
			protected set { _is_valid_capture_file = value; }
		}

        private Enums.ThreadStatusState _status = Enums.ThreadStatusState.Unstarted;
        /// <summary>
        /// Gets or sets the status of the task
        /// </summary>
        public Enums.ThreadStatusState Status
        {
            get { return _status; }
            protected set { _status = value; }
        }

        private string _error_message = "";
        /// <summary>
        /// Gets or sets an error message
        /// </summary>
        public string ErrorMessage
        {
            get { return _error_message; }
            protected set { _error_message = value; }
        }

        private List<string> _process_output_lines = new List<string>();
        /// <summary>
        /// Gets or sets the output lines from the application process
        /// </summary>
        public List<string> ProcessOutputLines
        {
            get { return _process_output_lines; }
            protected set { _process_output_lines = value; }
        }

        private List<string> _process_error_lines = new List<string>();
        /// <summary>
        /// Gets or sets the error lines from the application process
        /// </summary>
        public List<string> ProcessErrorLines
        {
            get { return _process_error_lines; }
            protected set { _process_error_lines = value; }
        }

        private ProgressData _progress = new ProgressData();
        /// <summary>
        /// Progress data for the process
        /// </summary>
        public ProgressData Progress
        {
            get { return _progress; }
            set { _progress = value; }
        }

        #endregion

		#region Methods

		/// <summary>
		/// Gets the application path
		/// </summary>
		/// <returns>True if the application path exists; false if it does not exist</returns>
		private bool GetApplicationPath()
		{
			bool exists = false;
			RegistryKey rk = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\wireshark.exe");

			if (rk.ValueCount != 0)
			{
				string path = (string)rk.GetValue("Path");
				_application_path = new DirectoryInfo(path);
				exists = true;
			}

			return exists;
		}

		/// <summary>
		/// Check to see if the application is on the computer?
		/// </summary>
		public void ValidateApplication()
		{
            _status = Enums.ThreadStatusState.InProgress;
            _is_valid = false;

            if (GetApplicationPath())
            {
                _application_file = new FileInfo(_application_path.FullName + "\\" + _application_name);
                if (_application_file.Exists)
                {
                    StartProcess("-h");
                    if (_process_output_lines.Count > 0)
                    {
                        ParseVersionString(_process_output_lines[0]);
                        if (_version_number.Major > 0)
                            _is_valid = true;
                        _progress.Current = _progress.Maximum;
                        _status = Enums.ThreadStatusState.Completed;
                    }
                }
                else
                {
                    ErrorMessage = "Application file does not exist!";
                    _status = Enums.ThreadStatusState.Error;
                }
            }
            else
            {
                ErrorMessage = "Application path does not exist!";
                _status = Enums.ThreadStatusState.Error;
            }
		}

		/// <summary>
		/// Parse the output of the application to determine its version
		/// </summary>
		/// <param name="first_line">First line of the process output stream that contains the version number of the application</param>
		private void ParseVersionString(string first_line)
		{
			string[] words = first_line.Split(' ');
			if (words.Length >= 2)
			{
                if (words[0] == "Capinfos" && words[1] == "(Wireshark)")
                {
                    // wireshark 2
                    _version_number = new Version(words[2]);
                }
                else
                {
                    _version_number = new Version(words[1]);
                }
			}
		}

		/// <summary>
		/// Starts the application with a particular set of arguments
		/// </summary>
		/// <param name="arguments">A <c>string</c> containing the arguments to use when running the application</param>
		private void StartProcess(string arguments)
		{
			_capinfos_task.Application = new FileInfo(_application_file.FullName);
            _capinfos_task.ApplicationVerb = _application_verb;
			_capinfos_task.Arguments = arguments;
			_capinfos_task.StartTask();

            while (_capinfos_task.Status != Enums.ThreadStatusState.Completed)
			{
				Thread.Sleep(Constants.ThreadSleepTime);
			}

			_process_output_lines = _capinfos_task.ProcessOutputLines;
			_process_error_lines = _capinfos_task.ProcessErrorLines;
		}

		/// <summary>
		/// Retrieve the capture file information for the specified file
		/// </summary>
		/// <param name="filename">A <c>String</c> containing the fully qualified name of the capture file</param>
		public void GetCaptureFileInformation(string filename)
		{
            _status = Enums.ThreadStatusState.InProgress;
			_is_valid_capture_file = false;

            _progress.Current = _progress.Minimum;
			StartProcess("\"" + filename + "\"");
            if (_process_output_lines.Count > 0)
            {
                _is_valid_capture_file = true;
                if (ParseCapinfosOutput())
                {
                    _status = Enums.ThreadStatusState.Completed;
                }
                else
                {
                    _error_message = "Error processing capinfos output.";
                    _status = Enums.ThreadStatusState.Error;
                }
                _progress.Current = _progress.Maximum;
            }
            else
            {
                _error_message = "";
                for (int i = 0; i < _process_error_lines.Count; i++)
                    _error_message += _process_error_lines[i];
                _status = Enums.ThreadStatusState.Error;
            }
		}

        /// <summary>
        /// Process the output of capinfos for the desired file.
        /// </summary>
        /// <returns>A <c>bool</c> representing whether the parse function interpreted the text correctly.</returns>
        private bool ParseCapinfosOutput()
        {
            bool ret = true;

            for (int i = 0; i < _process_output_lines.Count; i++)
            {
                string line = _process_output_lines[i];
                string[] line_part = line.Split(':');
                string[] part;

                string line_key = line_part[0].Trim();
                string line_value = "";
                if (line_part.Length > 2)
                    line_value = string.Join(":", line_part, 1, line_part.Length - 1).Trim();
                else if (line_part.Length == 2)
                    line_value = line_part[1].Trim();

                int temp_int;
                double temp_double;
                DateTime temp_dt;
                switch (line_key)
                {
                    case "File name":
                        _capture_file_info.CaptureFile = new FileInfo(line_value);
                        break;
                    case "File type":
                        _capture_file_info.FileType = line_value;
                        break;
                    case "File encapsulation":
                        _capture_file_info.FileEncapsulation = line_value;
                        break;
                    case "Number of packets":
                        int.TryParse(line_value, out temp_int);
                        _capture_file_info.FilePackets = temp_int;
                        break;
                    case "File size":
                        part = line_value.Split(' ');
                        int.TryParse(part[0], out temp_int);
                        _capture_file_info.FileSize = temp_int;
                        break;
                    case "Data size":
                        part = line_value.Split(' ');
                        int.TryParse(part[0], out temp_int);
                        _capture_file_info.FileDataSize = temp_int;
                        break;
                    case "Capture duration":
                        part = line_value.Split(' ');
                        double.TryParse(part[0], out temp_double);
                        _capture_file_info.FileDuration = temp_double;
                        break;
                    case "Start time":
                        ret = DateTime.TryParseExact(line_value, "ddd MMM dd HH:mm:ss yyyy", null, System.Globalization.DateTimeStyles.AllowWhiteSpaces, out temp_dt);
                        _capture_file_info.FileStart = new DateTime(temp_dt.Ticks);
                        break;
                    case "End time":
                        ret = DateTime.TryParseExact(line_value, "ddd MMM dd HH:mm:ss yyyy", null, System.Globalization.DateTimeStyles.AllowWhiteSpaces, out temp_dt);
                        _capture_file_info.FileEnd = new DateTime(temp_dt.Ticks);
                        break;
                    case "Data rate":
                        part = line_value.Split(' ');
                        if (part.Length == 2)
                        {
                            if (part[1] == "bytes/s")
                            {
                                double.TryParse(part[0], out temp_double);
                                _capture_file_info.FilePacketRateInBytes = temp_double;
                            }
                            else if (part[1] == "bits/s")
                            {
                                double.TryParse(part[0], out temp_double);
                                _capture_file_info.FilePacketRateInBits = temp_double;
                            }
                        }
                        else
                            _capture_file_info.FilePacketRateInBytes = CaptureFileInfo.FilePacketRateInBits = 0;
                        break;
                    case "Average packet size":
                        part = line_value.Split(' ');
                        double.TryParse(part[0], out temp_double);
                        _capture_file_info.FilePacketSize = temp_double;
                        break;
                    default:
                        break;
                }
            }

            return ret;
        }

		#endregion

	}
}
