using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

using IENetP.Generic;

namespace IENetP.Wireshark
{
	/// <summary>
	/// Wireshark process task thread
	/// </summary>
	public class ProcessTask : ThreadWrapper
	{
		#region Constructors

		#endregion

		#region Data Objects

		#endregion

		#region Properties

        private FileInfo _application;
		/// <summary>
		/// Gets or sets the process task application file
		/// </summary>
		public FileInfo Application
		{
			get { return _application; }
			set { _application = new FileInfo(value.FullName); }
		}

        private static string _application_verb;
        /// <summary>
        /// Gets or sets the application verb for the process task
        /// </summary>
        public string ApplicationVerb
        {
            get { return _application_verb; }
            set { _application_verb = value; }
        }

		private string _arguments;
		/// <summary>
		/// Gets or sets the process task application arguments
		/// </summary>
		public string Arguments
		{
			get { return _arguments; }
			set { _arguments = value; }
		}

		private static List<string> _process_output_lines;
		/// <summary>
		/// Gets or sets the process output lines of text from the proces task
		/// </summary>
		public List<string> ProcessOutputLines
		{
			get { return _process_output_lines; }
			protected set { _process_output_lines = value; }
		}

		private static List<string> _process_error_lines;
		/// <summary>
		/// Process error lines of text from the process task
		/// </summary>
		public List<string> ProcessErrorLines
		{
			get { return _process_error_lines; }
			protected set { _process_error_lines = value; }
		}

		#endregion

		#region Methods

		/// <summary>
		/// Task thread worker function
		/// </summary>
		protected override void DoTask()
		{
			_progress = 0;
			ProcessStartInfo proc_start_info = new ProcessStartInfo();
			Process proc = new Process();

            Status = Enums.ThreadStatusState.InProgress;

			proc_start_info.FileName = _application.FullName;
			proc_start_info.Arguments = _arguments;
			proc_start_info.CreateNoWindow = true;
			proc_start_info.UseShellExecute = false;
			proc_start_info.RedirectStandardOutput = true;
			proc_start_info.RedirectStandardError = true;
			proc_start_info.Verb = _application_verb;

            proc.OutputDataReceived += new DataReceivedEventHandler(ProcessTaskOutputHandler);
            proc.ErrorDataReceived += new DataReceivedEventHandler(ProcessTaskErrorHandler);
            proc.StartInfo = proc_start_info;
			proc.Start();

			_progress = 90;
			_process_output_lines = new List<string>();
			_process_error_lines = new List<string>();
            proc.BeginOutputReadLine();
            proc.BeginErrorReadLine();
            proc.WaitForExit();
		}

		/// <summary>
		/// Task thread completion operations
		/// </summary>
		protected override void OnCompleted()
		{
            Status = Enums.ThreadStatusState.Completed;
			_progress = 100;
		}

		#endregion

        #region Event Handlers

        private static void ProcessTaskOutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            if (!string.IsNullOrEmpty(outLine.Data))
            {
                _process_output_lines.Add(outLine.Data);
            }
        }

        private static void ProcessTaskErrorHandler(object sendingProcess, DataReceivedEventArgs errLine)
        {
            if (!string.IsNullOrEmpty(errLine.Data))
            {
                _process_error_lines.Add(errLine.Data);
            }
        }

        #endregion

    }
}
