﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IENetP.Generic;
using IENetP.Network;

namespace IENetP.Wireshark
{
    /// <summary>
    /// Profile to describe a protocol based on the Wireshark analysis metrics and filters
    /// </summary>
    public class ProtocolProfile
    {
        #region Constructors
        
        /// <summary>
        /// Default protocol profile
        /// </summary>
        public ProtocolProfile()
        {
        }

        /// <summary>
        /// Protocol profile with the desired parameters
        /// </summary>
        /// <param name="ident">An <c>Identifiation</c> class identifying the protocol profile.</param>
        /// <param name="override_packet_time">A <c>bool</c> indicating whether the packet time should be overridden.</param>
        /// <param name="new_packet_time">A <c>WiresharkHeaderField</c> defining the new packet time Wireshark header field.</param>
        /// <param name="new_packet_time_unit"></param>
        /// <param name="override_src_address"></param>
        /// <param name="new_src_address"></param>
        /// <param name="override_dst_address"></param>
        /// <param name="new_dst_address"></param>
        /// <param name="protocol_metrics"></param>
        /// <param name="protocol_filter"></param>
        public ProtocolProfile(Identification ident, bool override_packet_time, WiresharkHeaderField new_packet_time, TimeUnit new_packet_time_unit, 
            bool override_src_address, WiresharkHeaderField new_src_address, bool override_dst_address, WiresharkHeaderField new_dst_address,
            List<WiresharkHeaderField> protocol_metrics, string protocol_filter)
        {
            _ident = ident;
            _override_packet_time = override_packet_time;
            _new_packet_time = new_packet_time;
            _new_packet_time_unit = new_packet_time_unit;
            _override_src_address = override_src_address;
            _new_src_address = new_src_address;
            _override_dst_address = override_dst_address;
            _new_dst_address = new_dst_address;
            _protocol_metrics = protocol_metrics;
            _protocol_filter = protocol_filter;
        }

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        private Identification _ident = new Identification();
        /// <summary>
        /// Identification information for the protocol profile
        /// </summary>
        public Identification Ident
        {
            get { return _ident; }
            set { _ident = value; }
        }

        private bool _override_packet_time = false;
        /// <summary>
        /// Gets or sets a value indicating whether the packet time core analysis metric should be 
        /// overridden with the desired Wireshark header field
        /// </summary>
        public bool OverridePacketTime
        {
            get { return _override_packet_time; }
            set { _override_packet_time = value; }
        }

        private WiresharkHeaderField _new_packet_time = new WiresharkHeaderField();
        /// <summary>
        /// Gets or sets the new packet time core analysis metric Wireshark header field
        /// </summary>
        public WiresharkHeaderField NewPacketTime
        {
            get { return _new_packet_time; }
            set { _new_packet_time = value; }
        }

        private TimeUnit _new_packet_time_unit = new TimeUnit();
        /// <summary>
        /// Gets or sets the new packet time core analysis metric time unit
        /// </summary>
        public TimeUnit NewPacketTimeUnit
        {
            get { return _new_packet_time_unit; }
            set { _new_packet_time_unit = value; }
        }

        private bool _override_src_address = false;
        /// <summary>
        /// Gets or sets a value indicating whether the source address core analysis metric should be 
        /// overridden with the desired Wireshark header field
        /// </summary>
        public bool OverrideSourceAddress
        {
            get { return _override_src_address; }
            set { _override_src_address = value; }
        }

        private WiresharkHeaderField _new_src_address = new WiresharkHeaderField();
        /// <summary>
        /// Gets or sets the new source address core analysis metric Wireshark header field
        /// </summary>
        public WiresharkHeaderField NewSourceAddress
        {
            get { return _new_src_address; }
            set { _new_src_address = value; }
        }

        private bool _override_dst_address = false;
        /// <summary>
        /// Gets or sets a value indicating whether the destination address core analysis metric should be 
        /// overridden with the desired Wireshark header field
        /// </summary>
        public bool OverrideDestinationAddress
        {
            get { return _override_dst_address; }
            set { _override_dst_address = value; }
        }

        private WiresharkHeaderField _new_dst_address = new WiresharkHeaderField();
        /// <summary>
        /// Gets or sets the new destination address core analysis metric Wireshark header field
        /// </summary>
        public WiresharkHeaderField NewDestinationAddress
        {
            get { return _new_dst_address; }
            set { _new_dst_address = value; }
        }

        private List<WiresharkHeaderField> _protocol_metrics = new List<WiresharkHeaderField>();
        /// <summary>
        /// Gets or sets the list of additional protocol-specific analysis metrics
        /// </summary>
        public List<WiresharkHeaderField> ProtocolMetrics
        {
            get { return _protocol_metrics; }
            set { _protocol_metrics = value; }
        }

        private string _protocol_filter = "";
        /// <summary>
        /// Gets or sets a string containing a protocol-specific Wireshark filter
        /// </summary>
        /// <remarks>This value is not checked for syntax.</remarks>
        public string ProtocolFilter
        {
            get { return _protocol_filter; }
            set { _protocol_filter = value; }
        }

        #endregion

        #region Methods

        #endregion
        
        #region Event Handlers
        
        #endregion
    }
}
