using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using Microsoft.Win32;

using IENetP.Generic;
using IENetP.Network;

namespace IENetP.Wireshark
{
	/// <summary>
	/// Wireshark TShark (text-based interface) application interaction
	/// </summary>
	public class TShark
	{

		#region Constructors

		#endregion

		#region Data Objects

		private static string _application_name = "tshark.exe";
        private static string _application_verb = "TShark";
		private FileInfo _application_file;
		private DirectoryInfo _application_path;
        private ProcessTask _tshark_task = new ProcessTask();

		#endregion

		#region Properties

		private bool? _is_valid = null;
		/// <summary>
		/// Does the application file exist?
		/// </summary>
		public bool? IsValid
		{
			get { return _is_valid; }
			protected set { _is_valid = value; }
		}

		private Version _version_number = new Version();
		/// <summary>
		/// Gets or sets the version of the application
		/// </summary>
		public Version VersionNumber
		{
			get { return _version_number; }
			protected set { _version_number = value; }
		}
		/// <summary>
		/// Gets the version of the application represented as a string
		/// </summary>
		public string VersionString
		{
			get { return _version_number.ToString(); }
		}

        private WiresharkFileInfo _capture_file_info = new WiresharkFileInfo();
        /// <summary>
        /// Wireshark capture file information
        /// </summary>
        public WiresharkFileInfo CaptureFileInfo
        {
            get { return _capture_file_info; }
            set { _capture_file_info = value; }
        }

        private Enums.ThreadStatusState _status = Enums.ThreadStatusState.Unstarted;
        /// <summary>
        /// Gets or sets the status of the task
        /// </summary>
        public Enums.ThreadStatusState Status
        {
            get { return _status; }
            protected set { _status = value; }
        }

        private string _error_message = "";
        /// <summary>
        /// Gets or sets an error message
        /// </summary>
        public string ErrorMessage
        {
            get { return _error_message; }
            protected set { _error_message = value; }
        }

        private List<string> _process_output_lines;
		/// <summary>
		/// Gets or sets the output string lines from the application process
		/// </summary>
		public List<string> ProcessOutputLines
		{
			get { return _process_output_lines; }
			protected set { _process_output_lines = value; }
		}

		private List<string> _process_error_lines;
		/// <summary>
		/// Gets or sets the error string lines from the application process
		/// </summary>
		public List<string> ProcessErrorLines
		{
			get { return _process_error_lines; }
			protected set { _process_error_lines = value; }
		}

        private ProgressData _progress = new ProgressData();
        /// <summary>
        /// Progress data for the process
        /// </summary>
        public ProgressData Progress
        {
            get { return _progress; }
            set { _progress = value; }
        }

        private Enums.AnalysisStatus _process_status;
		/// <summary>
		/// Gets or sets the status of the TShark process
		/// </summary>
		public Enums.AnalysisStatus ProcessStatus
		{
			get {return _process_status; }
			protected set { _process_status = value; }
		}

        private string _wireshark_filter = "";
        /// <summary>
        /// Gets or sets the wireshark filter to apply when analyzing the capture file
        /// </summary>
        public string WiresharkFilter
        {
            get { return _wireshark_filter; }
            set { _wireshark_filter = value; }
        }

        private bool _redirect_to_temporary_file = false;
        /// <summary>
        /// Gets or sets a value determining whether the output shoudl be redirected to the desired temporary file.
        /// </summary>
        /// <value>
        /// <c>true</c> means the process output should be redirected to the temporary file and to <c>ProcessOutputLines</c>.
        /// <c>false</c> means that the output should only be redirected to <c>ProcessOutputLines</c>.
        /// </value>
        public bool RedirectToTemporaryFile
        {
            get { return _redirect_to_temporary_file; }
            set { _redirect_to_temporary_file = value; }
        }

        private FileInfo _temporary_file;
        /// <summary>
        /// Gets or sets the temporary file used to store the intermediate data
        /// </summary>
        public FileInfo TemporaryFile
        {
            get { return _temporary_file; }
            set { _temporary_file = new FileInfo(value.FullName); }
        }

        #endregion

		#region Methods

		/// <summary>
		/// Gets the application path
		/// </summary>
		/// <returns>True if the application path exists; false if it does not exist</returns>
		private bool GetApplicationPath()
		{
			bool exists = false;
			RegistryKey rk = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\wireshark.exe");

			if (rk.ValueCount != 0)
			{
				string path = (string)rk.GetValue("Path");
				_application_path = new DirectoryInfo(path);
				exists = true;
			}

			return exists;
		}

		/// <summary>
		/// Check to see if the application is on the computer?
		/// </summary>
		public void ValidateApplication()
		{
            _status = Enums.ThreadStatusState.InProgress;
			_is_valid = false;

            if (GetApplicationPath())
            {
                _application_file = new FileInfo(_application_path.FullName + "\\" + _application_name);
                if (_application_file.Exists)
                {
                    StartProcess("-h");
                    if (_process_output_lines.Count > 0)
                    {
                        ParseVersionString(_process_output_lines[0]);
                        if (_version_number.Major > 0)
                            _is_valid = true;
                        _progress.Current = _progress.Maximum;
                        _status = Enums.ThreadStatusState.Completed;
                    }
                }
                else
                {
                    ErrorMessage = "Application file does not exist!";
                    _status = Enums.ThreadStatusState.Error;
                }
            }
            else
            {
                ErrorMessage = "Application path does not exist!";
                _status = Enums.ThreadStatusState.Error;
            }
		}

		/// <summary>
		/// Parse the output of the application to determine its version
		/// </summary>
		/// <param name="first_line">First line of the process output stream that contains the version number of the application</param>
		private void ParseVersionString(string first_line)
		{
			string[] words = first_line.Split(' ');
			if (words.Length >= 2)
			{
                if (words[0] == "TShark" && words[1] == "(Wireshark)")
                {
                    // wireshark 2
                    _version_number = new Version(words[2]);
                }
                else
                {
                    _version_number = new Version(words[1]);
                }
            }
		}

        /// <summary>
        /// Build the wireshark fields file containing the list of protocols and header fields
        /// </summary>
        /// <returns>A <c>TSharkProcessStatus</c> indicating the status of the TShark process</returns>
        public Enums.AnalysisStatus BuildWiresharkFields()
        {
            string arguments = BuildWiresharkFieldsFlag();

            StartProcess(arguments);

            if (_process_output_lines.Count > 0)
                _process_status = Enums.AnalysisStatus.Success;
            else
                if (_process_error_lines.Count > 0)
                    _process_status = Enums.AnalysisStatus.Error;
                else
                    _process_status = Enums.AnalysisStatus.NoPacketsCaptured;

            if (_redirect_to_temporary_file && _process_status == Enums.AnalysisStatus.Success)
            {
                FileStream fs = new FileStream(_temporary_file.FullName, FileMode.Create, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);
                for (int i = 0; i < _process_output_lines.Count; i++)
                    sw.WriteLine(_process_output_lines[i]);
                sw.Close();
                fs.Close();
            }

            return _process_status;
        }

		/// <summary>
		/// Starts the application with a particular set of arguments
		/// </summary>
		/// <param name="arguments">A <c>string</c> containing the arguments to use when running the application</param>
		private void StartProcess(string arguments)
		{
			_tshark_task.Application = new FileInfo(_application_file.FullName);
            _tshark_task.ApplicationVerb = _application_verb;
			_tshark_task.Arguments = arguments;
			_tshark_task.StartTask();

            while (_tshark_task.Status != Enums.ThreadStatusState.Completed)
			{
				Thread.Sleep(Constants.ThreadSleepTime);
			}

			_process_output_lines = _tshark_task.ProcessOutputLines;
			_process_error_lines = _tshark_task.ProcessErrorLines;
		}

		/// <summary>
		/// Process a capture file looking for packets from a particular device
		/// </summary>
		/// <returns>A <c>TSharkProcessStatus</c> indicating the status of the TShark process</returns>
        public Enums.AnalysisStatus ProcessCaptureFile()
		{
            string arguments = BuildWiresharkInfile();
            arguments += BuildWiresharkFilters();
            arguments += BuildPSMLFlag();
            arguments += BuildNoNameResolutionFlag();
            arguments += BuildEnipFieldsFlag();

			StartProcess(arguments);

            if (_process_output_lines.Count > 0)
                _process_status = Enums.AnalysisStatus.Success;
			else
				if (_process_error_lines.Count > 0)
                    _process_status = Enums.AnalysisStatus.Error;
				else
                    _process_status = Enums.AnalysisStatus.NoPacketsCaptured;

            if (_redirect_to_temporary_file && _process_status == Enums.AnalysisStatus.Success)
            {
                FileStream fs = new FileStream(_temporary_file.FullName, FileMode.Create, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);
                for (int i = 0; i < _process_output_lines.Count; i++)
                    sw.WriteLine(_process_output_lines[i]);
                sw.Close();
                fs.Close();
            }

			return _process_status;
		}

		/// <summary>
		/// Build the wireshark argument containing the filename to read (infile)
		/// </summary>
		/// <returns>A <c>string</c> containing the appropriately formatted argument for the input capture file</returns>
		private string BuildWiresharkInfile()
		{
			string output = " -r \"" + _capture_file_info.CaptureFile.FullName + "\"";

			return output;
		}

		/// <summary>
		/// Build the wireshark read filters for a particular test case
		/// </summary>
		/// <returns>A <c>string</c> containing the appropriately formatted wireshark read filter</returns>
		private string BuildWiresharkFilters()
		{
			string output;
            if ((_version_number.Major == 1 && _version_number.Minor >= 10) || (_version_number.Major >= 2))
                output = " -R \"" + _wireshark_filter + "\" -2";
            else
                output = " -R \"" + _wireshark_filter + "\"";

			return output;
		}

        /// <summary>
        /// Build the wireshark argument flag to use the psml output format instead of raw lines
        /// </summary>
        /// <returns>A <c>string</c> containing the appropriately formatted argument for the psml output</returns>
        private string BuildPSMLFlag()
        {
            return " -T psml";
        }

        /// <summary>
        /// Build the wireshark argument flag to not perform name resolution on the addresses.  Without this, MAC addresses will not be processed correctly.
        /// </summary>
        /// <returns>A <c>string</c> containing the appropriately formatted argument to turn off name resolution</returns>
        private string BuildNoNameResolutionFlag()
        {
            return " -n";
        }

        /// <summary>
        /// Build the wireshark argument flag to add the desired EtherNet/IP fields to the output
        /// </summary>
        /// <returns>A <c>string</c> containing the appropriately formatted argument to add the desired EtherNet/IP fields to the output</returns>
        private string BuildEnipFieldsFlag()
        {
            string output;

            if ((_version_number.Major == 1 && _version_number.Minor >= 10) || (_version_number.Major >= 2))
                output = " -o gui.column.format:\"No., %m, Time, %t, Source, %s, Destination, %d, Protocol, %p, ENIP-CID, %Cus:enip.cpf.sai.connid, ENIP-SEQ, %Cus:enip.cpf.sai.seq, Info, %i\"";
            else
                output = " -o column.format:\"No., %m, Time, %t, Source, %s, Destination, %d, Protocol, %p, ENIP-CID, %Cus:enip.cpf.sai.connid, ENIP-SEQ, %Cus:enip.cpf.sai.seq, Info, %i\"";


            return output;
        }

        /// <summary>
        /// Build the wireshark argument flag to output the protocols and header fields
        /// </summary>
        /// <returns></returns>
        private string BuildWiresharkFieldsFlag()
        {
            string output;
            if ((_version_number.Major == 1 &&_version_number.Minor >= 10) || (_version_number.Major >= 2))
                output = "-G fields";
            else
                output = "-G fields3";
            return output;
        }

		#endregion
	}
}
