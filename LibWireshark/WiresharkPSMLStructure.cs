﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IENetP.Generic;
using IENetP.Network;

namespace IENetP.Wireshark
{
    /// <summary>
    /// 
    /// </summary>
    public class WiresharkPSMLStructure
    {
        #region Constructors

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        // ---------------------------------------------------------------------
        // COLUMN --> Number
        // ---------------------------------------------------------------------
        private string _number_title = "No.";
        /// <summary>
        /// Gets or sets the PSML structure title for the number section
        /// </summary>
        /// <remarks>This may be necessary for non-English versions of Wireshark.</remarks>
        public string NumberTitle
        {
            get { return _number_title; }
            set { _number_title = value; }
        }

        private int _number_index = 0;
        /// <summary>
        /// Gets or sets the PSML structure index for the number section
        /// </summary>
        public int NumberIndex
        {
            get { return _number_index; }
            set { _number_index = value; }
        }

        // ---------------------------------------------------------------------
        // COLUMN --> Time
        // ---------------------------------------------------------------------
        private string _time_title = "Time";
        /// <summary>
        /// Gets or sets the PSML structure title for the time section
        /// </summary>
        /// <remarks>This may be necessary for non-English versions of Wireshark.</remarks>
        public string TimeTitle
        {
            get { return _time_title; }
            set { _time_title = value; }
        }

        private int _time_index = 1;
        /// <summary>
        /// Gets or sets the PSML structure index for the time section
        /// </summary>
        public int TimeIndex
        {
            get { return _time_index; }
            set { _time_index = value; }
        }

        // ---------------------------------------------------------------------
        // COLUMN --> Source
        // ---------------------------------------------------------------------
        private string _src_title = "Source";
        /// <summary>
        /// Gets or sets the PSML structure title for the source address section
        /// </summary>
        /// <remarks>This may be necessary for non-English versions of Wireshark.</remarks>
        public string SourceTitle
        {
            get { return _src_title; }
            set { _src_title = value; }
        }

        private int _src_index = 2;
        /// <summary>
        /// Gets or sets the PSML structure index for the source section
        /// </summary>
        public int SourceIndex
        {
            get { return _src_index; }
            set { _src_index = value; }
        }

        // ---------------------------------------------------------------------
        // COLUMN --> Destination
        // ---------------------------------------------------------------------
        private string _dst_title = "Destination";
        /// <summary>
        /// Gets or sets the PSML structure title for the destination address section
        /// </summary>
        /// <remarks>This may be necessary for non-English versions of Wireshark.</remarks>
        public string DestinationTitle
        {
            get { return _dst_title; }
            set { _dst_title = value; }
        }

        private int _dst_index = 3;
        /// <summary>
        /// Gets or sets the PSML structure index for the destination section
        /// </summary>
        public int DestinationIndex
        {
            get { return _dst_index; }
            set { _dst_index = value; }
        }

        // ---------------------------------------------------------------------
        // COLUMN --> Protocol
        // ---------------------------------------------------------------------
        private string _protocol_title = "Protocol";
        /// <summary>
        /// Gets or sets the PSML structure title for the protocol section
        /// </summary>
        /// <remarks>This may be necessary for non-English versions of Wireshark.</remarks>
        public string ProtocolTitle
        {
            get { return _protocol_title; }
            set { _protocol_title = value; }
        }
        
        private int _protocol_index = 4;
        /// <summary>
        /// Gets or sets the PSML structure index for the protocol section
        /// </summary>
        public int ProtocolIndex
        {
            get { return _protocol_index; }
            set { _protocol_index = value; }
        }

        // ---------------------------------------------------------------------
        // COLUMN --> CUSTOM --> EtherNet/IP Connection ID
        // ---------------------------------------------------------------------
        private string _enip_cid_title = "ENIP-CID";
        /// <summary>
        /// Gets or sets the PSML structure title for the EtherNet/IP Connection ID section
        /// </summary>
        /// <remarks>This may be necessary for non-English versions of Wireshark.</remarks>
        public string EnipCidTitle
        {
            get { return _enip_cid_title; }
            set { _enip_cid_title = value; }
        }

        private int _enip_cid_index = 5;
        /// <summary>
        /// Gets or sets the PSML structure index for the EtherNet/IP Connection ID section
        /// </summary>
        public int EnipCidIndex
        {
            get { return _enip_cid_index; }
            set { _enip_cid_index = value; }
        }

        // ---------------------------------------------------------------------
        // COLUMN --> CUSTOM --> EtherNet/IP Sequence Number
        // ---------------------------------------------------------------------
        private string _enip_seq_title = "ENIP-SEQ";
        /// <summary>
        /// Gets or sets the PSML structure title for the EtherNet/IP Sequence Number section
        /// </summary>
        /// <remarks>This may be necessary for non-English versions of Wireshark.</remarks>
        public string EnipSeqTitle
        {
            get { return _enip_seq_title; }
            set { _enip_seq_title = value; }
        }

        private int _enip_seq_index = 6;
        /// <summary>
        /// Gets or sets the PSML structure index for the EtherNet/IP Sequence Number section
        /// </summary>
        public int EnipSeqIndex
        {
            get { return _enip_seq_index; }
            set { _enip_seq_index = value; }
        }

        // ---------------------------------------------------------------------
        // COLUMN --> Info
        // ---------------------------------------------------------------------
        private string _info_title = "Info";
        /// <summary>
        /// Gets or sets the PSML structure title for the info section
        /// </summary>
        /// <remarks>This may be necessary for non-English versions of Wireshark.</remarks>
        public string InfoTitle
        {
            get { return _info_title; }
            set { _info_title = value; }
        }

        private int _info_index = 7;
        /// <summary>
        /// Gets or sets the PSML structure index for the info section
        /// </summary>
        public int InfoIndex
        {
            get { return _info_index; }
            set { _info_index = value; }
        }

        // ---------------------------------------------------------------------
        // Other Columns
        // ---------------------------------------------------------------------
        private List<string> _other = new List<string>();
        /// <summary>
        /// Gets or sets any additional sections for the packet.
        /// </summary>
        /// <remarks>This may occur for a non-standard Wireshark installation.</remarks>
        public List<string> Other
        {
            get { return _other; }
            set { _other = value; }
        }

        #endregion

        #region Methods

        #endregion
    }
}
