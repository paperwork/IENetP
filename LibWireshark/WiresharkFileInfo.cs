using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace IENetP.Wireshark
{
	/// <summary>
	/// Wireshark capture file information
	/// </summary>
	public class WiresharkFileInfo
    {
        #region Constructors

        #endregion

        #region Data Objects

        #endregion

        #region Properties

        private FileInfo _capture_file;
		/// <summary>
		/// Capture file info
		/// </summary>
		public FileInfo CaptureFile
		{
			get { return _capture_file; }
			set { _capture_file = new FileInfo(value.FullName); }
		}
		/// <summary>
		/// Capture file name
		/// </summary>
		public string FileName
		{
			get { return _capture_file.Name; }
		}

		private string _file_type;
		/// <summary>
		/// Capture file type
		/// </summary>
		public string FileType
		{
			get { return _file_type; }
			set { _file_type = value; }
		}

		private string _file_encapsulation;
		/// <summary>
		/// Capture file encapsulation
		/// </summary>
		public string FileEncapsulation
		{
			get { return _file_encapsulation; }
			set { _file_encapsulation = value; }
		}

		private int _file_packets;
		/// <summary>
		/// Number of packets
		/// </summary>
		public int FilePackets
		{
			get { return _file_packets; }
			set { _file_packets = value; }
		}

		private int _file_size;
		/// <summary>
		/// Size of the file (in bytes)
		/// </summary>
		public int FileSize
		{
			get { return _file_size; }
			set { _file_size = value; }
		}

		private int _file_data_size;
		/// <summary>
		/// Total length of all the packets (in bytes)
		/// </summary>
		public int FileDataSize
		{
			get { return _file_data_size; }
			set { _file_data_size = value; }
		}

		private double _file_duration;
		/// <summary>
		/// Capture duration (in seconds)
		/// </summary>
		public double FileDuration
		{
			get { return _file_duration; }
			set { _file_duration = value; }
		}

		private DateTime _file_start;
		/// <summary>
		/// Capture start time
		/// </summary>
		public DateTime FileStart
		{
			get { return _file_start; }
			set { _file_start = value; }
		}

		private DateTime _file_end;
		/// <summary>
		/// Capture end time
		/// </summary>
		public DateTime FileEnd
		{
			get { return _file_end; }
			set { _file_end = value; }
		}

		private double _file_packet_rate_bytes;
		/// <summary>
		/// Average packet rate (in bytes)
		/// </summary>
		public double FilePacketRateInBytes
		{
			get { return _file_packet_rate_bytes; }
			set { _file_packet_rate_bytes = value; }
		}

		private double _file_packet_rate_bits;
		/// <summary>
		/// Average packet rate (in bits)
		/// </summary>
		public double FilePacketRateInBits
		{
			get { return _file_packet_rate_bits; }
			set { _file_packet_rate_bits = value; }
		}

		private double _file_packet_size;
		/// <summary>
		/// Average packet size (in bytes)
		/// </summary>
		public double FilePacketSize
		{
			get { return _file_packet_size; }
			set { _file_packet_size = value; }
		}

		#endregion

        #region Methods

        #endregion
    }
}
