# IENetP
Industrial Ethernet Network Performance (IENetP) Test Tool originally developed by the National Institute of Standards &amp; Technology (NIST). This test tool determines the network performance of industrial devices using standardized metrics and tests.
